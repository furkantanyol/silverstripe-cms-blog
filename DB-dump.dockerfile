FROM mysql:5.7

WORKDIR /home

COPY ./backup.sql /docker-entrypoint-initdb.d/dump.sql

# RUN /bin/bash -c "/usr/bin/mysqld_safe --skip-grant-tables &" && \
#   sleep 5 && \
#   mysql -u ss_user -e "CREATE DATABASE SS_mysite" && \
#   mysql -u ss_user SS_mysite < backup.sql