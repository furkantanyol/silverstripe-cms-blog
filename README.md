SilverStripe CMS Blog

# Installation

1. After cloning the project, go to project folder.

`cd silverstripe-cms-blog`
    
2. Make sure you have Docker up and running, then do Docker compose up.

`docker-compose up`

3. Open another terminal tab for the same directory and check the running images with docker ps.

`docker ps`

You should see something like this

```
CONTAINER ID        IMAGE                       COMMAND                  CREATED             STATUS              PORTS                  NAMES
ac9460fa9832        brettt89/silverstripe-web   "docker-php-entrypoi…"   24 minutes ago      Up 24 minutes       0.0.0.0:3000->80/tcp   silverstripe-cms-blog_web_1
164bdf482342        mysql:5.7                   "docker-entrypoint.s…"   24 minutes ago      Up 24 minutes       3306/tcp, 33060/tcp    silverstripe-cms-blog_db_1
```

4. Copy the CONTAINER ID of the `brettt89/silverstripe-web` image named `silverstripe-cms-blog_web_1` which is `ac9460fa9832`

5. Run `docker exec -ti ${CONTAINER_ID} sh` to run a command in a running container.

`docker exec -ti ac9460fa9832 sh` in this case

6. You should be in the bash. If your terminal shows # instead of your computer name you're in the right place.

7. Go to the cms app folder `cd html/silverstripe-cms` inside the image.

8. Run `composer install` to install the dependencies for our CMS app. (This may take forever but may the patience be with you!)

9. When it finishes go back to first terminal tab where docker runs. Kill the process by pressing `ctrl+c` a couple of times.

10. Run `docker-compose up` again.

11. Go to https://localhost:3000/silverstripe-cms

12. If you see your db initializing and creating tables go to https://localhost:3000/silverstripe-cms again to see your running app.
  
# Restore From MySQL Backup
cat backup.sql | docker exec -i CONTAINER /usr/bin/mysql -u root --password=root DATABASE

You can find the variables in the .env file for this script. (i.e. docker exec d7219526904f /usr/bin/mysqldump -u ss_user --password=123 SS_mysite > backup.sql)


