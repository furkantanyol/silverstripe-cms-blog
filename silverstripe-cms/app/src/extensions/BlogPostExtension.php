<?php

namespace {
    use SilverStripe\ORM\DataExtension;
    use SilverStripe\ORM\ArrayList;
    use SilverStripe\Forms\CheckboxField;
    use SilverStripe\Forms\FieldList;
    
    class BlogPostExtension extends DataExtension {
        
        private static $db = array(
            'IsFeatured' => 'Boolean',
        );

        public function updateCMSFields(FieldList $fields) {            
            $fields->addFieldToTab("Root.PostOptions", new CheckboxField('IsFeatured', 'Mark as featured post'));
        }

        public function RelatedCategoryPosts() {
        
            $relatedPosts = ArrayList::create();
    
            foreach ($this->owner->Categories() as $category) {
                $relatedPosts->merge($category->BlogPosts());
            }
    
            $relatedPosts->removeDuplicates('ID');
            $relatedPosts->remove($relatedPosts->byID($this->owner->ID));
    
            return $relatedPosts;
        }
    
        public function RelatedTagPosts() {
            $relatedPosts = ArrayList::create();
    
            foreach ($this->owner->Tags() as $tag) {
                $relatedPosts->merge($tag->BlogPosts());
            }
    
            $relatedPosts->removeDuplicates();
            $relatedPosts->remove($relatedPosts->byID($this->owner->ID));
    
            return $relatedPosts;
        }
        
    }
}

// return BlogPost::get()->sort('Date', 'desc')->first();
