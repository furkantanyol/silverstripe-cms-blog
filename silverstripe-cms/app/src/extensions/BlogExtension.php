<?php

namespace {

    use SilverStripe\ORM\DataExtension;
    use SilverStripe\Blog\Model\BlogPost;

    class BlogExtension extends DataExtension {

        public function LatestPost() {
            return BlogPost::get()->sort('PublishDate', 'desc')->first();
        }

        public function LatestFeaturedBlogPost() {
            $list = BlogPost::get()
                ->filter('ParentID', $this->owner->ID)
                ->filter('IsFeatured', true);

            $latest = $list->sort('PublishDate', 'desc')->first();

            return $latest;
        }

        public function FeaturedBlogPosts() {
            $list = BlogPost::get()
                ->filter('ParentID', $this->owner->ID)
                ->filter('IsFeatured', true);
            return $list;
        }
    }
}