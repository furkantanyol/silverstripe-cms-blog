const images = Array.from(document.querySelectorAll('article img:not(.category-tag), .image-container img'))
const modal = document.querySelector('.modal')
const carouselElements = document.querySelectorAll('.slideshow-container, .arrow-button')

images.forEach((image, i) => image.addEventListener('click', () => {
    if (modal.className.includes(classes.modal.modalActive)) return
    modal.className += ` ${classes.modal.modalActive}`
    // small timeout for transition/animation to work
    setTimeout(() => {
      modal.className += ` ${classes.modal.modalVisible}`
    }, 20)
    SlidePresenter.goToSlide(i+1)
}))

modal.addEventListener('click', () => {
    modal.className = classes.modal.modalHidden
})

carouselElements.forEach(carouselElement => carouselElement.addEventListener('click', (e) => {
    e.stopPropagation()
}))



var slideIndex = 1 // initial slide index is 1

const init = () => {
    const slideComponents = images.map((image, i) =>
        SlidePresenter.createSlide(image, i + 1)
    )
    // const currentSlideComponent = slideComponents[0]

    SlidePresenter.addSlidesToDOM(slideComponents)
    SlidePresenter.createNavigationElements(slideComponents.length)
    // SlidePresenter.renderFirstSlide(currentSlideComponent)
    // createSlides(slides)
}

// DOMService is used to create DOM elements in js
const DOMService = {
  createElement: ({
    elementType,
    className,
    src,
    innerHTML,
    href,
    target,
    clickHandler
  }) => {
    const element = document.createElement(elementType)
    element.className = className
    element.src = src
    element.href = href
    element.target = target
    if (innerHTML) element.innerHTML = innerHTML
    if (clickHandler) element.onclick = clickHandler

    return element
  }
}

// SlidePresenter helps create, render and navigate slides
const SlidePresenter = {
  createSlide: image => {
    // create the slide container
    const slideContainer = DOMService.createElement({
      elementType: elementTypes.div,
      className: classes.slide.container
    })

    //create the slide elements
    const slideImage = image

    // add slide elements to the slide container
    slideContainer.innerHTML +=
      slideImage.outerHTML

    return slideContainer
  },
  addSlidesToDOM: slideComponents => {
    slideComponents.forEach(slideComponent =>
      document.getElementById(ids.slides).appendChild(slideComponent)
    )
  },
  createNavigationElements: numberOfSlides => {
    // create dots
    for (let i = 1; i < numberOfSlides + 1; i++) {
      const dot = DOMService.createElement({
        elementType: elementTypes.span,
        className:
          i === 1
            ? `${classes.slide.dot} ${classes.slide.dotActive}`
            : classes.slide.dot,
        clickHandler: () => SlidePresenter.goToSlide(i)
      })
      document.getElementById(ids.dots).appendChild(dot)
    }

    // add functionality to arrow elements
    const prevArrow = document.getElementById(ids.prevArrow)
    const nextArrow = document.getElementById(ids.nextArrow)
    prevArrow.onclick = () => SlidePresenter.goPrevious(numberOfSlides)
    nextArrow.onclick = () => SlidePresenter.goNext(numberOfSlides)
  },
  goNext: numberOfSlides => {
    if (slideIndex === numberOfSlides) {
      slideIndex = 1
      SlidePresenter.goToSlide(1)
    } else {
      slideIndex += 1
    }

    SlidePresenter.goToSlide(slideIndex)
  },
  goPrevious: numberOfSlides => {
    if (slideIndex === 1) {
      slideIndex = numberOfSlides
    } else {
      slideIndex -= 1
    }
    SlidePresenter.goToSlide(slideIndex)
  },
  renderFirstSlide: slide => {
    // activates the first slide in init
    slide.className += ` ${classes.slide.containerActive} ${
      classes.slide.containerVisible
    }`
  },
  goToSlide: i => {
    slideIndex = i
    const allSlides = Array.from(
      document.getElementsByClassName(classes.slide.container)
    )
    const dots = Array.from(document.getElementsByClassName(classes.slide.dot))

    // show/hide slides and dots with resoect to the slide index
    allSlides.forEach((slide, index) => {
      if (index + 1 === slideIndex) {
        if (slide.className.includes(classes.slide.containerActive)) return
        slide.className += ` ${classes.slide.containerActive}`
        // small timeout for transition/animation to work
        setTimeout(() => {
          slide.className += ` ${classes.slide.containerVisible}`
        }, 20)
        dots[index].className += ` ${classes.slide.dotActive}`
      } else {
        slide.className = classes.slide.container
        dots[index].className = classes.slide.dot
      }
    })
  }
}

// constants
const classes = {
  slide: {
    container: 'slide-container',
    containerActive: 'slide-container-active',
    containerVisible: 'slide-container-visible',
    dotActive: 'dot-active',
    image: 'slide-image',
    title: 'slide-title',
    synopsis: 'slide-synopsis',
    link: 'slide-link',
    dot: 'dot',
    nextArrow: 'next-arrow',
    prevArrow: 'previous-arrow'
  },
  modal: { 
      modalHidden: 'modal',
      modalActive: 'modal-active',
      modalVisible: 'modal-visible'
  }
}

const ids = {
  prevArrow: 'prev-arrow',
  nextArrow: 'next-arrow',
  dots: 'dots',
  slides: 'slides'
}

const elementTypes = {
  div: 'div',
  img: 'img',
  h1: 'h1',
  a: 'a',
  p: 'p',
  span: 'span'
}

init();