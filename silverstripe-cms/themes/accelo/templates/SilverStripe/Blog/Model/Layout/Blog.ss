
<% require themedCSS("blog") %>
<% require themedCSS("blog-post") %>


<div class="blog-entry content-container <% if $SideBarView %>unit size3of4<% end_if %>">
	<article class="blog-article">
		<div class="blog-wrapper">
			<% if $LatestPost %>
			<div class="post-title-image-container">
				<div class="post-image latest-post-image">$LatestPost.FeaturedImage</div>
				<h1 class="post-title">
					$LatestPost.Title
					<a class="read-button" href="$LatestPost.URLSegment">READ NOW</a>
				</h1>
				<div class="categories">
					<p class="latest-update-text">Latest Update</p>
				</div>
			</div>
			<% end_if %>	

			<div class="content">$Content</div>

			<% if $PaginatedList.Exists %>
				<div class="related-post-list">
				<% loop $PaginatedList %>
					<% include SilverStripe\\Blog\\PostSummary %>
				<% end_loop %>
				</div>
			<% else %>
				<p><%t SilverStripe\\Blog\\Model\\Blog.NoPosts 'There are no posts' %></p>
			<% end_if %>
		</div>

		<div class="featured-post-container">
			<div class="featured-post-image">$LatestFeaturedBlogPost.FeaturedImage</div>
			<div class="featured-post-content">
				<p class="feature-text">Feature</p>
				<h1 class="featured-post-title">
					$LatestFeaturedBlogPost.Title
				</h1>
				<span class="stripe-thingy"></span>
				<p class="featured-post-excerpt">$LatestFeaturedBlogPost.Excerpt</p>
				<a class="read-button read-button-featured" href="$LatestFeaturedBlogPost.URLSegment">READ NOW</a>
			</div>
		</div>

		<div class="subscribe-container">
			<div class="subscribe-content">
				<h1 class="subscribe-title">SUBSCRIBE</h1>
				<p class="subscribe-description">Sign up to receive email updates whenever we post about exciting new features!</p>
				<form class="subscription-form">
					<input class="subscribe-input" placeholder="Email address" type="email">
					<input class="read-button subscribe-button" type="submit" value="SUBSCRIBE">
				</form>
			</div>
		</div>
	</article>

	$Form
	$CommentsForm

	<% with $PaginatedList %>
		<% include SilverStripe\\Blog\\Pagination %>
	<% end_with %>
</div>

<% include SilverStripe\\Blog\\BlogSideBar %>




<!--
		<h1>
			<% if $ArchiveYear %>
				<%t SilverStripe\\Blog\\Model\\Blog.Archive 'Archive' %>:
				<% if $ArchiveDay %>
					$ArchiveDate.Nice
				<% else_if $ArchiveMonth %>
					$ArchiveDate.format('MMMM, y')
				<% else %>
					$ArchiveDate.format('y')
				<% end_if %>
			<% else_if $CurrentTag %>
				<%t SilverStripe\\Blog\\Model\\Blog.Tag 'Tag' %>: $CurrentTag.Title
			<% else_if $CurrentCategory %>
				<%t SilverStripe\\Blog\\Model\\Blog.Category 'Category' %>: $CurrentCategory.Title
			<% else %>
				$Title
			<% end_if %>
		</h1>
-->	