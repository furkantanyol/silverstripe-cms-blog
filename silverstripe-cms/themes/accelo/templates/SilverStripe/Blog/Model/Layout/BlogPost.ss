<% require css('silverstripe/blog: client/dist/styles/main.css') %>
<% require themedCSS('blog-post') %>
<% require themedCSS('carousel') %>
<% require themedJavascript('carousel') %>

<div class="blog-entry content-container <% if $SideBarView %>unit size3of4<% end_if %>">
	<article>
		<div class="post-title-image-container">
			<% if $FeaturedImage %>
			<div class="post-image">$FeaturedImage</div>
			<% end_if %>
			<h1 class="post-title">$Title</h1>
			<div class="categories">
				<% if $Categories %>
					<img class="category-tag" src="/silverstripe-cms/resources/themes/accelo/images/tag.svg" alt="tag"/>
					<% loop $Categories %>
						<a class="category-btn" href="$Link">$Title</a>
					<% end_loop %>
				<% end_if %>
			</div>
		</div>

		<div class="content">$Content</div>
		<% include SilverStripe\\Blog\\EntryMeta %>
	</article>

	

	<div class="related-category-wrapper">
		<% if $RelatedCategoryPosts %>
			<h2 class="post-subtitle">Similar Articles</h2>
			<div class="related-post-list">
				<% loop $RelatedCategoryPosts %>
				<a class="related-post" href="$Link">
					<div class="related-post-container">
						<% if $FeaturedImage %>
						<div class="related-post-image">$FeaturedImage.ScaleWidth(1300)</div>
						<% end_if %>
						<p class="related-post-title">$Title</p>
						<p class="related-post-excerpt">$Excerpt(10)</p>
					</div>
				</a>					
				<% end_loop %>
			<div>
		<% end_if %>
	</div>

	<div class="modal">
		<div class="carousel-root">
			<button class="arrow-button" id="prev-arrow">&#10094;</button>
				<div class="slideshow-container">
					<div class="slides" id="slides">
						<!-- script generates the slides here -->
					</div>
					<div id="dots">
						<!-- script generates the dots here depending on the slide number -->
					</div>
				</div>
			<button class="arrow-button" id="next-arrow">&#10095;</button>
		</div>
	</div>

	$Form
	$CommentsForm
</div>

<% include SilverStripe\\Blog\\BlogSideBar %>
