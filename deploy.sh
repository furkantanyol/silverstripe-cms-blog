#!/usr/bin/env bash

#$(aws ecr get-login --no-include-email --region ap-southeast-2)
docker-compose --file docker-compose.dev.yml build
ecs-cli push 545319754199.dkr.ecr.ap-southeast-2.amazonaws.com/silverstrip-cms-blog:latest
ecs-cli push 545319754199.dkr.ecr.ap-southeast-2.amazonaws.com/silverstripe-cms-blog-database:latest
ecs-cli compose -f docker-compose.dev.yml up