-- MySQL dump 10.13  Distrib 5.7.25, for Linux (x86_64)
--
-- Host: localhost    Database: SS_mysite
-- ------------------------------------------------------
-- Server version	5.7.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Blog`
--

DROP TABLE IF EXISTS `Blog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Blog` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PostsPerPage` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Blog`
--

LOCK TABLES `Blog` WRITE;
/*!40000 ALTER TABLE `Blog` DISABLE KEYS */;
INSERT INTO `Blog` VALUES (14,10);
/*!40000 ALTER TABLE `Blog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `BlogCategory`
--

DROP TABLE IF EXISTS `BlogCategory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BlogCategory` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('SilverStripe\\Blog\\Model\\BlogCategory') CHARACTER SET utf8 DEFAULT 'SilverStripe\\Blog\\Model\\BlogCategory',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Title` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `URLSegment` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `BlogID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `ClassName` (`ClassName`),
  KEY `BlogID` (`BlogID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `BlogCategory`
--

LOCK TABLES `BlogCategory` WRITE;
/*!40000 ALTER TABLE `BlogCategory` DISABLE KEYS */;
INSERT INTO `BlogCategory` VALUES (1,'SilverStripe\\Blog\\Model\\BlogCategory','2019-04-20 12:28:37','2019-04-20 12:28:35','product','product',14),(2,'SilverStripe\\Blog\\Model\\BlogCategory','2019-04-20 12:28:37','2019-04-20 12:28:35','roadmap','roadmap',14),(3,'SilverStripe\\Blog\\Model\\BlogCategory','2019-04-20 12:28:38','2019-04-20 12:28:35','q1','q1',14),(4,'SilverStripe\\Blog\\Model\\BlogCategory','2019-04-20 12:28:38','2019-04-20 12:28:35','2019','2019',14),(5,'SilverStripe\\Blog\\Model\\BlogCategory','2019-04-24 00:48:33','2019-04-24 00:48:32','agency','agency',14),(6,'SilverStripe\\Blog\\Model\\BlogCategory','2019-04-24 00:48:34','2019-04-24 00:48:33','accelo','accelo',14);
/*!40000 ALTER TABLE `BlogCategory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `BlogPost`
--

DROP TABLE IF EXISTS `BlogPost`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BlogPost` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PublishDate` datetime DEFAULT NULL,
  `AuthorNames` varchar(1024) CHARACTER SET utf8 DEFAULT NULL,
  `Summary` mediumtext CHARACTER SET utf8,
  `FeaturedImageID` int(11) NOT NULL DEFAULT '0',
  `IsFeatured` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `PublishDate` (`PublishDate`),
  KEY `FeaturedImageID` (`FeaturedImageID`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `BlogPost`
--

LOCK TABLES `BlogPost` WRITE;
/*!40000 ALTER TABLE `BlogPost` DISABLE KEYS */;
INSERT INTO `BlogPost` VALUES (15,'2019-04-20 12:28:42',NULL,NULL,5,0),(16,'2019-04-21 11:49:17',NULL,NULL,8,0),(17,'2019-04-24 00:33:43',NULL,NULL,10,0),(18,'2019-04-24 00:48:35',NULL,NULL,11,0),(19,'2019-04-24 13:31:03',NULL,NULL,14,0),(20,'2019-04-24 23:05:45',NULL,NULL,15,1),(21,'2019-04-25 23:35:16',NULL,NULL,16,0);
/*!40000 ALTER TABLE `BlogPost` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `BlogPost_Authors`
--

DROP TABLE IF EXISTS `BlogPost_Authors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BlogPost_Authors` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `BlogPostID` int(11) NOT NULL DEFAULT '0',
  `MemberID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `BlogPostID` (`BlogPostID`),
  KEY `MemberID` (`MemberID`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `BlogPost_Authors`
--

LOCK TABLES `BlogPost_Authors` WRITE;
/*!40000 ALTER TABLE `BlogPost_Authors` DISABLE KEYS */;
INSERT INTO `BlogPost_Authors` VALUES (1,7,1),(2,9,1),(3,11,1),(4,12,1),(5,15,1),(6,16,1),(7,17,1),(8,18,1),(9,19,1),(10,20,1),(11,21,1);
/*!40000 ALTER TABLE `BlogPost_Authors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `BlogPost_Categories`
--

DROP TABLE IF EXISTS `BlogPost_Categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BlogPost_Categories` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `BlogPostID` int(11) NOT NULL DEFAULT '0',
  `BlogCategoryID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `BlogPostID` (`BlogPostID`),
  KEY `BlogCategoryID` (`BlogCategoryID`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `BlogPost_Categories`
--

LOCK TABLES `BlogPost_Categories` WRITE;
/*!40000 ALTER TABLE `BlogPost_Categories` DISABLE KEYS */;
INSERT INTO `BlogPost_Categories` VALUES (1,15,1),(2,15,2),(3,15,3),(4,15,4),(5,16,1),(6,18,5),(7,18,6),(8,18,1),(9,19,4),(10,20,4),(11,21,1);
/*!40000 ALTER TABLE `BlogPost_Categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `BlogPost_Live`
--

DROP TABLE IF EXISTS `BlogPost_Live`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BlogPost_Live` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PublishDate` datetime DEFAULT NULL,
  `AuthorNames` varchar(1024) CHARACTER SET utf8 DEFAULT NULL,
  `Summary` mediumtext CHARACTER SET utf8,
  `FeaturedImageID` int(11) NOT NULL DEFAULT '0',
  `IsFeatured` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `PublishDate` (`PublishDate`),
  KEY `FeaturedImageID` (`FeaturedImageID`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `BlogPost_Live`
--

LOCK TABLES `BlogPost_Live` WRITE;
/*!40000 ALTER TABLE `BlogPost_Live` DISABLE KEYS */;
INSERT INTO `BlogPost_Live` VALUES (15,'2019-04-20 12:28:42',NULL,NULL,5,0),(16,'2019-04-21 11:49:17',NULL,NULL,8,0),(17,'2019-04-24 00:33:43',NULL,NULL,10,0),(18,'2019-04-24 00:48:35',NULL,NULL,11,0),(19,'2019-04-24 13:31:03',NULL,NULL,14,0),(20,'2019-04-24 23:05:45',NULL,NULL,15,1),(21,'2019-04-25 23:35:16',NULL,NULL,16,0);
/*!40000 ALTER TABLE `BlogPost_Live` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `BlogPost_Tags`
--

DROP TABLE IF EXISTS `BlogPost_Tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BlogPost_Tags` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `BlogPostID` int(11) NOT NULL DEFAULT '0',
  `BlogTagID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `BlogPostID` (`BlogPostID`),
  KEY `BlogTagID` (`BlogTagID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `BlogPost_Tags`
--

LOCK TABLES `BlogPost_Tags` WRITE;
/*!40000 ALTER TABLE `BlogPost_Tags` DISABLE KEYS */;
INSERT INTO `BlogPost_Tags` VALUES (1,15,1),(2,15,2),(3,15,3),(4,15,4),(5,16,1);
/*!40000 ALTER TABLE `BlogPost_Tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `BlogPost_Versions`
--

DROP TABLE IF EXISTS `BlogPost_Versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BlogPost_Versions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `PublishDate` datetime DEFAULT NULL,
  `AuthorNames` varchar(1024) CHARACTER SET utf8 DEFAULT NULL,
  `Summary` mediumtext CHARACTER SET utf8,
  `FeaturedImageID` int(11) NOT NULL DEFAULT '0',
  `IsFeatured` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  KEY `RecordID` (`RecordID`),
  KEY `Version` (`Version`),
  KEY `PublishDate` (`PublishDate`),
  KEY `FeaturedImageID` (`FeaturedImageID`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `BlogPost_Versions`
--

LOCK TABLES `BlogPost_Versions` WRITE;
/*!40000 ALTER TABLE `BlogPost_Versions` DISABLE KEYS */;
INSERT INTO `BlogPost_Versions` VALUES (1,7,1,NULL,NULL,NULL,0,0),(2,7,2,NULL,NULL,NULL,0,0),(3,7,3,'2019-04-18 21:47:50',NULL,NULL,0,0),(4,7,4,'2019-04-18 21:47:50',NULL,NULL,0,0),(5,9,1,NULL,NULL,NULL,0,0),(6,9,2,NULL,NULL,NULL,0,0),(7,9,3,'2019-04-18 22:03:55',NULL,NULL,0,0),(8,9,4,'2019-04-18 22:03:55',NULL,NULL,0,0),(9,11,1,NULL,NULL,NULL,0,0),(10,12,1,NULL,NULL,NULL,0,0),(11,12,2,NULL,NULL,NULL,0,0),(12,12,3,'2019-04-18 23:03:10',NULL,NULL,0,0),(13,12,4,'2019-04-18 23:03:10',NULL,NULL,0,0),(14,12,5,'2019-04-18 23:03:10',NULL,NULL,0,0),(15,15,1,NULL,NULL,NULL,0,0),(16,15,2,NULL,NULL,NULL,5,0),(17,15,3,'2019-04-20 12:28:42',NULL,NULL,5,0),(18,15,4,'2019-04-20 12:28:42',NULL,NULL,5,0),(19,15,5,'2019-04-20 12:28:42',NULL,NULL,5,0),(20,15,6,'2019-04-20 12:28:42',NULL,NULL,5,0),(21,15,7,'2019-04-20 12:28:42',NULL,NULL,5,0),(22,15,8,'2019-04-20 12:28:42',NULL,NULL,5,0),(23,16,1,NULL,NULL,NULL,0,0),(24,16,2,NULL,NULL,NULL,8,0),(25,16,3,'2019-04-21 11:49:17',NULL,NULL,8,0),(26,16,4,'2019-04-21 11:49:17',NULL,NULL,8,0),(27,15,9,'2019-04-20 12:28:42',NULL,NULL,5,0),(28,15,10,'2019-04-20 12:28:42',NULL,NULL,5,0),(29,15,11,'2019-04-20 12:28:42',NULL,NULL,5,0),(30,15,12,'2019-04-20 12:28:42',NULL,NULL,5,0),(31,15,13,'2019-04-20 12:28:42',NULL,NULL,5,0),(32,15,14,'2019-04-20 12:28:42',NULL,NULL,5,0),(33,17,1,NULL,NULL,NULL,0,0),(34,17,2,NULL,NULL,NULL,10,0),(35,17,3,'2019-04-24 00:33:43',NULL,NULL,10,0),(36,17,4,'2019-04-24 00:33:43',NULL,NULL,10,0),(37,18,1,NULL,NULL,NULL,0,0),(38,18,2,NULL,NULL,NULL,11,0),(39,18,3,'2019-04-24 00:48:35',NULL,NULL,11,0),(40,18,4,'2019-04-24 00:48:35',NULL,NULL,11,0),(41,19,1,NULL,NULL,NULL,0,0),(42,19,2,NULL,NULL,NULL,14,0),(43,19,3,'2019-04-24 13:31:03',NULL,NULL,14,0),(44,19,4,'2019-04-24 13:31:03',NULL,NULL,14,0),(45,20,1,NULL,NULL,NULL,0,0),(46,20,2,NULL,NULL,NULL,15,1),(47,20,3,'2019-04-24 23:05:45',NULL,NULL,15,1),(48,20,4,'2019-04-24 23:05:45',NULL,NULL,15,1),(49,21,1,NULL,NULL,NULL,0,0),(50,21,2,NULL,NULL,NULL,16,0),(51,21,3,'2019-04-25 23:35:16',NULL,NULL,16,0),(52,21,4,'2019-04-25 23:35:16',NULL,NULL,16,0);
/*!40000 ALTER TABLE `BlogPost_Versions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `BlogTag`
--

DROP TABLE IF EXISTS `BlogTag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BlogTag` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('SilverStripe\\Blog\\Model\\BlogTag') CHARACTER SET utf8 DEFAULT 'SilverStripe\\Blog\\Model\\BlogTag',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Title` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `URLSegment` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `BlogID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `ClassName` (`ClassName`),
  KEY `BlogID` (`BlogID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `BlogTag`
--

LOCK TABLES `BlogTag` WRITE;
/*!40000 ALTER TABLE `BlogTag` DISABLE KEYS */;
INSERT INTO `BlogTag` VALUES (1,'SilverStripe\\Blog\\Model\\BlogTag','2019-04-20 12:28:38','2019-04-20 12:28:35','product','product',14),(2,'SilverStripe\\Blog\\Model\\BlogTag','2019-04-20 12:28:39','2019-04-20 12:28:36','roadmap','roadmap',14),(3,'SilverStripe\\Blog\\Model\\BlogTag','2019-04-20 12:28:39','2019-04-20 12:28:36','q1','q1',14),(4,'SilverStripe\\Blog\\Model\\BlogTag','2019-04-20 12:28:39','2019-04-20 12:28:36','2019','2019',14);
/*!40000 ALTER TABLE `BlogTag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Blog_Contributors`
--

DROP TABLE IF EXISTS `Blog_Contributors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Blog_Contributors` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `BlogID` int(11) NOT NULL DEFAULT '0',
  `MemberID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `BlogID` (`BlogID`),
  KEY `MemberID` (`MemberID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Blog_Contributors`
--

LOCK TABLES `Blog_Contributors` WRITE;
/*!40000 ALTER TABLE `Blog_Contributors` DISABLE KEYS */;
/*!40000 ALTER TABLE `Blog_Contributors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Blog_Editors`
--

DROP TABLE IF EXISTS `Blog_Editors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Blog_Editors` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `BlogID` int(11) NOT NULL DEFAULT '0',
  `MemberID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `BlogID` (`BlogID`),
  KEY `MemberID` (`MemberID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Blog_Editors`
--

LOCK TABLES `Blog_Editors` WRITE;
/*!40000 ALTER TABLE `Blog_Editors` DISABLE KEYS */;
/*!40000 ALTER TABLE `Blog_Editors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Blog_Live`
--

DROP TABLE IF EXISTS `Blog_Live`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Blog_Live` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PostsPerPage` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Blog_Live`
--

LOCK TABLES `Blog_Live` WRITE;
/*!40000 ALTER TABLE `Blog_Live` DISABLE KEYS */;
INSERT INTO `Blog_Live` VALUES (14,10);
/*!40000 ALTER TABLE `Blog_Live` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Blog_Versions`
--

DROP TABLE IF EXISTS `Blog_Versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Blog_Versions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `PostsPerPage` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  KEY `RecordID` (`RecordID`),
  KEY `Version` (`Version`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Blog_Versions`
--

LOCK TABLES `Blog_Versions` WRITE;
/*!40000 ALTER TABLE `Blog_Versions` DISABLE KEYS */;
INSERT INTO `Blog_Versions` VALUES (1,6,1,10),(2,6,2,10),(3,8,1,10),(4,8,2,10),(5,8,3,10),(6,8,4,10),(7,8,5,10),(8,8,6,10),(9,8,7,10),(10,8,8,10),(11,8,9,10),(12,8,10,10),(13,10,1,10),(14,10,2,10),(15,10,3,10),(16,13,1,10),(17,13,2,10),(18,14,1,10),(19,14,2,10),(20,14,3,10);
/*!40000 ALTER TABLE `Blog_Versions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Blog_Writers`
--

DROP TABLE IF EXISTS `Blog_Writers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Blog_Writers` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `BlogID` int(11) NOT NULL DEFAULT '0',
  `MemberID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `BlogID` (`BlogID`),
  KEY `MemberID` (`MemberID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Blog_Writers`
--

LOCK TABLES `Blog_Writers` WRITE;
/*!40000 ALTER TABLE `Blog_Writers` DISABLE KEYS */;
/*!40000 ALTER TABLE `Blog_Writers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ChangeSet`
--

DROP TABLE IF EXISTS `ChangeSet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ChangeSet` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('SilverStripe\\Versioned\\ChangeSet') CHARACTER SET utf8 DEFAULT 'SilverStripe\\Versioned\\ChangeSet',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `State` enum('open','published','reverted') CHARACTER SET utf8 DEFAULT 'open',
  `IsInferred` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Description` mediumtext CHARACTER SET utf8,
  `PublishDate` datetime DEFAULT NULL,
  `LastSynced` datetime DEFAULT NULL,
  `OwnerID` int(11) NOT NULL DEFAULT '0',
  `PublisherID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `State` (`State`),
  KEY `ID` (`ID`),
  KEY `ClassName` (`ClassName`),
  KEY `OwnerID` (`OwnerID`),
  KEY `PublisherID` (`PublisherID`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ChangeSet`
--

LOCK TABLES `ChangeSet` WRITE;
/*!40000 ALTER TABLE `ChangeSet` DISABLE KEYS */;
INSERT INTO `ChangeSet` VALUES (1,'SilverStripe\\Versioned\\ChangeSet','2019-04-18 21:47:52','2019-04-18 21:47:49','Generated by publish of \'New Blog Post\' at Apr 18, 2019, 9:47 PM','published',1,NULL,'2019-04-18 21:47:52','2019-04-18 21:47:50',0,1),(2,'SilverStripe\\Versioned\\ChangeSet','2019-04-18 21:48:30','2019-04-18 21:48:29','Generated by publish of \'New Blog\' at Apr 18, 2019, 9:48 PM','published',1,NULL,'2019-04-18 21:48:30','2019-04-18 21:48:29',0,1),(3,'SilverStripe\\Versioned\\ChangeSet','2019-04-18 22:03:56','2019-04-18 22:03:53','Generated by publish of \'New Blog Post asdnfdklnfadf\' at Apr 18, 2019, 10:03 PM','published',1,NULL,'2019-04-18 22:03:56','2019-04-18 22:03:54',0,1),(4,'SilverStripe\\Versioned\\ChangeSet','2019-04-18 22:04:33','2019-04-18 22:04:31','Generated by publish of \'New Blog\' at Apr 18, 2019, 10:04 PM','published',1,NULL,'2019-04-18 22:04:33','2019-04-18 22:04:32',0,1),(5,'SilverStripe\\Versioned\\ChangeSet','2019-04-18 22:10:40','2019-04-18 22:10:38','Generated by publish of \'crazy blog of tyhe year 2081\' at Apr 18, 2019, 10:10 PM','published',1,NULL,'2019-04-18 22:10:40','2019-04-18 22:10:39',0,1),(6,'SilverStripe\\Versioned\\ChangeSet','2019-04-18 22:26:40','2019-04-18 22:26:37','Generated by publish of \'crazy blog of tyhe year 2081sadfasdf\' at Apr 18, 2019, 10:26 PM','published',1,NULL,'2019-04-18 22:26:40','2019-04-18 22:26:39',0,1),(7,'SilverStripe\\Versioned\\ChangeSet','2019-04-18 22:30:08','2019-04-18 22:30:06','Generated by publish of \'crazy blog of tyhe year 2081sadfasdf\' at Apr 18, 2019, 10:30 PM','published',1,NULL,'2019-04-18 22:30:08','2019-04-18 22:30:07',0,1),(8,'SilverStripe\\Versioned\\ChangeSet','2019-04-18 22:35:18','2019-04-18 22:35:17','Generated by publish of \'crazy blog of tyhe year 2081sadfasdf\' at Apr 18, 2019, 10:35 PM','published',1,NULL,'2019-04-18 22:35:18','2019-04-18 22:35:17',0,1),(9,'SilverStripe\\Versioned\\ChangeSet','2019-04-18 22:43:17','2019-04-18 22:43:15','Generated by publish of \'New Blog 2\' at Apr 18, 2019, 10:43 PM','published',1,NULL,'2019-04-18 22:43:17','2019-04-18 22:43:16',0,1),(10,'SilverStripe\\Versioned\\ChangeSet','2019-04-18 23:03:11','2019-04-18 23:03:09','Generated by publish of \'aaaarrrgghhhhhh\' at Apr 18, 2019, 11:03 PM','published',1,NULL,'2019-04-18 23:03:11','2019-04-18 23:03:09',0,1),(11,'SilverStripe\\Versioned\\ChangeSet','2019-04-18 23:06:46','2019-04-18 23:06:44','Generated by publish of \'New Blog\' at Apr 18, 2019, 11:06 PM','published',1,NULL,'2019-04-18 23:06:46','2019-04-18 23:06:45',0,1),(12,'SilverStripe\\Versioned\\ChangeSet','2019-04-20 12:14:27','2019-04-20 12:14:26','Generated by publish of \'Blog\' at Apr 20, 2019, 12:14 PM','published',1,NULL,'2019-04-20 12:14:27','2019-04-20 12:14:26',0,1),(13,'SilverStripe\\Versioned\\ChangeSet','2019-04-20 12:28:56','2019-04-20 12:28:40','Generated by publish of \'Product Roadmap: 2019 Q1\' at Apr 20, 2019, 12:28 PM','published',1,NULL,'2019-04-20 12:28:56','2019-04-20 12:28:42',0,1),(14,'SilverStripe\\Versioned\\ChangeSet','2019-04-21 10:09:59','2019-04-21 10:09:50','Generated by publish of \'Product Roadmap: 2019 Q1\' at Apr 21, 2019, 10:09 AM','published',1,NULL,'2019-04-21 10:09:59','2019-04-21 10:09:53',0,1),(15,'SilverStripe\\Versioned\\ChangeSet','2019-04-21 10:56:38','2019-04-21 10:56:31','Generated by publish of \'Product Roadmap: 2019 Q1\' at Apr 21, 2019, 10:56 AM','published',1,NULL,'2019-04-21 10:56:38','2019-04-21 10:56:33',0,1),(16,'SilverStripe\\Versioned\\ChangeSet','2019-04-21 11:49:20','2019-04-21 11:49:16','Generated by publish of \'Q1 Priorities 2019\' at Apr 21, 2019, 11:49 AM','published',1,NULL,'2019-04-21 11:49:20','2019-04-21 11:49:17',0,1),(17,'SilverStripe\\Versioned\\ChangeSet','2019-04-22 23:31:27','2019-04-22 23:31:19','Generated by publish of \'Product Roadmap: 2019 Q1\' at Apr 22, 2019, 11:31 PM','published',1,NULL,'2019-04-22 23:31:27','2019-04-22 23:31:21',0,1),(18,'SilverStripe\\Versioned\\ChangeSet','2019-04-22 23:33:55','2019-04-22 23:33:47','Generated by publish of \'Product Roadmap: 2019 Q1\' at Apr 22, 2019, 11:33 PM','published',1,NULL,'2019-04-22 23:33:55','2019-04-22 23:33:49',0,1),(19,'SilverStripe\\Versioned\\ChangeSet','2019-04-22 23:41:35','2019-04-22 23:41:27','Generated by publish of \'Product Roadmap: 2019 Q1\' at Apr 22, 2019, 11:41 PM','published',1,NULL,'2019-04-22 23:41:35','2019-04-22 23:41:29',0,1),(20,'SilverStripe\\Versioned\\ChangeSet','2019-04-24 00:31:05','2019-04-24 00:31:04','Generated by publish of \'fancycrave 371075 unsplash\' at Apr 24, 2019, 12:31 AM','published',1,NULL,'2019-04-24 00:31:05','2019-04-24 00:31:04',0,1),(21,'SilverStripe\\Versioned\\ChangeSet','2019-04-24 00:33:44','2019-04-24 00:33:42','Generated by publish of \'Scheduling & Rates\' at Apr 24, 2019, 12:33 AM','published',1,NULL,'2019-04-24 00:33:44','2019-04-24 00:33:43',0,1),(22,'SilverStripe\\Versioned\\ChangeSet','2019-04-24 00:48:39','2019-04-24 00:48:34','Generated by publish of \'Digital Agencies: Our Future Of Media\' at Apr 24, 2019, 12:48 AM','published',1,NULL,'2019-04-24 00:48:39','2019-04-24 00:48:35',0,1),(23,'SilverStripe\\Versioned\\ChangeSet','2019-04-24 13:31:05','2019-04-24 13:31:02','Generated by publish of \'Timers And Timesheets\' at Apr 24, 2019, 1:31 PM','published',1,NULL,'2019-04-24 13:31:05','2019-04-24 13:31:02',0,1),(24,'SilverStripe\\Versioned\\ChangeSet','2019-04-24 23:05:48','2019-04-24 23:05:44','Generated by publish of \'Working on the road? Our new timers update is live!\' at Apr 24, 2019, 11:05 PM','published',1,NULL,'2019-04-24 23:05:48','2019-04-24 23:05:45',0,1),(25,'SilverStripe\\Versioned\\ChangeSet','2019-04-25 23:35:19','2019-04-25 23:35:15','Generated by publish of \'Invoice Template Customization\' at Apr 25, 2019, 11:35 PM','published',1,NULL,'2019-04-25 23:35:19','2019-04-25 23:35:16',0,1),(26,'SilverStripe\\Versioned\\ChangeSet','2019-04-25 23:39:56','2019-04-25 23:39:54','Generated by publish of \'Invoice Template Customization\' at Apr 25, 2019, 11:39 PM','published',1,NULL,'2019-04-25 23:39:56','2019-04-25 23:39:55',0,1);
/*!40000 ALTER TABLE `ChangeSet` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ChangeSetItem`
--

DROP TABLE IF EXISTS `ChangeSetItem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ChangeSetItem` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('SilverStripe\\Versioned\\ChangeSetItem') CHARACTER SET utf8 DEFAULT 'SilverStripe\\Versioned\\ChangeSetItem',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `VersionBefore` int(11) NOT NULL DEFAULT '0',
  `VersionAfter` int(11) NOT NULL DEFAULT '0',
  `Added` enum('explicitly','implicitly') CHARACTER SET utf8 DEFAULT 'implicitly',
  `ChangeSetID` int(11) NOT NULL DEFAULT '0',
  `ObjectID` int(11) NOT NULL DEFAULT '0',
  `ObjectClass` enum('SilverStripe\\Assets\\File','SilverStripe\\SiteConfig\\SiteConfig','SilverStripe\\Versioned\\ChangeSet','SilverStripe\\Versioned\\ChangeSetItem','SilverStripe\\Assets\\Shortcodes\\FileLink','SilverStripe\\Blog\\Model\\BlogCategory','SilverStripe\\Blog\\Model\\BlogTag','SilverStripe\\CMS\\Model\\SiteTree','SilverStripe\\CMS\\Model\\SiteTreeLink','SilverStripe\\Security\\Group','SilverStripe\\Security\\LoginAttempt','SilverStripe\\Security\\Member','SilverStripe\\Security\\MemberPassword','SilverStripe\\Security\\Permission','SilverStripe\\Security\\PermissionRole','SilverStripe\\Security\\PermissionRoleCode','SilverStripe\\Security\\RememberLoginHash','SilverStripe\\Assets\\Folder','SilverStripe\\Assets\\Image','Page','SilverStripe\\ErrorPage\\ErrorPage','SilverStripe\\Blog\\Model\\Blog','SilverStripe\\Blog\\Model\\BlogPost','SilverStripe\\CMS\\Model\\RedirectorPage','SilverStripe\\CMS\\Model\\VirtualPage') CHARACTER SET utf8 DEFAULT 'SilverStripe\\Assets\\File',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ObjectUniquePerChangeSet` (`ObjectID`,`ObjectClass`,`ChangeSetID`),
  KEY `ClassName` (`ClassName`),
  KEY `ChangeSetID` (`ChangeSetID`),
  KEY `Object` (`ObjectID`,`ObjectClass`)
) ENGINE=InnoDB AUTO_INCREMENT=82 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ChangeSetItem`
--

LOCK TABLES `ChangeSetItem` WRITE;
/*!40000 ALTER TABLE `ChangeSetItem` DISABLE KEYS */;
INSERT INTO `ChangeSetItem` VALUES (1,'SilverStripe\\Versioned\\ChangeSetItem','2019-04-18 21:47:51','2019-04-18 21:47:50',0,4,'explicitly',1,7,'SilverStripe\\CMS\\Model\\SiteTree'),(3,'SilverStripe\\Versioned\\ChangeSetItem','2019-04-18 22:03:55','2019-04-18 22:03:54',0,4,'explicitly',3,9,'SilverStripe\\CMS\\Model\\SiteTree'),(10,'SilverStripe\\Versioned\\ChangeSetItem','2019-04-18 23:03:11','2019-04-18 23:03:09',0,4,'explicitly',10,12,'SilverStripe\\CMS\\Model\\SiteTree'),(12,'SilverStripe\\Versioned\\ChangeSetItem','2019-04-20 12:14:27','2019-04-20 12:14:26',0,3,'explicitly',12,14,'SilverStripe\\CMS\\Model\\SiteTree'),(13,'SilverStripe\\Versioned\\ChangeSetItem','2019-04-20 12:28:50','2019-04-20 12:28:40',0,4,'explicitly',13,15,'SilverStripe\\CMS\\Model\\SiteTree'),(14,'SilverStripe\\Versioned\\ChangeSetItem','2019-04-20 12:28:51','2019-04-20 12:28:41',0,2,'implicitly',13,2,'SilverStripe\\Assets\\File'),(15,'SilverStripe\\Versioned\\ChangeSetItem','2019-04-20 12:28:51','2019-04-20 12:28:41',0,0,'implicitly',13,1,'SilverStripe\\Assets\\Shortcodes\\FileLink'),(16,'SilverStripe\\Versioned\\ChangeSetItem','2019-04-20 12:28:52','2019-04-20 12:28:41',0,2,'implicitly',13,3,'SilverStripe\\Assets\\File'),(17,'SilverStripe\\Versioned\\ChangeSetItem','2019-04-20 12:28:53','2019-04-20 12:28:41',0,0,'implicitly',13,2,'SilverStripe\\Assets\\Shortcodes\\FileLink'),(18,'SilverStripe\\Versioned\\ChangeSetItem','2019-04-20 12:28:54','2019-04-20 12:28:41',0,2,'implicitly',13,4,'SilverStripe\\Assets\\File'),(19,'SilverStripe\\Versioned\\ChangeSetItem','2019-04-20 12:28:54','2019-04-20 12:28:42',0,0,'implicitly',13,3,'SilverStripe\\Assets\\Shortcodes\\FileLink'),(20,'SilverStripe\\Versioned\\ChangeSetItem','2019-04-20 12:28:55','2019-04-20 12:28:42',0,2,'implicitly',13,5,'SilverStripe\\Assets\\File'),(21,'SilverStripe\\Versioned\\ChangeSetItem','2019-04-21 10:09:56','2019-04-21 10:09:50',4,6,'explicitly',14,15,'SilverStripe\\CMS\\Model\\SiteTree'),(22,'SilverStripe\\Versioned\\ChangeSetItem','2019-04-21 10:09:56','2019-04-21 10:09:52',2,2,'implicitly',14,2,'SilverStripe\\Assets\\File'),(23,'SilverStripe\\Versioned\\ChangeSetItem','2019-04-21 10:09:56','2019-04-21 10:09:52',0,0,'implicitly',14,1,'SilverStripe\\Assets\\Shortcodes\\FileLink'),(24,'SilverStripe\\Versioned\\ChangeSetItem','2019-04-21 10:09:57','2019-04-21 10:09:52',2,2,'implicitly',14,3,'SilverStripe\\Assets\\File'),(25,'SilverStripe\\Versioned\\ChangeSetItem','2019-04-21 10:09:57','2019-04-21 10:09:52',0,0,'implicitly',14,2,'SilverStripe\\Assets\\Shortcodes\\FileLink'),(26,'SilverStripe\\Versioned\\ChangeSetItem','2019-04-21 10:09:57','2019-04-21 10:09:52',2,2,'implicitly',14,4,'SilverStripe\\Assets\\File'),(27,'SilverStripe\\Versioned\\ChangeSetItem','2019-04-21 10:09:58','2019-04-21 10:09:52',0,0,'implicitly',14,3,'SilverStripe\\Assets\\Shortcodes\\FileLink'),(28,'SilverStripe\\Versioned\\ChangeSetItem','2019-04-21 10:09:58','2019-04-21 10:09:52',2,2,'implicitly',14,5,'SilverStripe\\Assets\\File'),(29,'SilverStripe\\Versioned\\ChangeSetItem','2019-04-21 10:56:35','2019-04-21 10:56:31',6,8,'explicitly',15,15,'SilverStripe\\CMS\\Model\\SiteTree'),(30,'SilverStripe\\Versioned\\ChangeSetItem','2019-04-21 10:56:36','2019-04-21 10:56:32',2,2,'implicitly',15,2,'SilverStripe\\Assets\\File'),(31,'SilverStripe\\Versioned\\ChangeSetItem','2019-04-21 10:56:36','2019-04-21 10:56:32',0,0,'implicitly',15,1,'SilverStripe\\Assets\\Shortcodes\\FileLink'),(32,'SilverStripe\\Versioned\\ChangeSetItem','2019-04-21 10:56:36','2019-04-21 10:56:32',2,2,'implicitly',15,3,'SilverStripe\\Assets\\File'),(33,'SilverStripe\\Versioned\\ChangeSetItem','2019-04-21 10:56:36','2019-04-21 10:56:32',0,0,'implicitly',15,2,'SilverStripe\\Assets\\Shortcodes\\FileLink'),(34,'SilverStripe\\Versioned\\ChangeSetItem','2019-04-21 10:56:37','2019-04-21 10:56:32',2,2,'implicitly',15,4,'SilverStripe\\Assets\\File'),(35,'SilverStripe\\Versioned\\ChangeSetItem','2019-04-21 10:56:37','2019-04-21 10:56:32',0,0,'implicitly',15,3,'SilverStripe\\Assets\\Shortcodes\\FileLink'),(36,'SilverStripe\\Versioned\\ChangeSetItem','2019-04-21 10:56:37','2019-04-21 10:56:33',2,2,'implicitly',15,5,'SilverStripe\\Assets\\File'),(37,'SilverStripe\\Versioned\\ChangeSetItem','2019-04-21 11:49:19','2019-04-21 11:49:16',0,4,'explicitly',16,16,'SilverStripe\\CMS\\Model\\SiteTree'),(38,'SilverStripe\\Versioned\\ChangeSetItem','2019-04-21 11:49:20','2019-04-21 11:49:16',0,2,'implicitly',16,8,'SilverStripe\\Assets\\File'),(39,'SilverStripe\\Versioned\\ChangeSetItem','2019-04-22 23:31:24','2019-04-22 23:31:19',8,10,'explicitly',17,15,'SilverStripe\\CMS\\Model\\SiteTree'),(40,'SilverStripe\\Versioned\\ChangeSetItem','2019-04-22 23:31:25','2019-04-22 23:31:20',0,3,'implicitly',17,9,'SilverStripe\\Assets\\File'),(41,'SilverStripe\\Versioned\\ChangeSetItem','2019-04-22 23:31:25','2019-04-22 23:31:20',0,0,'implicitly',17,4,'SilverStripe\\Assets\\Shortcodes\\FileLink'),(42,'SilverStripe\\Versioned\\ChangeSetItem','2019-04-22 23:31:25','2019-04-22 23:31:21',2,2,'implicitly',17,2,'SilverStripe\\Assets\\File'),(43,'SilverStripe\\Versioned\\ChangeSetItem','2019-04-22 23:31:26','2019-04-22 23:31:21',0,0,'implicitly',17,1,'SilverStripe\\Assets\\Shortcodes\\FileLink'),(44,'SilverStripe\\Versioned\\ChangeSetItem','2019-04-22 23:31:26','2019-04-22 23:31:21',2,2,'implicitly',17,3,'SilverStripe\\Assets\\File'),(45,'SilverStripe\\Versioned\\ChangeSetItem','2019-04-22 23:31:26','2019-04-22 23:31:21',0,0,'implicitly',17,2,'SilverStripe\\Assets\\Shortcodes\\FileLink'),(46,'SilverStripe\\Versioned\\ChangeSetItem','2019-04-22 23:31:26','2019-04-22 23:31:21',2,2,'implicitly',17,4,'SilverStripe\\Assets\\File'),(47,'SilverStripe\\Versioned\\ChangeSetItem','2019-04-22 23:31:27','2019-04-22 23:31:21',0,0,'implicitly',17,3,'SilverStripe\\Assets\\Shortcodes\\FileLink'),(48,'SilverStripe\\Versioned\\ChangeSetItem','2019-04-22 23:31:27','2019-04-22 23:31:21',2,2,'implicitly',17,5,'SilverStripe\\Assets\\File'),(49,'SilverStripe\\Versioned\\ChangeSetItem','2019-04-22 23:33:52','2019-04-22 23:33:47',10,12,'explicitly',18,15,'SilverStripe\\CMS\\Model\\SiteTree'),(50,'SilverStripe\\Versioned\\ChangeSetItem','2019-04-22 23:33:52','2019-04-22 23:33:48',3,3,'implicitly',18,9,'SilverStripe\\Assets\\File'),(51,'SilverStripe\\Versioned\\ChangeSetItem','2019-04-22 23:33:53','2019-04-22 23:33:48',0,0,'implicitly',18,4,'SilverStripe\\Assets\\Shortcodes\\FileLink'),(52,'SilverStripe\\Versioned\\ChangeSetItem','2019-04-22 23:33:53','2019-04-22 23:33:48',2,2,'implicitly',18,2,'SilverStripe\\Assets\\File'),(53,'SilverStripe\\Versioned\\ChangeSetItem','2019-04-22 23:33:53','2019-04-22 23:33:48',0,0,'implicitly',18,1,'SilverStripe\\Assets\\Shortcodes\\FileLink'),(54,'SilverStripe\\Versioned\\ChangeSetItem','2019-04-22 23:33:53','2019-04-22 23:33:49',2,2,'implicitly',18,3,'SilverStripe\\Assets\\File'),(55,'SilverStripe\\Versioned\\ChangeSetItem','2019-04-22 23:33:54','2019-04-22 23:33:49',0,0,'implicitly',18,2,'SilverStripe\\Assets\\Shortcodes\\FileLink'),(56,'SilverStripe\\Versioned\\ChangeSetItem','2019-04-22 23:33:54','2019-04-22 23:33:49',2,2,'implicitly',18,4,'SilverStripe\\Assets\\File'),(57,'SilverStripe\\Versioned\\ChangeSetItem','2019-04-22 23:33:54','2019-04-22 23:33:49',0,0,'implicitly',18,3,'SilverStripe\\Assets\\Shortcodes\\FileLink'),(58,'SilverStripe\\Versioned\\ChangeSetItem','2019-04-22 23:33:54','2019-04-22 23:33:49',2,2,'implicitly',18,5,'SilverStripe\\Assets\\File'),(59,'SilverStripe\\Versioned\\ChangeSetItem','2019-04-22 23:41:32','2019-04-22 23:41:27',12,14,'explicitly',19,15,'SilverStripe\\CMS\\Model\\SiteTree'),(60,'SilverStripe\\Versioned\\ChangeSetItem','2019-04-22 23:41:32','2019-04-22 23:41:28',3,3,'implicitly',19,9,'SilverStripe\\Assets\\File'),(61,'SilverStripe\\Versioned\\ChangeSetItem','2019-04-22 23:41:33','2019-04-22 23:41:28',0,0,'implicitly',19,4,'SilverStripe\\Assets\\Shortcodes\\FileLink'),(62,'SilverStripe\\Versioned\\ChangeSetItem','2019-04-22 23:41:33','2019-04-22 23:41:29',2,2,'implicitly',19,2,'SilverStripe\\Assets\\File'),(63,'SilverStripe\\Versioned\\ChangeSetItem','2019-04-22 23:41:33','2019-04-22 23:41:29',0,0,'implicitly',19,1,'SilverStripe\\Assets\\Shortcodes\\FileLink'),(64,'SilverStripe\\Versioned\\ChangeSetItem','2019-04-22 23:41:33','2019-04-22 23:41:29',2,2,'implicitly',19,3,'SilverStripe\\Assets\\File'),(65,'SilverStripe\\Versioned\\ChangeSetItem','2019-04-22 23:41:34','2019-04-22 23:41:29',0,0,'implicitly',19,2,'SilverStripe\\Assets\\Shortcodes\\FileLink'),(66,'SilverStripe\\Versioned\\ChangeSetItem','2019-04-22 23:41:34','2019-04-22 23:41:29',2,2,'implicitly',19,4,'SilverStripe\\Assets\\File'),(67,'SilverStripe\\Versioned\\ChangeSetItem','2019-04-22 23:41:34','2019-04-22 23:41:29',0,0,'implicitly',19,3,'SilverStripe\\Assets\\Shortcodes\\FileLink'),(68,'SilverStripe\\Versioned\\ChangeSetItem','2019-04-22 23:41:34','2019-04-22 23:41:29',2,2,'implicitly',19,5,'SilverStripe\\Assets\\File'),(69,'SilverStripe\\Versioned\\ChangeSetItem','2019-04-24 00:31:05','2019-04-24 00:31:04',0,3,'explicitly',20,10,'SilverStripe\\Assets\\File'),(70,'SilverStripe\\Versioned\\ChangeSetItem','2019-04-24 00:33:44','2019-04-24 00:33:42',0,4,'explicitly',21,17,'SilverStripe\\CMS\\Model\\SiteTree'),(71,'SilverStripe\\Versioned\\ChangeSetItem','2019-04-24 00:33:44','2019-04-24 00:33:42',3,3,'implicitly',21,10,'SilverStripe\\Assets\\File'),(72,'SilverStripe\\Versioned\\ChangeSetItem','2019-04-24 00:48:37','2019-04-24 00:48:34',0,4,'explicitly',22,18,'SilverStripe\\CMS\\Model\\SiteTree'),(73,'SilverStripe\\Versioned\\ChangeSetItem','2019-04-24 00:48:38','2019-04-24 00:48:35',0,2,'implicitly',22,11,'SilverStripe\\Assets\\File'),(74,'SilverStripe\\Versioned\\ChangeSetItem','2019-04-24 13:31:04','2019-04-24 13:31:02',0,4,'explicitly',23,19,'SilverStripe\\CMS\\Model\\SiteTree'),(75,'SilverStripe\\Versioned\\ChangeSetItem','2019-04-24 13:31:05','2019-04-24 13:31:02',0,2,'implicitly',23,14,'SilverStripe\\Assets\\File'),(76,'SilverStripe\\Versioned\\ChangeSetItem','2019-04-24 23:05:47','2019-04-24 23:05:44',0,4,'explicitly',24,20,'SilverStripe\\CMS\\Model\\SiteTree'),(77,'SilverStripe\\Versioned\\ChangeSetItem','2019-04-24 23:05:48','2019-04-24 23:05:45',0,2,'implicitly',24,15,'SilverStripe\\Assets\\File'),(78,'SilverStripe\\Versioned\\ChangeSetItem','2019-04-25 23:35:18','2019-04-25 23:35:15',0,4,'explicitly',25,21,'SilverStripe\\CMS\\Model\\SiteTree'),(79,'SilverStripe\\Versioned\\ChangeSetItem','2019-04-25 23:35:18','2019-04-25 23:35:16',0,2,'implicitly',25,16,'SilverStripe\\Assets\\File'),(80,'SilverStripe\\Versioned\\ChangeSetItem','2019-04-25 23:39:56','2019-04-25 23:39:55',4,4,'explicitly',26,21,'SilverStripe\\CMS\\Model\\SiteTree'),(81,'SilverStripe\\Versioned\\ChangeSetItem','2019-04-25 23:39:56','2019-04-25 23:39:55',2,2,'implicitly',26,16,'SilverStripe\\Assets\\File');
/*!40000 ALTER TABLE `ChangeSetItem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ChangeSetItem_ReferencedBy`
--

DROP TABLE IF EXISTS `ChangeSetItem_ReferencedBy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ChangeSetItem_ReferencedBy` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ChangeSetItemID` int(11) NOT NULL DEFAULT '0',
  `ChildID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `ChangeSetItemID` (`ChangeSetItemID`),
  KEY `ChildID` (`ChildID`)
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ChangeSetItem_ReferencedBy`
--

LOCK TABLES `ChangeSetItem_ReferencedBy` WRITE;
/*!40000 ALTER TABLE `ChangeSetItem_ReferencedBy` DISABLE KEYS */;
INSERT INTO `ChangeSetItem_ReferencedBy` VALUES (1,14,13),(2,15,13),(3,16,13),(4,17,13),(5,18,13),(6,19,13),(7,20,13),(8,22,21),(9,23,21),(10,24,21),(11,25,21),(12,26,21),(13,27,21),(14,28,21),(15,30,29),(16,31,29),(17,32,29),(18,33,29),(19,34,29),(20,35,29),(21,36,29),(22,38,37),(23,40,39),(24,41,39),(25,42,39),(26,43,39),(27,44,39),(28,45,39),(29,46,39),(30,47,39),(31,48,39),(32,50,49),(33,51,49),(34,52,49),(35,53,49),(36,54,49),(37,55,49),(38,56,49),(39,57,49),(40,58,49),(41,60,59),(42,61,59),(43,62,59),(44,63,59),(45,64,59),(46,65,59),(47,66,59),(48,67,59),(49,68,59),(50,71,70),(51,73,72),(52,75,74),(53,77,76),(54,79,78),(55,81,80);
/*!40000 ALTER TABLE `ChangeSetItem_ReferencedBy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ErrorPage`
--

DROP TABLE IF EXISTS `ErrorPage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ErrorPage` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ErrorCode` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ErrorPage`
--

LOCK TABLES `ErrorPage` WRITE;
/*!40000 ALTER TABLE `ErrorPage` DISABLE KEYS */;
INSERT INTO `ErrorPage` VALUES (4,404),(5,500);
/*!40000 ALTER TABLE `ErrorPage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ErrorPage_Live`
--

DROP TABLE IF EXISTS `ErrorPage_Live`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ErrorPage_Live` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ErrorCode` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ErrorPage_Live`
--

LOCK TABLES `ErrorPage_Live` WRITE;
/*!40000 ALTER TABLE `ErrorPage_Live` DISABLE KEYS */;
INSERT INTO `ErrorPage_Live` VALUES (4,404),(5,500);
/*!40000 ALTER TABLE `ErrorPage_Live` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ErrorPage_Versions`
--

DROP TABLE IF EXISTS `ErrorPage_Versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ErrorPage_Versions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `ErrorCode` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  KEY `RecordID` (`RecordID`),
  KEY `Version` (`Version`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ErrorPage_Versions`
--

LOCK TABLES `ErrorPage_Versions` WRITE;
/*!40000 ALTER TABLE `ErrorPage_Versions` DISABLE KEYS */;
INSERT INTO `ErrorPage_Versions` VALUES (1,4,1,404),(2,4,2,404),(3,5,1,500),(4,5,2,500);
/*!40000 ALTER TABLE `ErrorPage_Versions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `File`
--

DROP TABLE IF EXISTS `File`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `File` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('SilverStripe\\Assets\\File','SilverStripe\\Assets\\Folder','SilverStripe\\Assets\\Image') CHARACTER SET utf8 DEFAULT 'SilverStripe\\Assets\\File',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Version` int(11) NOT NULL DEFAULT '0',
  `CanViewType` enum('Anyone','LoggedInUsers','OnlyTheseUsers','Inherit') CHARACTER SET utf8 DEFAULT 'Inherit',
  `CanEditType` enum('LoggedInUsers','OnlyTheseUsers','Inherit') CHARACTER SET utf8 DEFAULT 'Inherit',
  `Name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Title` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `ShowInSearch` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `ParentID` int(11) NOT NULL DEFAULT '0',
  `OwnerID` int(11) NOT NULL DEFAULT '0',
  `FileHash` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `FileFilename` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `FileVariant` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `Name` (`Name`),
  KEY `ClassName` (`ClassName`),
  KEY `ParentID` (`ParentID`),
  KEY `OwnerID` (`OwnerID`),
  KEY `FileHash` (`FileHash`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `File`
--

LOCK TABLES `File` WRITE;
/*!40000 ALTER TABLE `File` DISABLE KEYS */;
INSERT INTO `File` VALUES (1,'SilverStripe\\Assets\\Folder','2019-04-18 21:47:26','2019-04-18 21:47:25',2,'Inherit','Inherit','Uploads','Uploads',1,0,1,NULL,NULL,NULL),(2,'SilverStripe\\Assets\\Image','2019-04-20 12:28:50','2019-04-20 12:19:32',2,'Inherit','Inherit','link_oot.png','link oot',1,1,1,'e8fa9273b6eb52a9c1fc43f9071b5e8ea01f00c5','Uploads/link_oot.png',NULL),(3,'SilverStripe\\Assets\\Image','2019-04-20 12:28:52','2019-04-20 12:19:38',2,'Inherit','Inherit','mario_64-.png','mario 64 ',1,1,1,'65114dbce061c979c7871ca1c16bae00098dc0dc','Uploads/mario_64-.png',NULL),(4,'SilverStripe\\Assets\\Image','2019-04-20 12:28:53','2019-04-20 12:19:47',2,'Inherit','Inherit','wave_race.png','wave race',1,1,1,'5e477476bb7acf88573cd181cc0b1097a8f273d8','Uploads/wave_race.png',NULL),(5,'SilverStripe\\Assets\\Image','2019-04-20 12:28:54','2019-04-20 12:20:45',2,'Inherit','Inherit','jaromir-kavan-296728-unsplash.jpg','jaromir kavan 296728 unsplash',1,1,1,'cbc20fc1498092efb10105661b15c1881ae2093d','Uploads/jaromir-kavan-296728-unsplash.jpg',NULL),(6,'SilverStripe\\Assets\\Image','2019-04-21 11:47:46','2019-04-21 11:47:46',1,'Inherit','Inherit','sarah-dorweiler-211779-unsplash.jpg','sarah dorweiler 211779 unsplash',1,1,1,'2f91b761554940e0cdb6fc889a3e0352eb6e651e','Uploads/sarah-dorweiler-211779-unsplash.jpg',NULL),(7,'SilverStripe\\Assets\\Image','2019-04-21 11:48:06','2019-04-21 11:48:06',1,'Inherit','Inherit','sarah-dorweiler-211779-unsplash-v2.jpg','sarah dorweiler 211779 unsplash v2',1,1,1,'2f91b761554940e0cdb6fc889a3e0352eb6e651e','Uploads/sarah-dorweiler-211779-unsplash-v2.jpg',NULL),(8,'SilverStripe\\Assets\\Image','2019-04-21 11:49:19','2019-04-21 11:49:01',2,'Inherit','Inherit','jo-szczepanska-1163258-unsplash.jpg','jo szczepanska 1163258 unsplash',1,1,1,'e37015ca8cf4a452d990114ac54475c7583a121e','Uploads/jo-szczepanska-1163258-unsplash.jpg',NULL),(9,'SilverStripe\\Assets\\Image','2019-04-22 23:31:24','2019-04-22 23:29:52',3,'Inherit','Inherit','king-kong.jpg','king kong',1,0,1,'a8a148aa9e81c3ac41b2d0aa296c97d60f86c621','king-kong.jpg',NULL),(10,'SilverStripe\\Assets\\Image','2019-04-24 00:31:05','2019-04-24 00:29:49',3,'Inherit','Inherit','fancycrave-371075-unsplash.jpg','fancycrave 371075 unsplash',1,1,1,'2fa54d24ba2664f2dd05f137523e71c8a2f1f331','Uploads/fancycrave-371075-unsplash.jpg',NULL),(11,'SilverStripe\\Assets\\Image','2019-04-24 00:48:38','2019-04-24 00:47:52',2,'Inherit','Inherit','jo-szczepanska-1163258-unsplash-v2.jpg','jo szczepanska 1163258 unsplash v2',1,1,1,'e37015ca8cf4a452d990114ac54475c7583a121e','Uploads/jo-szczepanska-1163258-unsplash-v2.jpg',NULL),(12,'SilverStripe\\Assets\\Image','2019-04-24 13:29:16','2019-04-24 13:29:16',1,'Inherit','Inherit','sonja-langford-357-unsplash.jpg','sonja langford 357 unsplash',1,1,1,'2bfd104fc44b906fb61ceabedbe86f766c829f5e','Uploads/sonja-langford-357-unsplash.jpg',NULL),(13,'SilverStripe\\Assets\\Image','2019-04-24 13:29:37','2019-04-24 13:29:37',1,'Inherit','Inherit','sonja-langford-357-unsplashz.jpg','sonja langford 357 unsplashz',1,1,1,'2bfd104fc44b906fb61ceabedbe86f766c829f5e','Uploads/sonja-langford-357-unsplashz.jpg',NULL),(14,'SilverStripe\\Assets\\Image','2019-04-24 13:31:04','2019-04-24 13:29:53',2,'Inherit','Inherit','annie-spratt-165447-unsplash.jpg','annie spratt 165447 unsplash',1,1,1,'aaa60d70bdc153f5c3de225aca581bf5804a2e0d','Uploads/annie-spratt-165447-unsplash.jpg',NULL),(15,'SilverStripe\\Assets\\Image','2019-04-24 23:05:47','2019-04-24 23:05:20',2,'Inherit','Inherit','emmanuel-kontokalos-652241-unsplash.jpg','emmanuel kontokalos 652241 unsplash',1,1,1,'54c5fdab85a07670c118b0b460edab8ef15fda3c','Uploads/emmanuel-kontokalos-652241-unsplash.jpg',NULL),(16,'SilverStripe\\Assets\\Image','2019-04-25 23:35:18','2019-04-25 23:34:48',2,'Inherit','Inherit','domenico-loia-310197-unsplash.jpg','domenico loia 310197 unsplash',1,1,1,'45279db1cbb1047f201f1885eb9f8c40d9eeaf36','Uploads/domenico-loia-310197-unsplash.jpg',NULL);
/*!40000 ALTER TABLE `File` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `FileLink`
--

DROP TABLE IF EXISTS `FileLink`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `FileLink` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('SilverStripe\\Assets\\Shortcodes\\FileLink') CHARACTER SET utf8 DEFAULT 'SilverStripe\\Assets\\Shortcodes\\FileLink',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `LinkedID` int(11) NOT NULL DEFAULT '0',
  `ParentID` int(11) NOT NULL DEFAULT '0',
  `ParentClass` enum('SilverStripe\\Assets\\File','SilverStripe\\SiteConfig\\SiteConfig','SilverStripe\\Versioned\\ChangeSet','SilverStripe\\Versioned\\ChangeSetItem','SilverStripe\\Assets\\Shortcodes\\FileLink','SilverStripe\\Blog\\Model\\BlogCategory','SilverStripe\\Blog\\Model\\BlogTag','SilverStripe\\CMS\\Model\\SiteTree','SilverStripe\\CMS\\Model\\SiteTreeLink','SilverStripe\\Security\\Group','SilverStripe\\Security\\LoginAttempt','SilverStripe\\Security\\Member','SilverStripe\\Security\\MemberPassword','SilverStripe\\Security\\Permission','SilverStripe\\Security\\PermissionRole','SilverStripe\\Security\\PermissionRoleCode','SilverStripe\\Security\\RememberLoginHash','SilverStripe\\Assets\\Folder','SilverStripe\\Assets\\Image','Page','SilverStripe\\ErrorPage\\ErrorPage','SilverStripe\\Blog\\Model\\Blog','SilverStripe\\Blog\\Model\\BlogPost','SilverStripe\\CMS\\Model\\RedirectorPage','SilverStripe\\CMS\\Model\\VirtualPage') CHARACTER SET utf8 DEFAULT 'SilverStripe\\Assets\\File',
  PRIMARY KEY (`ID`),
  KEY `ClassName` (`ClassName`),
  KEY `LinkedID` (`LinkedID`),
  KEY `Parent` (`ParentID`,`ParentClass`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `FileLink`
--

LOCK TABLES `FileLink` WRITE;
/*!40000 ALTER TABLE `FileLink` DISABLE KEYS */;
INSERT INTO `FileLink` VALUES (1,'SilverStripe\\Assets\\Shortcodes\\FileLink','2019-04-20 12:28:36','2019-04-20 12:28:36',2,15,'SilverStripe\\Blog\\Model\\BlogPost'),(2,'SilverStripe\\Assets\\Shortcodes\\FileLink','2019-04-20 12:28:37','2019-04-20 12:28:37',3,15,'SilverStripe\\Blog\\Model\\BlogPost'),(3,'SilverStripe\\Assets\\Shortcodes\\FileLink','2019-04-20 12:28:37','2019-04-20 12:28:37',4,15,'SilverStripe\\Blog\\Model\\BlogPost'),(4,'SilverStripe\\Assets\\Shortcodes\\FileLink','2019-04-22 23:31:17','2019-04-22 23:31:17',9,15,'SilverStripe\\Blog\\Model\\BlogPost');
/*!40000 ALTER TABLE `FileLink` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `File_EditorGroups`
--

DROP TABLE IF EXISTS `File_EditorGroups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `File_EditorGroups` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `FileID` int(11) NOT NULL DEFAULT '0',
  `GroupID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `FileID` (`FileID`),
  KEY `GroupID` (`GroupID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `File_EditorGroups`
--

LOCK TABLES `File_EditorGroups` WRITE;
/*!40000 ALTER TABLE `File_EditorGroups` DISABLE KEYS */;
/*!40000 ALTER TABLE `File_EditorGroups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `File_Live`
--

DROP TABLE IF EXISTS `File_Live`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `File_Live` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('SilverStripe\\Assets\\File','SilverStripe\\Assets\\Folder','SilverStripe\\Assets\\Image') CHARACTER SET utf8 DEFAULT 'SilverStripe\\Assets\\File',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Version` int(11) NOT NULL DEFAULT '0',
  `CanViewType` enum('Anyone','LoggedInUsers','OnlyTheseUsers','Inherit') CHARACTER SET utf8 DEFAULT 'Inherit',
  `CanEditType` enum('LoggedInUsers','OnlyTheseUsers','Inherit') CHARACTER SET utf8 DEFAULT 'Inherit',
  `Name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Title` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `ShowInSearch` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `ParentID` int(11) NOT NULL DEFAULT '0',
  `OwnerID` int(11) NOT NULL DEFAULT '0',
  `FileHash` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `FileFilename` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `FileVariant` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `Name` (`Name`),
  KEY `ClassName` (`ClassName`),
  KEY `ParentID` (`ParentID`),
  KEY `OwnerID` (`OwnerID`),
  KEY `FileHash` (`FileHash`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `File_Live`
--

LOCK TABLES `File_Live` WRITE;
/*!40000 ALTER TABLE `File_Live` DISABLE KEYS */;
INSERT INTO `File_Live` VALUES (1,'SilverStripe\\Assets\\Folder','2019-04-18 21:47:26','2019-04-18 21:47:25',2,'Inherit','Inherit','Uploads','Uploads',1,0,1,NULL,NULL,NULL),(2,'SilverStripe\\Assets\\Image','2019-04-20 12:28:50','2019-04-20 12:19:32',2,'Inherit','Inherit','link_oot.png','link oot',1,1,1,'e8fa9273b6eb52a9c1fc43f9071b5e8ea01f00c5','Uploads/link_oot.png',NULL),(3,'SilverStripe\\Assets\\Image','2019-04-20 12:28:52','2019-04-20 12:19:38',2,'Inherit','Inherit','mario_64-.png','mario 64 ',1,1,1,'65114dbce061c979c7871ca1c16bae00098dc0dc','Uploads/mario_64-.png',NULL),(4,'SilverStripe\\Assets\\Image','2019-04-20 12:28:53','2019-04-20 12:19:47',2,'Inherit','Inherit','wave_race.png','wave race',1,1,1,'5e477476bb7acf88573cd181cc0b1097a8f273d8','Uploads/wave_race.png',NULL),(5,'SilverStripe\\Assets\\Image','2019-04-20 12:28:54','2019-04-20 12:20:45',2,'Inherit','Inherit','jaromir-kavan-296728-unsplash.jpg','jaromir kavan 296728 unsplash',1,1,1,'cbc20fc1498092efb10105661b15c1881ae2093d','Uploads/jaromir-kavan-296728-unsplash.jpg',NULL),(8,'SilverStripe\\Assets\\Image','2019-04-21 11:49:19','2019-04-21 11:49:01',2,'Inherit','Inherit','jo-szczepanska-1163258-unsplash.jpg','jo szczepanska 1163258 unsplash',1,1,1,'e37015ca8cf4a452d990114ac54475c7583a121e','Uploads/jo-szczepanska-1163258-unsplash.jpg',NULL),(9,'SilverStripe\\Assets\\Image','2019-04-22 23:31:24','2019-04-22 23:29:52',3,'Inherit','Inherit','king-kong.jpg','king kong',1,0,1,'a8a148aa9e81c3ac41b2d0aa296c97d60f86c621','king-kong.jpg',NULL),(10,'SilverStripe\\Assets\\Image','2019-04-24 00:31:05','2019-04-24 00:29:49',3,'Inherit','Inherit','fancycrave-371075-unsplash.jpg','fancycrave 371075 unsplash',1,1,1,'2fa54d24ba2664f2dd05f137523e71c8a2f1f331','Uploads/fancycrave-371075-unsplash.jpg',NULL),(11,'SilverStripe\\Assets\\Image','2019-04-24 00:48:38','2019-04-24 00:47:52',2,'Inherit','Inherit','jo-szczepanska-1163258-unsplash-v2.jpg','jo szczepanska 1163258 unsplash v2',1,1,1,'e37015ca8cf4a452d990114ac54475c7583a121e','Uploads/jo-szczepanska-1163258-unsplash-v2.jpg',NULL),(14,'SilverStripe\\Assets\\Image','2019-04-24 13:31:04','2019-04-24 13:29:53',2,'Inherit','Inherit','annie-spratt-165447-unsplash.jpg','annie spratt 165447 unsplash',1,1,1,'aaa60d70bdc153f5c3de225aca581bf5804a2e0d','Uploads/annie-spratt-165447-unsplash.jpg',NULL),(15,'SilverStripe\\Assets\\Image','2019-04-24 23:05:47','2019-04-24 23:05:20',2,'Inherit','Inherit','emmanuel-kontokalos-652241-unsplash.jpg','emmanuel kontokalos 652241 unsplash',1,1,1,'54c5fdab85a07670c118b0b460edab8ef15fda3c','Uploads/emmanuel-kontokalos-652241-unsplash.jpg',NULL),(16,'SilverStripe\\Assets\\Image','2019-04-25 23:35:18','2019-04-25 23:34:48',2,'Inherit','Inherit','domenico-loia-310197-unsplash.jpg','domenico loia 310197 unsplash',1,1,1,'45279db1cbb1047f201f1885eb9f8c40d9eeaf36','Uploads/domenico-loia-310197-unsplash.jpg',NULL);
/*!40000 ALTER TABLE `File_Live` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `File_Versions`
--

DROP TABLE IF EXISTS `File_Versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `File_Versions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `WasPublished` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `WasDeleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `WasDraft` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `AuthorID` int(11) NOT NULL DEFAULT '0',
  `PublisherID` int(11) NOT NULL DEFAULT '0',
  `ClassName` enum('SilverStripe\\Assets\\File','SilverStripe\\Assets\\Folder','SilverStripe\\Assets\\Image') CHARACTER SET utf8 DEFAULT 'SilverStripe\\Assets\\File',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `CanViewType` enum('Anyone','LoggedInUsers','OnlyTheseUsers','Inherit') CHARACTER SET utf8 DEFAULT 'Inherit',
  `CanEditType` enum('LoggedInUsers','OnlyTheseUsers','Inherit') CHARACTER SET utf8 DEFAULT 'Inherit',
  `Name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Title` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `ShowInSearch` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `ParentID` int(11) NOT NULL DEFAULT '0',
  `OwnerID` int(11) NOT NULL DEFAULT '0',
  `FileHash` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `FileFilename` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `FileVariant` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `RecordID_Version` (`RecordID`,`Version`),
  KEY `RecordID` (`RecordID`),
  KEY `Version` (`Version`),
  KEY `AuthorID` (`AuthorID`),
  KEY `PublisherID` (`PublisherID`),
  KEY `Name` (`Name`),
  KEY `ClassName` (`ClassName`),
  KEY `ParentID` (`ParentID`),
  KEY `OwnerID` (`OwnerID`),
  KEY `FileHash` (`FileHash`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `File_Versions`
--

LOCK TABLES `File_Versions` WRITE;
/*!40000 ALTER TABLE `File_Versions` DISABLE KEYS */;
INSERT INTO `File_Versions` VALUES (1,1,1,0,0,1,1,0,'SilverStripe\\Assets\\Folder','2019-04-18 21:47:25','2019-04-18 21:47:25','Inherit','Inherit','Uploads','Uploads',1,0,1,NULL,NULL,NULL),(2,1,2,1,0,1,1,1,'SilverStripe\\Assets\\Folder','2019-04-18 21:47:26','2019-04-18 21:47:25','Inherit','Inherit','Uploads','Uploads',1,0,1,NULL,NULL,NULL),(3,2,1,0,0,1,1,0,'SilverStripe\\Assets\\Image','2019-04-20 12:19:32','2019-04-20 12:19:32','Inherit','Inherit','link_oot.png','link oot',1,1,1,'e8fa9273b6eb52a9c1fc43f9071b5e8ea01f00c5','Uploads/link_oot.png',NULL),(4,3,1,0,0,1,1,0,'SilverStripe\\Assets\\Image','2019-04-20 12:19:38','2019-04-20 12:19:38','Inherit','Inherit','mario_64-.png','mario 64 ',1,1,1,'65114dbce061c979c7871ca1c16bae00098dc0dc','Uploads/mario_64-.png',NULL),(5,4,1,0,0,1,1,0,'SilverStripe\\Assets\\Image','2019-04-20 12:19:47','2019-04-20 12:19:47','Inherit','Inherit','wave_race.png','wave race',1,1,1,'5e477476bb7acf88573cd181cc0b1097a8f273d8','Uploads/wave_race.png',NULL),(6,5,1,0,0,1,1,0,'SilverStripe\\Assets\\Image','2019-04-20 12:20:45','2019-04-20 12:20:45','Inherit','Inherit','jaromir-kavan-296728-unsplash.jpg','jaromir kavan 296728 unsplash',1,1,1,'cbc20fc1498092efb10105661b15c1881ae2093d','Uploads/jaromir-kavan-296728-unsplash.jpg',NULL),(7,2,2,1,0,1,1,1,'SilverStripe\\Assets\\Image','2019-04-20 12:28:50','2019-04-20 12:19:32','Inherit','Inherit','link_oot.png','link oot',1,1,1,'e8fa9273b6eb52a9c1fc43f9071b5e8ea01f00c5','Uploads/link_oot.png',NULL),(8,3,2,1,0,1,1,1,'SilverStripe\\Assets\\Image','2019-04-20 12:28:52','2019-04-20 12:19:38','Inherit','Inherit','mario_64-.png','mario 64 ',1,1,1,'65114dbce061c979c7871ca1c16bae00098dc0dc','Uploads/mario_64-.png',NULL),(9,4,2,1,0,1,1,1,'SilverStripe\\Assets\\Image','2019-04-20 12:28:53','2019-04-20 12:19:47','Inherit','Inherit','wave_race.png','wave race',1,1,1,'5e477476bb7acf88573cd181cc0b1097a8f273d8','Uploads/wave_race.png',NULL),(10,5,2,1,0,1,1,1,'SilverStripe\\Assets\\Image','2019-04-20 12:28:54','2019-04-20 12:20:45','Inherit','Inherit','jaromir-kavan-296728-unsplash.jpg','jaromir kavan 296728 unsplash',1,1,1,'cbc20fc1498092efb10105661b15c1881ae2093d','Uploads/jaromir-kavan-296728-unsplash.jpg',NULL),(11,6,1,0,0,1,1,0,'SilverStripe\\Assets\\Image','2019-04-21 11:47:46','2019-04-21 11:47:46','Inherit','Inherit','sarah-dorweiler-211779-unsplash.jpg','sarah dorweiler 211779 unsplash',1,1,1,'2f91b761554940e0cdb6fc889a3e0352eb6e651e','Uploads/sarah-dorweiler-211779-unsplash.jpg',NULL),(12,7,1,0,0,1,1,0,'SilverStripe\\Assets\\Image','2019-04-21 11:48:06','2019-04-21 11:48:06','Inherit','Inherit','sarah-dorweiler-211779-unsplash-v2.jpg','sarah dorweiler 211779 unsplash v2',1,1,1,'2f91b761554940e0cdb6fc889a3e0352eb6e651e','Uploads/sarah-dorweiler-211779-unsplash-v2.jpg',NULL),(13,8,1,0,0,1,1,0,'SilverStripe\\Assets\\Image','2019-04-21 11:49:01','2019-04-21 11:49:01','Inherit','Inherit','jo-szczepanska-1163258-unsplash.jpg','jo szczepanska 1163258 unsplash',1,1,1,'e37015ca8cf4a452d990114ac54475c7583a121e','Uploads/jo-szczepanska-1163258-unsplash.jpg',NULL),(14,8,2,1,0,1,1,1,'SilverStripe\\Assets\\Image','2019-04-21 11:49:19','2019-04-21 11:49:01','Inherit','Inherit','jo-szczepanska-1163258-unsplash.jpg','jo szczepanska 1163258 unsplash',1,1,1,'e37015ca8cf4a452d990114ac54475c7583a121e','Uploads/jo-szczepanska-1163258-unsplash.jpg',NULL),(15,9,1,0,0,1,1,0,'SilverStripe\\Assets\\Image','2019-04-22 23:29:52','2019-04-22 23:29:52','Inherit','Inherit','king-kong.jpg','king kong',1,0,1,'a8a148aa9e81c3ac41b2d0aa296c97d60f86c621','king-kong.jpg',NULL),(16,9,2,0,0,1,1,0,'SilverStripe\\Assets\\Image','2019-04-22 23:29:53','2019-04-22 23:29:52','Inherit','Inherit','king-kong.jpg','king kong',1,0,1,'a8a148aa9e81c3ac41b2d0aa296c97d60f86c621','king-kong.jpg',NULL),(17,9,3,1,0,1,1,1,'SilverStripe\\Assets\\Image','2019-04-22 23:31:24','2019-04-22 23:29:52','Inherit','Inherit','king-kong.jpg','king kong',1,0,1,'a8a148aa9e81c3ac41b2d0aa296c97d60f86c621','king-kong.jpg',NULL),(18,10,1,0,0,1,1,0,'SilverStripe\\Assets\\Image','2019-04-24 00:29:49','2019-04-24 00:29:49','Inherit','Inherit','fancycrave-371075-unsplash.jpg','fancycrave 371075 unsplash',1,1,1,'2fa54d24ba2664f2dd05f137523e71c8a2f1f331','Uploads/fancycrave-371075-unsplash.jpg',NULL),(19,10,2,0,0,1,1,0,'SilverStripe\\Assets\\Image','2019-04-24 00:31:03','2019-04-24 00:29:49','Inherit','Inherit','fancycrave-371075-unsplash.jpg','fancycrave 371075 unsplash',1,1,1,'2fa54d24ba2664f2dd05f137523e71c8a2f1f331','Uploads/fancycrave-371075-unsplash.jpg',NULL),(20,10,3,1,0,1,1,1,'SilverStripe\\Assets\\Image','2019-04-24 00:31:05','2019-04-24 00:29:49','Inherit','Inherit','fancycrave-371075-unsplash.jpg','fancycrave 371075 unsplash',1,1,1,'2fa54d24ba2664f2dd05f137523e71c8a2f1f331','Uploads/fancycrave-371075-unsplash.jpg',NULL),(21,11,1,0,0,1,1,0,'SilverStripe\\Assets\\Image','2019-04-24 00:47:52','2019-04-24 00:47:52','Inherit','Inherit','jo-szczepanska-1163258-unsplash-v2.jpg','jo szczepanska 1163258 unsplash v2',1,1,1,'e37015ca8cf4a452d990114ac54475c7583a121e','Uploads/jo-szczepanska-1163258-unsplash-v2.jpg',NULL),(22,11,2,1,0,1,1,1,'SilverStripe\\Assets\\Image','2019-04-24 00:48:38','2019-04-24 00:47:52','Inherit','Inherit','jo-szczepanska-1163258-unsplash-v2.jpg','jo szczepanska 1163258 unsplash v2',1,1,1,'e37015ca8cf4a452d990114ac54475c7583a121e','Uploads/jo-szczepanska-1163258-unsplash-v2.jpg',NULL),(23,12,1,0,0,1,1,0,'SilverStripe\\Assets\\Image','2019-04-24 13:29:16','2019-04-24 13:29:16','Inherit','Inherit','sonja-langford-357-unsplash.jpg','sonja langford 357 unsplash',1,1,1,'2bfd104fc44b906fb61ceabedbe86f766c829f5e','Uploads/sonja-langford-357-unsplash.jpg',NULL),(24,13,1,0,0,1,1,0,'SilverStripe\\Assets\\Image','2019-04-24 13:29:37','2019-04-24 13:29:37','Inherit','Inherit','sonja-langford-357-unsplashz.jpg','sonja langford 357 unsplashz',1,1,1,'2bfd104fc44b906fb61ceabedbe86f766c829f5e','Uploads/sonja-langford-357-unsplashz.jpg',NULL),(25,14,1,0,0,1,1,0,'SilverStripe\\Assets\\Image','2019-04-24 13:29:53','2019-04-24 13:29:53','Inherit','Inherit','annie-spratt-165447-unsplash.jpg','annie spratt 165447 unsplash',1,1,1,'aaa60d70bdc153f5c3de225aca581bf5804a2e0d','Uploads/annie-spratt-165447-unsplash.jpg',NULL),(26,14,2,1,0,1,1,1,'SilverStripe\\Assets\\Image','2019-04-24 13:31:04','2019-04-24 13:29:53','Inherit','Inherit','annie-spratt-165447-unsplash.jpg','annie spratt 165447 unsplash',1,1,1,'aaa60d70bdc153f5c3de225aca581bf5804a2e0d','Uploads/annie-spratt-165447-unsplash.jpg',NULL),(27,15,1,0,0,1,1,0,'SilverStripe\\Assets\\Image','2019-04-24 23:05:20','2019-04-24 23:05:20','Inherit','Inherit','emmanuel-kontokalos-652241-unsplash.jpg','emmanuel kontokalos 652241 unsplash',1,1,1,'54c5fdab85a07670c118b0b460edab8ef15fda3c','Uploads/emmanuel-kontokalos-652241-unsplash.jpg',NULL),(28,15,2,1,0,1,1,1,'SilverStripe\\Assets\\Image','2019-04-24 23:05:47','2019-04-24 23:05:20','Inherit','Inherit','emmanuel-kontokalos-652241-unsplash.jpg','emmanuel kontokalos 652241 unsplash',1,1,1,'54c5fdab85a07670c118b0b460edab8ef15fda3c','Uploads/emmanuel-kontokalos-652241-unsplash.jpg',NULL),(29,16,1,0,0,1,1,0,'SilverStripe\\Assets\\Image','2019-04-25 23:34:48','2019-04-25 23:34:48','Inherit','Inherit','domenico-loia-310197-unsplash.jpg','domenico loia 310197 unsplash',1,1,1,'45279db1cbb1047f201f1885eb9f8c40d9eeaf36','Uploads/domenico-loia-310197-unsplash.jpg',NULL),(30,16,2,1,0,1,1,1,'SilverStripe\\Assets\\Image','2019-04-25 23:35:18','2019-04-25 23:34:48','Inherit','Inherit','domenico-loia-310197-unsplash.jpg','domenico loia 310197 unsplash',1,1,1,'45279db1cbb1047f201f1885eb9f8c40d9eeaf36','Uploads/domenico-loia-310197-unsplash.jpg',NULL);
/*!40000 ALTER TABLE `File_Versions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `File_ViewerGroups`
--

DROP TABLE IF EXISTS `File_ViewerGroups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `File_ViewerGroups` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `FileID` int(11) NOT NULL DEFAULT '0',
  `GroupID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `FileID` (`FileID`),
  KEY `GroupID` (`GroupID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `File_ViewerGroups`
--

LOCK TABLES `File_ViewerGroups` WRITE;
/*!40000 ALTER TABLE `File_ViewerGroups` DISABLE KEYS */;
/*!40000 ALTER TABLE `File_ViewerGroups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Group`
--

DROP TABLE IF EXISTS `Group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Group` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('SilverStripe\\Security\\Group') CHARACTER SET utf8 DEFAULT 'SilverStripe\\Security\\Group',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Title` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Description` mediumtext CHARACTER SET utf8,
  `Code` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Locked` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Sort` int(11) NOT NULL DEFAULT '0',
  `HtmlEditorConfig` mediumtext CHARACTER SET utf8,
  `ParentID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `ClassName` (`ClassName`),
  KEY `ParentID` (`ParentID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Group`
--

LOCK TABLES `Group` WRITE;
/*!40000 ALTER TABLE `Group` DISABLE KEYS */;
INSERT INTO `Group` VALUES (1,'SilverStripe\\Security\\Group','2019-04-18 18:43:12','2019-04-18 18:43:12','Content Authors',NULL,'content-authors',0,1,NULL,0),(2,'SilverStripe\\Security\\Group','2019-04-18 18:43:12','2019-04-18 18:43:12','Administrators',NULL,'administrators',0,0,NULL,0),(3,'SilverStripe\\Security\\Group','2019-04-18 21:46:41','2019-04-18 21:46:41','Blog users',NULL,'blog-users',0,0,NULL,0);
/*!40000 ALTER TABLE `Group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Group_Members`
--

DROP TABLE IF EXISTS `Group_Members`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Group_Members` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `GroupID` int(11) NOT NULL DEFAULT '0',
  `MemberID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `GroupID` (`GroupID`),
  KEY `MemberID` (`MemberID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Group_Members`
--

LOCK TABLES `Group_Members` WRITE;
/*!40000 ALTER TABLE `Group_Members` DISABLE KEYS */;
INSERT INTO `Group_Members` VALUES (1,2,1);
/*!40000 ALTER TABLE `Group_Members` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Group_Roles`
--

DROP TABLE IF EXISTS `Group_Roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Group_Roles` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `GroupID` int(11) NOT NULL DEFAULT '0',
  `PermissionRoleID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `GroupID` (`GroupID`),
  KEY `PermissionRoleID` (`PermissionRoleID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Group_Roles`
--

LOCK TABLES `Group_Roles` WRITE;
/*!40000 ALTER TABLE `Group_Roles` DISABLE KEYS */;
/*!40000 ALTER TABLE `Group_Roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `LoginAttempt`
--

DROP TABLE IF EXISTS `LoginAttempt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `LoginAttempt` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('SilverStripe\\Security\\LoginAttempt') CHARACTER SET utf8 DEFAULT 'SilverStripe\\Security\\LoginAttempt',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Email` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `EmailHashed` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Status` enum('Success','Failure') CHARACTER SET utf8 DEFAULT 'Success',
  `IP` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `MemberID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `ClassName` (`ClassName`),
  KEY `MemberID` (`MemberID`),
  KEY `EmailHashed` (`EmailHashed`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `LoginAttempt`
--

LOCK TABLES `LoginAttempt` WRITE;
/*!40000 ALTER TABLE `LoginAttempt` DISABLE KEYS */;
INSERT INTO `LoginAttempt` VALUES (1,'SilverStripe\\Security\\LoginAttempt','2019-04-18 18:43:45','2019-04-18 18:43:45',NULL,'d033e22ae348aeb5660fc2140aec35850c4da997','Failure','172.20.0.1',0),(2,'SilverStripe\\Security\\LoginAttempt','2019-04-18 18:43:47','2019-04-18 18:43:47',NULL,'d033e22ae348aeb5660fc2140aec35850c4da997','Failure','172.20.0.1',0),(3,'SilverStripe\\Security\\LoginAttempt','2019-04-18 18:44:02','2019-04-18 18:44:02',NULL,'d033e22ae348aeb5660fc2140aec35850c4da997','Failure','172.20.0.1',0),(4,'SilverStripe\\Security\\LoginAttempt','2019-04-18 18:45:13','2019-04-18 18:45:13',NULL,'d033e22ae348aeb5660fc2140aec35850c4da997','Failure','172.20.0.1',0),(5,'SilverStripe\\Security\\LoginAttempt','2019-04-18 18:47:00','2019-04-18 18:47:00',NULL,'d033e22ae348aeb5660fc2140aec35850c4da997','Success','172.20.0.1',1),(6,'SilverStripe\\Security\\LoginAttempt','2019-04-18 21:46:06','2019-04-18 21:46:06',NULL,'d033e22ae348aeb5660fc2140aec35850c4da997','Success','172.26.0.1',1),(7,'SilverStripe\\Security\\LoginAttempt','2019-04-18 22:00:03','2019-04-18 22:00:03',NULL,'d033e22ae348aeb5660fc2140aec35850c4da997','Success','172.27.0.1',1),(8,'SilverStripe\\Security\\LoginAttempt','2019-04-18 22:09:27','2019-04-18 22:09:27',NULL,'d033e22ae348aeb5660fc2140aec35850c4da997','Success','172.28.0.1',1),(9,'SilverStripe\\Security\\LoginAttempt','2019-04-18 22:25:03','2019-04-18 22:25:03',NULL,'d033e22ae348aeb5660fc2140aec35850c4da997','Success','172.29.0.1',1),(10,'SilverStripe\\Security\\LoginAttempt','2019-04-18 22:29:31','2019-04-18 22:29:31',NULL,'d033e22ae348aeb5660fc2140aec35850c4da997','Success','172.30.0.1',1),(11,'SilverStripe\\Security\\LoginAttempt','2019-04-18 22:34:08','2019-04-18 22:34:08',NULL,'d033e22ae348aeb5660fc2140aec35850c4da997','Success','172.31.0.1',1),(12,'SilverStripe\\Security\\LoginAttempt','2019-04-18 22:42:09','2019-04-18 22:42:09',NULL,'d033e22ae348aeb5660fc2140aec35850c4da997','Success','192.168.0.1',1),(13,'SilverStripe\\Security\\LoginAttempt','2019-04-18 22:42:11','2019-04-18 22:42:11',NULL,'d033e22ae348aeb5660fc2140aec35850c4da997','Success','192.168.0.1',1),(14,'SilverStripe\\Security\\LoginAttempt','2019-04-18 22:49:42','2019-04-18 22:49:42',NULL,'d033e22ae348aeb5660fc2140aec35850c4da997','Success','192.168.16.1',1),(15,'SilverStripe\\Security\\LoginAttempt','2019-04-18 23:01:35','2019-04-18 23:01:35',NULL,'da39a3ee5e6b4b0d3255bfef95601890afd80709','Success','192.168.48.1',1),(16,'SilverStripe\\Security\\LoginAttempt','2019-04-20 12:10:19','2019-04-20 12:10:19',NULL,'d033e22ae348aeb5660fc2140aec35850c4da997','Success','192.168.112.1',1),(17,'SilverStripe\\Security\\LoginAttempt','2019-04-21 10:01:29','2019-04-21 10:01:29',NULL,'d033e22ae348aeb5660fc2140aec35850c4da997','Success','192.168.144.1',1),(18,'SilverStripe\\Security\\LoginAttempt','2019-04-21 10:55:10','2019-04-21 10:55:10',NULL,'d033e22ae348aeb5660fc2140aec35850c4da997','Success','192.168.144.1',1),(19,'SilverStripe\\Security\\LoginAttempt','2019-04-21 11:43:14','2019-04-21 11:43:14',NULL,'da39a3ee5e6b4b0d3255bfef95601890afd80709','Success','192.168.176.1',1),(20,'SilverStripe\\Security\\LoginAttempt','2019-04-22 23:28:00','2019-04-22 23:28:00',NULL,'d033e22ae348aeb5660fc2140aec35850c4da997','Success','192.168.0.1',1),(21,'SilverStripe\\Security\\LoginAttempt','2019-04-23 23:39:48','2019-04-23 23:39:48',NULL,'d033e22ae348aeb5660fc2140aec35850c4da997','Success','192.168.0.1',1),(22,'SilverStripe\\Security\\LoginAttempt','2019-04-24 13:24:57','2019-04-24 13:24:57',NULL,'da39a3ee5e6b4b0d3255bfef95601890afd80709','Success','192.168.0.1',1),(23,'SilverStripe\\Security\\LoginAttempt','2019-04-24 15:41:41','2019-04-24 15:41:41',NULL,'d033e22ae348aeb5660fc2140aec35850c4da997','Success','192.168.128.1',1),(24,'SilverStripe\\Security\\LoginAttempt','2019-04-24 22:03:23','2019-04-24 22:03:23',NULL,'d033e22ae348aeb5660fc2140aec35850c4da997','Success','172.25.0.1',1),(25,'SilverStripe\\Security\\LoginAttempt','2019-04-24 22:11:35','2019-04-24 22:11:35',NULL,'d033e22ae348aeb5660fc2140aec35850c4da997','Success','172.26.0.1',1),(26,'SilverStripe\\Security\\LoginAttempt','2019-04-24 22:19:24','2019-04-24 22:19:24',NULL,'d033e22ae348aeb5660fc2140aec35850c4da997','Success','172.27.0.1',1),(27,'SilverStripe\\Security\\LoginAttempt','2019-04-24 22:29:29','2019-04-24 22:29:29',NULL,'d033e22ae348aeb5660fc2140aec35850c4da997','Success','172.28.0.1',1),(28,'SilverStripe\\Security\\LoginAttempt','2019-04-24 22:31:44','2019-04-24 22:31:44',NULL,'d033e22ae348aeb5660fc2140aec35850c4da997','Success','172.29.0.1',1),(29,'SilverStripe\\Security\\LoginAttempt','2019-04-24 23:02:24','2019-04-24 23:02:24',NULL,'d033e22ae348aeb5660fc2140aec35850c4da997','Success','172.30.0.1',1),(30,'SilverStripe\\Security\\LoginAttempt','2019-04-25 23:31:44','2019-04-25 23:31:44',NULL,'da39a3ee5e6b4b0d3255bfef95601890afd80709','Success','192.168.96.1',1),(31,'SilverStripe\\Security\\LoginAttempt','2019-04-26 01:37:29','2019-04-26 01:37:29',NULL,'d033e22ae348aeb5660fc2140aec35850c4da997','Success','192.168.96.1',1),(32,'SilverStripe\\Security\\LoginAttempt','2019-04-26 01:37:31','2019-04-26 01:37:31',NULL,'d033e22ae348aeb5660fc2140aec35850c4da997','Success','192.168.96.1',1),(33,'SilverStripe\\Security\\LoginAttempt','2019-04-26 16:37:51','2019-04-26 16:37:51',NULL,'d033e22ae348aeb5660fc2140aec35850c4da997','Success','192.168.128.1',1),(34,'SilverStripe\\Security\\LoginAttempt','2019-04-26 17:31:49','2019-04-26 17:31:49',NULL,'d033e22ae348aeb5660fc2140aec35850c4da997','Failure','192.168.128.1',1),(35,'SilverStripe\\Security\\LoginAttempt','2019-04-26 17:32:00','2019-04-26 17:32:00',NULL,'d033e22ae348aeb5660fc2140aec35850c4da997','Success','192.168.128.1',1),(36,'SilverStripe\\Security\\LoginAttempt','2019-04-26 17:32:01','2019-04-26 17:32:01',NULL,'d033e22ae348aeb5660fc2140aec35850c4da997','Success','192.168.128.1',1),(37,'SilverStripe\\Security\\LoginAttempt','2019-04-26 17:32:03','2019-04-26 17:32:03',NULL,'d033e22ae348aeb5660fc2140aec35850c4da997','Success','192.168.128.1',1);
/*!40000 ALTER TABLE `LoginAttempt` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Member`
--

DROP TABLE IF EXISTS `Member`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Member` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('SilverStripe\\Security\\Member') CHARACTER SET utf8 DEFAULT 'SilverStripe\\Security\\Member',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `URLSegment` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `BlogProfileSummary` mediumtext CHARACTER SET utf8,
  `FirstName` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Surname` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Email` varchar(254) CHARACTER SET utf8 DEFAULT NULL,
  `TempIDHash` varchar(160) CHARACTER SET utf8 DEFAULT NULL,
  `TempIDExpired` datetime DEFAULT NULL,
  `Password` varchar(160) CHARACTER SET utf8 DEFAULT NULL,
  `AutoLoginHash` varchar(160) CHARACTER SET utf8 DEFAULT NULL,
  `AutoLoginExpired` datetime DEFAULT NULL,
  `PasswordEncryption` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `Salt` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `PasswordExpiry` date DEFAULT NULL,
  `LockedOutUntil` datetime DEFAULT NULL,
  `Locale` varchar(6) CHARACTER SET utf8 DEFAULT NULL,
  `FailedLoginCount` int(11) NOT NULL DEFAULT '0',
  `BlogProfileImageID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `Surname` (`Surname`),
  KEY `FirstName` (`FirstName`),
  KEY `ClassName` (`ClassName`),
  KEY `BlogProfileImageID` (`BlogProfileImageID`),
  KEY `Email` (`Email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Member`
--

LOCK TABLES `Member` WRITE;
/*!40000 ALTER TABLE `Member` DISABLE KEYS */;
INSERT INTO `Member` VALUES (1,'SilverStripe\\Security\\Member','2019-04-26 17:32:04','2019-04-18 18:46:31','default-admin',NULL,'Default Admin',NULL,'admin','668d6de03fd9d605449393bfa6b4abc6c0c43308','2019-04-29 17:32:04','$2y$10$b4a12be8b7e8acb980173OEnFcnIczGxyZopgO97GUNfvWRshQkQK',NULL,NULL,'blowfish','10$b4a12be8b7e8acb980173a',NULL,NULL,'en_US',0,0);
/*!40000 ALTER TABLE `Member` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `MemberPassword`
--

DROP TABLE IF EXISTS `MemberPassword`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MemberPassword` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('SilverStripe\\Security\\MemberPassword') CHARACTER SET utf8 DEFAULT 'SilverStripe\\Security\\MemberPassword',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Password` varchar(160) CHARACTER SET utf8 DEFAULT NULL,
  `Salt` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `PasswordEncryption` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `MemberID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `ClassName` (`ClassName`),
  KEY `MemberID` (`MemberID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `MemberPassword`
--

LOCK TABLES `MemberPassword` WRITE;
/*!40000 ALTER TABLE `MemberPassword` DISABLE KEYS */;
INSERT INTO `MemberPassword` VALUES (1,'SilverStripe\\Security\\MemberPassword','2019-04-18 18:46:32','2019-04-18 18:46:32',NULL,NULL,'none',1),(2,'SilverStripe\\Security\\MemberPassword','2019-04-18 18:46:32','2019-04-18 18:46:32','$2y$10$b4a12be8b7e8acb980173OEnFcnIczGxyZopgO97GUNfvWRshQkQK','10$b4a12be8b7e8acb980173a','blowfish',1);
/*!40000 ALTER TABLE `MemberPassword` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Permission`
--

DROP TABLE IF EXISTS `Permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Permission` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('SilverStripe\\Security\\Permission') CHARACTER SET utf8 DEFAULT 'SilverStripe\\Security\\Permission',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Code` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Arg` int(11) NOT NULL DEFAULT '0',
  `Type` int(11) NOT NULL DEFAULT '1',
  `GroupID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `ClassName` (`ClassName`),
  KEY `GroupID` (`GroupID`),
  KEY `Code` (`Code`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Permission`
--

LOCK TABLES `Permission` WRITE;
/*!40000 ALTER TABLE `Permission` DISABLE KEYS */;
INSERT INTO `Permission` VALUES (1,'SilverStripe\\Security\\Permission','2019-04-18 18:43:12','2019-04-18 18:43:12','CMS_ACCESS_CMSMain',0,1,1),(2,'SilverStripe\\Security\\Permission','2019-04-18 18:43:12','2019-04-18 18:43:12','CMS_ACCESS_AssetAdmin',0,1,1),(3,'SilverStripe\\Security\\Permission','2019-04-18 18:43:12','2019-04-18 18:43:12','CMS_ACCESS_ReportAdmin',0,1,1),(4,'SilverStripe\\Security\\Permission','2019-04-18 18:43:12','2019-04-18 18:43:12','SITETREE_REORGANISE',0,1,1),(5,'SilverStripe\\Security\\Permission','2019-04-18 18:43:12','2019-04-18 18:43:12','ADMIN',0,1,2),(6,'SilverStripe\\Security\\Permission','2019-04-18 21:46:41','2019-04-18 21:46:41','CMS_ACCESS_CMSMain',0,1,3);
/*!40000 ALTER TABLE `Permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `PermissionRole`
--

DROP TABLE IF EXISTS `PermissionRole`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PermissionRole` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('SilverStripe\\Security\\PermissionRole') CHARACTER SET utf8 DEFAULT 'SilverStripe\\Security\\PermissionRole',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Title` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `OnlyAdminCanApply` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `Title` (`Title`),
  KEY `ClassName` (`ClassName`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `PermissionRole`
--

LOCK TABLES `PermissionRole` WRITE;
/*!40000 ALTER TABLE `PermissionRole` DISABLE KEYS */;
/*!40000 ALTER TABLE `PermissionRole` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `PermissionRoleCode`
--

DROP TABLE IF EXISTS `PermissionRoleCode`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PermissionRoleCode` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('SilverStripe\\Security\\PermissionRoleCode') CHARACTER SET utf8 DEFAULT 'SilverStripe\\Security\\PermissionRoleCode',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Code` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `RoleID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `ClassName` (`ClassName`),
  KEY `RoleID` (`RoleID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `PermissionRoleCode`
--

LOCK TABLES `PermissionRoleCode` WRITE;
/*!40000 ALTER TABLE `PermissionRoleCode` DISABLE KEYS */;
/*!40000 ALTER TABLE `PermissionRoleCode` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `RedirectorPage`
--

DROP TABLE IF EXISTS `RedirectorPage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RedirectorPage` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RedirectionType` enum('Internal','External') CHARACTER SET utf8 DEFAULT 'Internal',
  `ExternalURL` varchar(2083) CHARACTER SET utf8 DEFAULT NULL,
  `LinkToID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `LinkToID` (`LinkToID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `RedirectorPage`
--

LOCK TABLES `RedirectorPage` WRITE;
/*!40000 ALTER TABLE `RedirectorPage` DISABLE KEYS */;
/*!40000 ALTER TABLE `RedirectorPage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `RedirectorPage_Live`
--

DROP TABLE IF EXISTS `RedirectorPage_Live`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RedirectorPage_Live` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RedirectionType` enum('Internal','External') CHARACTER SET utf8 DEFAULT 'Internal',
  `ExternalURL` varchar(2083) CHARACTER SET utf8 DEFAULT NULL,
  `LinkToID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `LinkToID` (`LinkToID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `RedirectorPage_Live`
--

LOCK TABLES `RedirectorPage_Live` WRITE;
/*!40000 ALTER TABLE `RedirectorPage_Live` DISABLE KEYS */;
/*!40000 ALTER TABLE `RedirectorPage_Live` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `RedirectorPage_Versions`
--

DROP TABLE IF EXISTS `RedirectorPage_Versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RedirectorPage_Versions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `RedirectionType` enum('Internal','External') CHARACTER SET utf8 DEFAULT 'Internal',
  `ExternalURL` varchar(2083) CHARACTER SET utf8 DEFAULT NULL,
  `LinkToID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  KEY `RecordID` (`RecordID`),
  KEY `Version` (`Version`),
  KEY `LinkToID` (`LinkToID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `RedirectorPage_Versions`
--

LOCK TABLES `RedirectorPage_Versions` WRITE;
/*!40000 ALTER TABLE `RedirectorPage_Versions` DISABLE KEYS */;
/*!40000 ALTER TABLE `RedirectorPage_Versions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `RememberLoginHash`
--

DROP TABLE IF EXISTS `RememberLoginHash`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RememberLoginHash` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('SilverStripe\\Security\\RememberLoginHash') CHARACTER SET utf8 DEFAULT 'SilverStripe\\Security\\RememberLoginHash',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `DeviceID` varchar(40) CHARACTER SET utf8 DEFAULT NULL,
  `Hash` varchar(160) CHARACTER SET utf8 DEFAULT NULL,
  `ExpiryDate` datetime DEFAULT NULL,
  `MemberID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `ClassName` (`ClassName`),
  KEY `MemberID` (`MemberID`),
  KEY `DeviceID` (`DeviceID`),
  KEY `Hash` (`Hash`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `RememberLoginHash`
--

LOCK TABLES `RememberLoginHash` WRITE;
/*!40000 ALTER TABLE `RememberLoginHash` DISABLE KEYS */;
/*!40000 ALTER TABLE `RememberLoginHash` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SiteConfig`
--

DROP TABLE IF EXISTS `SiteConfig`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SiteConfig` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('SilverStripe\\SiteConfig\\SiteConfig') CHARACTER SET utf8 DEFAULT 'SilverStripe\\SiteConfig\\SiteConfig',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Title` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Tagline` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `CanViewType` enum('Anyone','LoggedInUsers','OnlyTheseUsers') CHARACTER SET utf8 DEFAULT 'Anyone',
  `CanEditType` enum('LoggedInUsers','OnlyTheseUsers') CHARACTER SET utf8 DEFAULT 'LoggedInUsers',
  `CanCreateTopLevelType` enum('LoggedInUsers','OnlyTheseUsers') CHARACTER SET utf8 DEFAULT 'LoggedInUsers',
  PRIMARY KEY (`ID`),
  KEY `ClassName` (`ClassName`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SiteConfig`
--

LOCK TABLES `SiteConfig` WRITE;
/*!40000 ALTER TABLE `SiteConfig` DISABLE KEYS */;
INSERT INTO `SiteConfig` VALUES (1,'SilverStripe\\SiteConfig\\SiteConfig','2019-04-18 18:43:10','2019-04-18 18:43:10','Your Site Name','your tagline here','Anyone','LoggedInUsers','LoggedInUsers');
/*!40000 ALTER TABLE `SiteConfig` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SiteConfig_CreateTopLevelGroups`
--

DROP TABLE IF EXISTS `SiteConfig_CreateTopLevelGroups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SiteConfig_CreateTopLevelGroups` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SiteConfigID` int(11) NOT NULL DEFAULT '0',
  `GroupID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `SiteConfigID` (`SiteConfigID`),
  KEY `GroupID` (`GroupID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SiteConfig_CreateTopLevelGroups`
--

LOCK TABLES `SiteConfig_CreateTopLevelGroups` WRITE;
/*!40000 ALTER TABLE `SiteConfig_CreateTopLevelGroups` DISABLE KEYS */;
/*!40000 ALTER TABLE `SiteConfig_CreateTopLevelGroups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SiteConfig_EditorGroups`
--

DROP TABLE IF EXISTS `SiteConfig_EditorGroups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SiteConfig_EditorGroups` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SiteConfigID` int(11) NOT NULL DEFAULT '0',
  `GroupID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `SiteConfigID` (`SiteConfigID`),
  KEY `GroupID` (`GroupID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SiteConfig_EditorGroups`
--

LOCK TABLES `SiteConfig_EditorGroups` WRITE;
/*!40000 ALTER TABLE `SiteConfig_EditorGroups` DISABLE KEYS */;
/*!40000 ALTER TABLE `SiteConfig_EditorGroups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SiteConfig_ViewerGroups`
--

DROP TABLE IF EXISTS `SiteConfig_ViewerGroups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SiteConfig_ViewerGroups` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SiteConfigID` int(11) NOT NULL DEFAULT '0',
  `GroupID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `SiteConfigID` (`SiteConfigID`),
  KEY `GroupID` (`GroupID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SiteConfig_ViewerGroups`
--

LOCK TABLES `SiteConfig_ViewerGroups` WRITE;
/*!40000 ALTER TABLE `SiteConfig_ViewerGroups` DISABLE KEYS */;
/*!40000 ALTER TABLE `SiteConfig_ViewerGroups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SiteTree`
--

DROP TABLE IF EXISTS `SiteTree`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SiteTree` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('SilverStripe\\CMS\\Model\\SiteTree','Page','SilverStripe\\ErrorPage\\ErrorPage','SilverStripe\\Blog\\Model\\Blog','SilverStripe\\Blog\\Model\\BlogPost','SilverStripe\\CMS\\Model\\RedirectorPage','SilverStripe\\CMS\\Model\\VirtualPage') CHARACTER SET utf8 DEFAULT 'Page',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `CanViewType` enum('Anyone','LoggedInUsers','OnlyTheseUsers','Inherit') CHARACTER SET utf8 DEFAULT 'Inherit',
  `CanEditType` enum('LoggedInUsers','OnlyTheseUsers','Inherit') CHARACTER SET utf8 DEFAULT 'Inherit',
  `Version` int(11) NOT NULL DEFAULT '0',
  `URLSegment` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Title` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `MenuTitle` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `Content` mediumtext CHARACTER SET utf8,
  `MetaDescription` mediumtext CHARACTER SET utf8,
  `ExtraMeta` mediumtext CHARACTER SET utf8,
  `ShowInMenus` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ShowInSearch` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Sort` int(11) NOT NULL DEFAULT '0',
  `HasBrokenFile` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `HasBrokenLink` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ReportClass` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `ParentID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `Sort` (`Sort`),
  KEY `ClassName` (`ClassName`),
  KEY `ParentID` (`ParentID`),
  KEY `URLSegment` (`URLSegment`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SiteTree`
--

LOCK TABLES `SiteTree` WRITE;
/*!40000 ALTER TABLE `SiteTree` DISABLE KEYS */;
INSERT INTO `SiteTree` VALUES (1,'Page','2019-04-18 18:43:11','2019-04-18 18:43:10','Inherit','Inherit',2,'home','Home',NULL,'<p>Welcome to SilverStripe! This is the default homepage. You can edit this page by opening <a href=\"admin/\">the CMS</a>.</p><p>You can now access the <a href=\"http://docs.silverstripe.org\">developer documentation</a>, or begin the <a href=\"http://www.silverstripe.org/learn/lessons\">SilverStripe lessons</a>.</p>',NULL,NULL,1,1,1,0,0,NULL,0),(4,'SilverStripe\\ErrorPage\\ErrorPage','2019-04-18 18:43:13','2019-04-18 18:43:12','Inherit','Inherit',2,'page-not-found','Page not found',NULL,'<p>Sorry, it seems you were trying to access a page that doesn\'t exist.</p><p>Please check the spelling of the URL you were trying to access and try again.</p>',NULL,NULL,0,0,4,0,0,NULL,0),(5,'SilverStripe\\ErrorPage\\ErrorPage','2019-04-18 18:43:14','2019-04-18 18:43:14','Inherit','Inherit',2,'server-error','Server error',NULL,'<p>Sorry, there was a problem with handling your request.</p>',NULL,NULL,0,0,5,0,0,NULL,0),(14,'SilverStripe\\Blog\\Model\\Blog','2019-04-20 12:14:27','2019-04-20 12:12:09','Inherit','Inherit',3,'blog','Blog',NULL,NULL,NULL,NULL,1,1,7,0,0,NULL,0),(15,'SilverStripe\\Blog\\Model\\BlogPost','2019-04-22 23:41:30','2019-04-20 12:14:44','Inherit','Inherit',14,'product-roadmap-2019-q1','Product Roadmap: 2019 Q1','Product Roadmap 2019 Q1','<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</p><div class=\"image-container\">[image src=\"/silverstripe-cms/assets/Uploads/e8fa9273b6/link_oot.png\" id=\"2\" width=\"292\" height=\"292\" class=\"container-image ss-htmleditorfield-file image\" title=\"link oot\" alt=\"link\"][image src=\"/silverstripe-cms/assets/Uploads/65114dbce0/mario_64-.png\" id=\"3\" width=\"307\" height=\"226\" class=\"container-image ss-htmleditorfield-file image\" title=\"mario 64 \" alt=\"mario\"][image src=\"/silverstripe-cms/assets/Uploads/5e477476bb/wave_race.png\" id=\"4\" width=\"574\" height=\"362\" class=\"container-image ss-htmleditorfield-file image\" title=\"wave race\" alt=\"wave\"]</div><p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</p><div class=\"image-container\">[image src=\"/silverstripe-cms/assets/a8a148aa9e/king-kong.jpg\" id=\"9\" width=\"1446\" height=\"766\" class=\"container-image-single ss-htmleditorfield-file image\" title=\"king kong\" alt=\"king-kong\"]</div>',NULL,NULL,0,1,1,0,0,NULL,14),(16,'SilverStripe\\Blog\\Model\\BlogPost','2019-04-21 11:49:18','2019-04-21 11:45:22','Inherit','Inherit',4,'q1-priorities-2019','Q1 Priorities 2019',NULL,'<p><span>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</span></p><p><span>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</span></p>',NULL,NULL,0,1,2,0,0,NULL,14),(17,'SilverStripe\\Blog\\Model\\BlogPost','2019-04-24 00:33:43','2019-04-23 23:40:42','Inherit','Inherit',4,'scheduling-and-rates','Scheduling & Rates',NULL,'<p>Knowing how your staff are tracking against a budget is something our projects module aims to do as a core feature. The latest update...Knowing how your staff are tracking against a budget is something our projects module aims to do as a core feature. The latest update...Knowing how your staff are tracking against a budget is something our projects module aims to do as a core feature. The latest update...Knowing how your staff are tracking against a budget is something our projects module aims to do as a core feature. The latest update...Knowing how your staff are tracking against a budget is something our projects module aims to do as a core feature. The latest update...Knowing how your staff are tracking against a budget is something our projects module aims to do as a core feature. The latest update...Knowing how your staff are tracking against a budget is something our projects module aims to do as a core feature. The latest update...Knowing how your staff are tracking against a budget is something our projects module aims to do as a core feature. The latest update...Knowing how your staff are tracking against a budget is something our projects module aims to do as a core feature. The latest update...Knowing how your staff are tracking against a budget is something our projects module aims to do as a core feature. The latest update...Knowing how your staff are tracking against a budget is something our projects module aims to do as a core feature. The latest update...Knowing how your staff are tracking against a budget is something our projects module aims to do as a core feature. The latest update...Knowing how your staff are tracking against a budget is something our projects module aims to do as a core feature. The latest update...Knowing how your staff are tracking against a budget is something our projects module aims to do as a core feature. The latest update...Knowing how your staff are tracking against a budget is something our projects module aims to do as a core feature. The latest update...Knowing how your staff are tracking against a budget is something our projects module aims to do as a core feature. The latest update...Knowing how your staff are tracking against a budget is something our projects module aims to do as a core feature. The latest update...Knowing how your staff are tracking against a budget is something our projects module aims to do as a core feature. The latest update...Knowing how your staff are tracking against a budget is something our projects module aims to do as a core feature. The latest update...Knowing how your staff are tracking against a budget is something our projects module aims to do as a core feature. The latest update...</p>',NULL,NULL,0,1,3,0,0,NULL,14),(18,'SilverStripe\\Blog\\Model\\BlogPost','2019-04-24 00:48:36','2019-04-24 00:46:42','Inherit','Inherit',4,'digital-agencies-our-future-of-media','Digital Agencies: Our Future Of Media',NULL,'<p>2019 is shaping up to be a big year at Accelo, and we have some awesome new features to help you and your business and...2019 is shaping up to be a big year at Accelo, and we have some awesome new features to help you and your business and...2019 is shaping up to be a big year at Accelo, and we have some awesome new features to help you and your business and...2019 is shaping up to be a big year at Accelo, and we have some awesome new features to help you and your business and...2019 is shaping up to be a big year at Accelo, and we have some awesome new features to help you and your business and...2019 is shaping up to be a big year at Accelo, and we have some awesome new features to help you and your business and...2019 is shaping up to be a big year at Accelo, and we have some awesome new features to help you and your business and...2019 is shaping up to be a big year at Accelo, and we have some awesome new features to help you and your business and...2019 is shaping up to be a big year at Accelo, and we have some awesome new features to help you and your business and...2019 is shaping up to be a big year at Accelo, and we have some awesome new features to help you and your business and...2019 is shaping up to be a big year at Accelo, and we have some awesome new features to help you and your business and...2019 is shaping up to be a big year at Accelo, and we have some awesome new features to help you and your business and...2019 is shaping up to be a big year at Accelo, and we have some awesome new features to help you and your business and...2019 is shaping up to be a big year at Accelo, and we have some awesome new features to help you and your business and...2019 is shaping up to be a big year at Accelo, and we have some awesome new features to help you and your business and...2019 is shaping up to be a big year at Accelo, and we have some awesome new features to help you and your business and...2019 is shaping up to be a big year at Accelo, and we have some awesome new features to help you and your business and...2019 is shaping up to be a big year at Accelo, and we have some awesome new features to help you and your business and...</p>',NULL,NULL,0,1,4,0,0,NULL,14),(19,'SilverStripe\\Blog\\Model\\BlogPost','2019-04-24 13:31:03','2019-04-24 13:28:13','Inherit','Inherit',4,'timers-and-timesheets','Timers And Timesheets',NULL,'<p>Time is important. It’s a resource, and we know a lot of our end-users base their business workflows on how much of it they have...Time is important. It’s a resource, and we know a lot of our end-users base their business workflows on how much of it they have...Time is important. It’s a resource, and we know a lot of our end-users base their business workflows on how much of it they have...Time is important. It’s a resource, and we know a lot of our end-users base their business workflows on how much of it they have...Time is important. It’s a resource, and we know a lot of our end-users base their business workflows on how much of it they have...Time is important. It’s a resource, and we know a lot of our end-users base their business workflows on how much of it they have...Time is important. It’s a resource, and we know a lot of our end-users base their business workflows on how much of it they have...Time is important. It’s a resource, and we know a lot of our end-users base their business workflows on how much of it they have...Time is important. It’s a resource, and we know a lot of our end-users base their business workflows on how much of it they have...Time is important. It’s a resource, and we know a lot of our end-users base their business workflows on how much of it they have...Time is important. It’s a resource, and we know a lot of our end-users base their business workflows on how much of it they have...Time is important. It’s a resource, and we know a lot of our end-users base their business workflows on how much of it they have...Time is important. It’s a resource, and we know a lot of our end-users base their business workflows on how much of it they have...Time is important. It’s a resource, and we know a lot of our end-users base their business workflows on how much of it they have...Time is important. It’s a resource, and we know a lot of our end-users base their business workflows on how much of it they have...Time is important. It’s a resource, and we know a lot of our end-users base their business workflows on how much of it they have...Time is important. It’s a resource, and we know a lot of our end-users base their business workflows on how much of it they have...</p>',NULL,NULL,0,1,5,0,0,NULL,14),(20,'SilverStripe\\Blog\\Model\\BlogPost','2019-04-24 23:05:46','2019-04-24 22:04:26','Inherit','Inherit',4,'working-on-the-road-our-new-timers-update-is-live','Working on the road? Our new timers update is live!',NULL,'<p>For a long time, timers have been at the heart of our time keeping system. A lot of our clients love how timers can start from anywhere in our web app and mobile app. With this new update, we’re aiming to solve the pain point around adding descriptions to timers while they’re active…<br><br><br>For a long time, timers have been at the heart of our time keeping system. A lot of our clients love how timers can start from anywhere in our web app and mobile app. With this new update, we’re aiming to solve the pain point around adding descriptions to timers while they’re active…<br><br>For a long time, timers have been at the heart of our time keeping system. A lot of our clients love how timers can start from anywhere in our web app and mobile app. With this new update, we’re aiming to solve the pain point around adding descriptions to timers while they’re active…For a long time, timers have been at the heart of our time keeping system. A lot of our clients love how timers can start from anywhere in our web app and mobile app. With this new update, we’re aiming to solve the pain point around adding descriptions to timers while they’re active…</p>',NULL,NULL,0,1,6,0,0,NULL,14),(21,'SilverStripe\\Blog\\Model\\BlogPost','2019-04-25 23:39:54','2019-04-25 23:33:07','Inherit','Inherit',4,'invoice-template-customization','Invoice Template Customization',NULL,'<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt...</p><p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt...</p><p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt...</p><p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt...</p><p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt...</p><p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt...</p><p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt...</p><p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt...</p><p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt...</p><p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt...</p><p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt...</p><p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt...</p>',NULL,NULL,0,1,7,0,0,NULL,14);
/*!40000 ALTER TABLE `SiteTree` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SiteTreeLink`
--

DROP TABLE IF EXISTS `SiteTreeLink`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SiteTreeLink` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('SilverStripe\\CMS\\Model\\SiteTreeLink') CHARACTER SET utf8 DEFAULT 'SilverStripe\\CMS\\Model\\SiteTreeLink',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `LinkedID` int(11) NOT NULL DEFAULT '0',
  `ParentID` int(11) NOT NULL DEFAULT '0',
  `ParentClass` enum('SilverStripe\\Assets\\File','SilverStripe\\SiteConfig\\SiteConfig','SilverStripe\\Versioned\\ChangeSet','SilverStripe\\Versioned\\ChangeSetItem','SilverStripe\\Assets\\Shortcodes\\FileLink','SilverStripe\\Blog\\Model\\BlogCategory','SilverStripe\\Blog\\Model\\BlogTag','SilverStripe\\CMS\\Model\\SiteTree','SilverStripe\\CMS\\Model\\SiteTreeLink','SilverStripe\\Security\\Group','SilverStripe\\Security\\LoginAttempt','SilverStripe\\Security\\Member','SilverStripe\\Security\\MemberPassword','SilverStripe\\Security\\Permission','SilverStripe\\Security\\PermissionRole','SilverStripe\\Security\\PermissionRoleCode','SilverStripe\\Security\\RememberLoginHash','SilverStripe\\Assets\\Folder','SilverStripe\\Assets\\Image','Page','SilverStripe\\ErrorPage\\ErrorPage','SilverStripe\\Blog\\Model\\Blog','SilverStripe\\Blog\\Model\\BlogPost','SilverStripe\\CMS\\Model\\RedirectorPage','SilverStripe\\CMS\\Model\\VirtualPage') CHARACTER SET utf8 DEFAULT 'SilverStripe\\Assets\\File',
  PRIMARY KEY (`ID`),
  KEY `ClassName` (`ClassName`),
  KEY `LinkedID` (`LinkedID`),
  KEY `Parent` (`ParentID`,`ParentClass`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SiteTreeLink`
--

LOCK TABLES `SiteTreeLink` WRITE;
/*!40000 ALTER TABLE `SiteTreeLink` DISABLE KEYS */;
/*!40000 ALTER TABLE `SiteTreeLink` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SiteTree_EditorGroups`
--

DROP TABLE IF EXISTS `SiteTree_EditorGroups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SiteTree_EditorGroups` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SiteTreeID` int(11) NOT NULL DEFAULT '0',
  `GroupID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `SiteTreeID` (`SiteTreeID`),
  KEY `GroupID` (`GroupID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SiteTree_EditorGroups`
--

LOCK TABLES `SiteTree_EditorGroups` WRITE;
/*!40000 ALTER TABLE `SiteTree_EditorGroups` DISABLE KEYS */;
/*!40000 ALTER TABLE `SiteTree_EditorGroups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SiteTree_Live`
--

DROP TABLE IF EXISTS `SiteTree_Live`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SiteTree_Live` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('SilverStripe\\CMS\\Model\\SiteTree','Page','SilverStripe\\ErrorPage\\ErrorPage','SilverStripe\\Blog\\Model\\Blog','SilverStripe\\Blog\\Model\\BlogPost','SilverStripe\\CMS\\Model\\RedirectorPage','SilverStripe\\CMS\\Model\\VirtualPage') CHARACTER SET utf8 DEFAULT 'Page',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `CanViewType` enum('Anyone','LoggedInUsers','OnlyTheseUsers','Inherit') CHARACTER SET utf8 DEFAULT 'Inherit',
  `CanEditType` enum('LoggedInUsers','OnlyTheseUsers','Inherit') CHARACTER SET utf8 DEFAULT 'Inherit',
  `Version` int(11) NOT NULL DEFAULT '0',
  `URLSegment` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Title` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `MenuTitle` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `Content` mediumtext CHARACTER SET utf8,
  `MetaDescription` mediumtext CHARACTER SET utf8,
  `ExtraMeta` mediumtext CHARACTER SET utf8,
  `ShowInMenus` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ShowInSearch` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Sort` int(11) NOT NULL DEFAULT '0',
  `HasBrokenFile` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `HasBrokenLink` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ReportClass` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `ParentID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `Sort` (`Sort`),
  KEY `ClassName` (`ClassName`),
  KEY `ParentID` (`ParentID`),
  KEY `URLSegment` (`URLSegment`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SiteTree_Live`
--

LOCK TABLES `SiteTree_Live` WRITE;
/*!40000 ALTER TABLE `SiteTree_Live` DISABLE KEYS */;
INSERT INTO `SiteTree_Live` VALUES (4,'SilverStripe\\ErrorPage\\ErrorPage','2019-04-18 18:43:13','2019-04-18 18:43:12','Inherit','Inherit',2,'page-not-found','Page not found',NULL,'<p>Sorry, it seems you were trying to access a page that doesn\'t exist.</p><p>Please check the spelling of the URL you were trying to access and try again.</p>',NULL,NULL,0,0,4,0,0,NULL,0),(5,'SilverStripe\\ErrorPage\\ErrorPage','2019-04-18 18:43:14','2019-04-18 18:43:14','Inherit','Inherit',2,'server-error','Server error',NULL,'<p>Sorry, there was a problem with handling your request.</p>',NULL,NULL,0,0,5,0,0,NULL,0),(14,'SilverStripe\\Blog\\Model\\Blog','2019-04-20 12:14:27','2019-04-20 12:12:09','Inherit','Inherit',3,'blog','Blog',NULL,NULL,NULL,NULL,1,1,7,0,0,NULL,0),(15,'SilverStripe\\Blog\\Model\\BlogPost','2019-04-22 23:41:30','2019-04-20 12:14:44','Inherit','Inherit',14,'product-roadmap-2019-q1','Product Roadmap: 2019 Q1','Product Roadmap 2019 Q1','<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</p><div class=\"image-container\">[image src=\"/silverstripe-cms/assets/Uploads/e8fa9273b6/link_oot.png\" id=\"2\" width=\"292\" height=\"292\" class=\"container-image ss-htmleditorfield-file image\" title=\"link oot\" alt=\"link\"][image src=\"/silverstripe-cms/assets/Uploads/65114dbce0/mario_64-.png\" id=\"3\" width=\"307\" height=\"226\" class=\"container-image ss-htmleditorfield-file image\" title=\"mario 64 \" alt=\"mario\"][image src=\"/silverstripe-cms/assets/Uploads/5e477476bb/wave_race.png\" id=\"4\" width=\"574\" height=\"362\" class=\"container-image ss-htmleditorfield-file image\" title=\"wave race\" alt=\"wave\"]</div><p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</p><div class=\"image-container\">[image src=\"/silverstripe-cms/assets/a8a148aa9e/king-kong.jpg\" id=\"9\" width=\"1446\" height=\"766\" class=\"container-image-single ss-htmleditorfield-file image\" title=\"king kong\" alt=\"king-kong\"]</div>',NULL,NULL,0,1,1,0,0,NULL,14),(16,'SilverStripe\\Blog\\Model\\BlogPost','2019-04-21 11:49:18','2019-04-21 11:45:22','Inherit','Inherit',4,'q1-priorities-2019','Q1 Priorities 2019',NULL,'<p><span>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</span></p><p><span>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</span></p>',NULL,NULL,0,1,2,0,0,NULL,14),(17,'SilverStripe\\Blog\\Model\\BlogPost','2019-04-24 00:33:43','2019-04-23 23:40:42','Inherit','Inherit',4,'scheduling-and-rates','Scheduling & Rates',NULL,'<p>Knowing how your staff are tracking against a budget is something our projects module aims to do as a core feature. The latest update...Knowing how your staff are tracking against a budget is something our projects module aims to do as a core feature. The latest update...Knowing how your staff are tracking against a budget is something our projects module aims to do as a core feature. The latest update...Knowing how your staff are tracking against a budget is something our projects module aims to do as a core feature. The latest update...Knowing how your staff are tracking against a budget is something our projects module aims to do as a core feature. The latest update...Knowing how your staff are tracking against a budget is something our projects module aims to do as a core feature. The latest update...Knowing how your staff are tracking against a budget is something our projects module aims to do as a core feature. The latest update...Knowing how your staff are tracking against a budget is something our projects module aims to do as a core feature. The latest update...Knowing how your staff are tracking against a budget is something our projects module aims to do as a core feature. The latest update...Knowing how your staff are tracking against a budget is something our projects module aims to do as a core feature. The latest update...Knowing how your staff are tracking against a budget is something our projects module aims to do as a core feature. The latest update...Knowing how your staff are tracking against a budget is something our projects module aims to do as a core feature. The latest update...Knowing how your staff are tracking against a budget is something our projects module aims to do as a core feature. The latest update...Knowing how your staff are tracking against a budget is something our projects module aims to do as a core feature. The latest update...Knowing how your staff are tracking against a budget is something our projects module aims to do as a core feature. The latest update...Knowing how your staff are tracking against a budget is something our projects module aims to do as a core feature. The latest update...Knowing how your staff are tracking against a budget is something our projects module aims to do as a core feature. The latest update...Knowing how your staff are tracking against a budget is something our projects module aims to do as a core feature. The latest update...Knowing how your staff are tracking against a budget is something our projects module aims to do as a core feature. The latest update...Knowing how your staff are tracking against a budget is something our projects module aims to do as a core feature. The latest update...</p>',NULL,NULL,0,1,3,0,0,NULL,14),(18,'SilverStripe\\Blog\\Model\\BlogPost','2019-04-24 00:48:36','2019-04-24 00:46:42','Inherit','Inherit',4,'digital-agencies-our-future-of-media','Digital Agencies: Our Future Of Media',NULL,'<p>2019 is shaping up to be a big year at Accelo, and we have some awesome new features to help you and your business and...2019 is shaping up to be a big year at Accelo, and we have some awesome new features to help you and your business and...2019 is shaping up to be a big year at Accelo, and we have some awesome new features to help you and your business and...2019 is shaping up to be a big year at Accelo, and we have some awesome new features to help you and your business and...2019 is shaping up to be a big year at Accelo, and we have some awesome new features to help you and your business and...2019 is shaping up to be a big year at Accelo, and we have some awesome new features to help you and your business and...2019 is shaping up to be a big year at Accelo, and we have some awesome new features to help you and your business and...2019 is shaping up to be a big year at Accelo, and we have some awesome new features to help you and your business and...2019 is shaping up to be a big year at Accelo, and we have some awesome new features to help you and your business and...2019 is shaping up to be a big year at Accelo, and we have some awesome new features to help you and your business and...2019 is shaping up to be a big year at Accelo, and we have some awesome new features to help you and your business and...2019 is shaping up to be a big year at Accelo, and we have some awesome new features to help you and your business and...2019 is shaping up to be a big year at Accelo, and we have some awesome new features to help you and your business and...2019 is shaping up to be a big year at Accelo, and we have some awesome new features to help you and your business and...2019 is shaping up to be a big year at Accelo, and we have some awesome new features to help you and your business and...2019 is shaping up to be a big year at Accelo, and we have some awesome new features to help you and your business and...2019 is shaping up to be a big year at Accelo, and we have some awesome new features to help you and your business and...2019 is shaping up to be a big year at Accelo, and we have some awesome new features to help you and your business and...</p>',NULL,NULL,0,1,4,0,0,NULL,14),(19,'SilverStripe\\Blog\\Model\\BlogPost','2019-04-24 13:31:03','2019-04-24 13:28:13','Inherit','Inherit',4,'timers-and-timesheets','Timers And Timesheets',NULL,'<p>Time is important. It’s a resource, and we know a lot of our end-users base their business workflows on how much of it they have...Time is important. It’s a resource, and we know a lot of our end-users base their business workflows on how much of it they have...Time is important. It’s a resource, and we know a lot of our end-users base their business workflows on how much of it they have...Time is important. It’s a resource, and we know a lot of our end-users base their business workflows on how much of it they have...Time is important. It’s a resource, and we know a lot of our end-users base their business workflows on how much of it they have...Time is important. It’s a resource, and we know a lot of our end-users base their business workflows on how much of it they have...Time is important. It’s a resource, and we know a lot of our end-users base their business workflows on how much of it they have...Time is important. It’s a resource, and we know a lot of our end-users base their business workflows on how much of it they have...Time is important. It’s a resource, and we know a lot of our end-users base their business workflows on how much of it they have...Time is important. It’s a resource, and we know a lot of our end-users base their business workflows on how much of it they have...Time is important. It’s a resource, and we know a lot of our end-users base their business workflows on how much of it they have...Time is important. It’s a resource, and we know a lot of our end-users base their business workflows on how much of it they have...Time is important. It’s a resource, and we know a lot of our end-users base their business workflows on how much of it they have...Time is important. It’s a resource, and we know a lot of our end-users base their business workflows on how much of it they have...Time is important. It’s a resource, and we know a lot of our end-users base their business workflows on how much of it they have...Time is important. It’s a resource, and we know a lot of our end-users base their business workflows on how much of it they have...Time is important. It’s a resource, and we know a lot of our end-users base their business workflows on how much of it they have...</p>',NULL,NULL,0,1,5,0,0,NULL,14),(20,'SilverStripe\\Blog\\Model\\BlogPost','2019-04-24 23:05:46','2019-04-24 22:04:26','Inherit','Inherit',4,'working-on-the-road-our-new-timers-update-is-live','Working on the road? Our new timers update is live!',NULL,'<p>For a long time, timers have been at the heart of our time keeping system. A lot of our clients love how timers can start from anywhere in our web app and mobile app. With this new update, we’re aiming to solve the pain point around adding descriptions to timers while they’re active…<br><br><br>For a long time, timers have been at the heart of our time keeping system. A lot of our clients love how timers can start from anywhere in our web app and mobile app. With this new update, we’re aiming to solve the pain point around adding descriptions to timers while they’re active…<br><br>For a long time, timers have been at the heart of our time keeping system. A lot of our clients love how timers can start from anywhere in our web app and mobile app. With this new update, we’re aiming to solve the pain point around adding descriptions to timers while they’re active…For a long time, timers have been at the heart of our time keeping system. A lot of our clients love how timers can start from anywhere in our web app and mobile app. With this new update, we’re aiming to solve the pain point around adding descriptions to timers while they’re active…</p>',NULL,NULL,0,1,6,0,0,NULL,14),(21,'SilverStripe\\Blog\\Model\\BlogPost','2019-04-25 23:35:17','2019-04-25 23:33:07','Inherit','Inherit',4,'invoice-template-customization','Invoice Template Customization',NULL,'<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt...</p><p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt...</p><p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt...</p><p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt...</p><p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt...</p><p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt...</p><p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt...</p><p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt...</p><p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt...</p><p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt...</p><p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt...</p><p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt...</p>',NULL,NULL,0,1,7,0,0,NULL,14);
/*!40000 ALTER TABLE `SiteTree_Live` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SiteTree_Versions`
--

DROP TABLE IF EXISTS `SiteTree_Versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SiteTree_Versions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `WasPublished` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `WasDeleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `WasDraft` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `AuthorID` int(11) NOT NULL DEFAULT '0',
  `PublisherID` int(11) NOT NULL DEFAULT '0',
  `ClassName` enum('SilverStripe\\CMS\\Model\\SiteTree','Page','SilverStripe\\ErrorPage\\ErrorPage','SilverStripe\\Blog\\Model\\Blog','SilverStripe\\Blog\\Model\\BlogPost','SilverStripe\\CMS\\Model\\RedirectorPage','SilverStripe\\CMS\\Model\\VirtualPage') CHARACTER SET utf8 DEFAULT 'Page',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `CanViewType` enum('Anyone','LoggedInUsers','OnlyTheseUsers','Inherit') CHARACTER SET utf8 DEFAULT 'Inherit',
  `CanEditType` enum('LoggedInUsers','OnlyTheseUsers','Inherit') CHARACTER SET utf8 DEFAULT 'Inherit',
  `URLSegment` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Title` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `MenuTitle` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `Content` mediumtext CHARACTER SET utf8,
  `MetaDescription` mediumtext CHARACTER SET utf8,
  `ExtraMeta` mediumtext CHARACTER SET utf8,
  `ShowInMenus` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ShowInSearch` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Sort` int(11) NOT NULL DEFAULT '0',
  `HasBrokenFile` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `HasBrokenLink` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ReportClass` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `ParentID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `RecordID_Version` (`RecordID`,`Version`),
  KEY `RecordID` (`RecordID`),
  KEY `Version` (`Version`),
  KEY `AuthorID` (`AuthorID`),
  KEY `PublisherID` (`PublisherID`),
  KEY `Sort` (`Sort`),
  KEY `ClassName` (`ClassName`),
  KEY `ParentID` (`ParentID`),
  KEY `URLSegment` (`URLSegment`)
) ENGINE=InnoDB AUTO_INCREMENT=97 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SiteTree_Versions`
--

LOCK TABLES `SiteTree_Versions` WRITE;
/*!40000 ALTER TABLE `SiteTree_Versions` DISABLE KEYS */;
INSERT INTO `SiteTree_Versions` VALUES (1,1,1,0,0,1,0,0,'Page','2019-04-18 18:43:10','2019-04-18 18:43:10','Inherit','Inherit','home','Home',NULL,'<p>Welcome to SilverStripe! This is the default homepage. You can edit this page by opening <a href=\"admin/\">the CMS</a>.</p><p>You can now access the <a href=\"http://docs.silverstripe.org\">developer documentation</a>, or begin the <a href=\"http://www.silverstripe.org/learn/lessons\">SilverStripe lessons</a>.</p>',NULL,NULL,1,1,1,0,0,NULL,0),(2,1,2,1,0,1,0,0,'Page','2019-04-18 18:43:11','2019-04-18 18:43:10','Inherit','Inherit','home','Home',NULL,'<p>Welcome to SilverStripe! This is the default homepage. You can edit this page by opening <a href=\"admin/\">the CMS</a>.</p><p>You can now access the <a href=\"http://docs.silverstripe.org\">developer documentation</a>, or begin the <a href=\"http://www.silverstripe.org/learn/lessons\">SilverStripe lessons</a>.</p>',NULL,NULL,1,1,1,0,0,NULL,0),(3,2,1,0,0,1,0,0,'Page','2019-04-18 18:43:11','2019-04-18 18:43:11','Inherit','Inherit','about-us','About Us',NULL,'<p>You can fill this page out with your own content, or delete it and create your own pages.</p>',NULL,NULL,1,1,2,0,0,NULL,0),(4,2,2,1,0,1,0,0,'Page','2019-04-18 18:43:11','2019-04-18 18:43:11','Inherit','Inherit','about-us','About Us',NULL,'<p>You can fill this page out with your own content, or delete it and create your own pages.</p>',NULL,NULL,1,1,2,0,0,NULL,0),(5,3,1,0,0,1,0,0,'Page','2019-04-18 18:43:11','2019-04-18 18:43:11','Inherit','Inherit','contact-us','Contact Us',NULL,'<p>You can fill this page out with your own content, or delete it and create your own pages.</p>',NULL,NULL,1,1,3,0,0,NULL,0),(6,3,2,1,0,1,0,0,'Page','2019-04-18 18:43:11','2019-04-18 18:43:11','Inherit','Inherit','contact-us','Contact Us',NULL,'<p>You can fill this page out with your own content, or delete it and create your own pages.</p>',NULL,NULL,1,1,3,0,0,NULL,0),(7,4,1,0,0,1,0,0,'SilverStripe\\ErrorPage\\ErrorPage','2019-04-18 18:43:12','2019-04-18 18:43:12','Inherit','Inherit','page-not-found','Page not found',NULL,'<p>Sorry, it seems you were trying to access a page that doesn\'t exist.</p><p>Please check the spelling of the URL you were trying to access and try again.</p>',NULL,NULL,0,0,4,0,0,NULL,0),(8,4,2,1,0,1,0,0,'SilverStripe\\ErrorPage\\ErrorPage','2019-04-18 18:43:13','2019-04-18 18:43:12','Inherit','Inherit','page-not-found','Page not found',NULL,'<p>Sorry, it seems you were trying to access a page that doesn\'t exist.</p><p>Please check the spelling of the URL you were trying to access and try again.</p>',NULL,NULL,0,0,4,0,0,NULL,0),(9,5,1,0,0,1,0,0,'SilverStripe\\ErrorPage\\ErrorPage','2019-04-18 18:43:14','2019-04-18 18:43:14','Inherit','Inherit','server-error','Server error',NULL,'<p>Sorry, there was a problem with handling your request.</p>',NULL,NULL,0,0,5,0,0,NULL,0),(10,5,2,1,0,1,0,0,'SilverStripe\\ErrorPage\\ErrorPage','2019-04-18 18:43:14','2019-04-18 18:43:14','Inherit','Inherit','server-error','Server error',NULL,'<p>Sorry, there was a problem with handling your request.</p>',NULL,NULL,0,0,5,0,0,NULL,0),(11,6,1,0,0,1,1,0,'SilverStripe\\Blog\\Model\\Blog','2019-04-18 21:46:41','2019-04-18 21:46:41','Inherit','Inherit','new-blog','New Blog',NULL,NULL,NULL,NULL,1,1,6,0,0,NULL,0),(12,7,1,0,0,1,1,0,'SilverStripe\\Blog\\Model\\BlogPost','2019-04-18 21:47:12','2019-04-18 21:47:12','Inherit','Inherit','new-blog-post','New Blog Post',NULL,NULL,NULL,NULL,0,1,1,0,0,NULL,6),(13,7,2,0,0,1,1,0,'SilverStripe\\Blog\\Model\\BlogPost','2019-04-18 21:47:49','2019-04-18 21:47:12','Inherit','Inherit','new-blog-post','New Blog Post',NULL,'<p>newww bloogggg posstttttt</p>',NULL,NULL,0,1,1,0,0,NULL,6),(14,7,3,0,0,1,1,0,'SilverStripe\\Blog\\Model\\BlogPost','2019-04-18 21:47:50','2019-04-18 21:47:12','Inherit','Inherit','new-blog-post','New Blog Post',NULL,'<p>newww bloogggg posstttttt</p>',NULL,NULL,0,1,1,0,0,NULL,6),(15,7,4,1,0,1,1,1,'SilverStripe\\Blog\\Model\\BlogPost','2019-04-18 21:47:51','2019-04-18 21:47:12','Inherit','Inherit','new-blog-post','New Blog Post',NULL,'<p>newww bloogggg posstttttt</p>',NULL,NULL,0,1,1,0,0,NULL,6),(16,6,2,1,0,1,1,1,'SilverStripe\\Blog\\Model\\Blog','2019-04-18 21:48:30','2019-04-18 21:46:41','Inherit','Inherit','new-blog','New Blog',NULL,NULL,NULL,NULL,1,1,6,0,0,NULL,0),(17,8,1,0,0,1,1,0,'SilverStripe\\Blog\\Model\\Blog','2019-04-18 22:00:35','2019-04-18 22:00:35','Inherit','Inherit','new-blog-2','New Blog',NULL,NULL,NULL,NULL,1,1,7,0,0,NULL,0),(18,9,1,0,0,1,1,0,'SilverStripe\\Blog\\Model\\BlogPost','2019-04-18 22:02:55','2019-04-18 22:02:55','Inherit','Inherit','new-blog-post','New Blog Post',NULL,NULL,NULL,NULL,0,1,1,0,0,NULL,8),(19,9,2,0,0,1,1,0,'SilverStripe\\Blog\\Model\\BlogPost','2019-04-18 22:03:53','2019-04-18 22:02:55','Inherit','Inherit','new-blog-post-asdnfdklnfadf','New Blog Post asdnfdklnfadf',NULL,'<p>xmzcvnlnadslfkamsdlfkmsadf</p>',NULL,NULL,0,1,1,0,0,NULL,8),(20,9,3,0,0,1,1,0,'SilverStripe\\Blog\\Model\\BlogPost','2019-04-18 22:03:55','2019-04-18 22:02:55','Inherit','Inherit','new-blog-post-asdnfdklnfadf','New Blog Post asdnfdklnfadf',NULL,'<p>xmzcvnlnadslfkamsdlfkmsadf</p>',NULL,NULL,0,1,1,0,0,NULL,8),(21,9,4,1,0,1,1,1,'SilverStripe\\Blog\\Model\\BlogPost','2019-04-18 22:03:55','2019-04-18 22:02:55','Inherit','Inherit','new-blog-post-asdnfdklnfadf','New Blog Post asdnfdklnfadf',NULL,'<p>xmzcvnlnadslfkamsdlfkmsadf</p>',NULL,NULL,0,1,1,0,0,NULL,8),(22,8,2,1,0,1,1,1,'SilverStripe\\Blog\\Model\\Blog','2019-04-18 22:04:32','2019-04-18 22:00:35','Inherit','Inherit','new-blog-2','New Blog',NULL,NULL,NULL,NULL,1,1,7,0,0,NULL,0),(23,8,3,0,0,1,1,0,'SilverStripe\\Blog\\Model\\Blog','2019-04-18 22:10:38','2019-04-18 22:00:35','Inherit','Inherit','new-blog-2','crazy blog of tyhe year 2081',NULL,NULL,NULL,NULL,1,1,7,0,0,NULL,0),(24,8,4,1,0,1,1,1,'SilverStripe\\Blog\\Model\\Blog','2019-04-18 22:10:39','2019-04-18 22:00:35','Inherit','Inherit','new-blog-2','crazy blog of tyhe year 2081',NULL,NULL,NULL,NULL,1,1,7,0,0,NULL,0),(25,8,5,0,0,1,1,0,'SilverStripe\\Blog\\Model\\Blog','2019-04-18 22:26:37','2019-04-18 22:00:35','Inherit','Inherit','new-blog-2','crazy blog of tyhe year 2081sadfasdf',NULL,NULL,NULL,NULL,1,1,7,0,0,NULL,0),(26,8,6,1,0,1,1,1,'SilverStripe\\Blog\\Model\\Blog','2019-04-18 22:26:39','2019-04-18 22:00:35','Inherit','Inherit','new-blog-2','crazy blog of tyhe year 2081sadfasdf',NULL,NULL,NULL,NULL,1,1,7,0,0,NULL,0),(27,8,7,0,0,1,1,0,'SilverStripe\\Blog\\Model\\Blog','2019-04-18 22:30:06','2019-04-18 22:00:35','Inherit','Inherit','new-blog-2','crazy blog of tyhe year 2081sadfasdf','adsfafss',NULL,NULL,NULL,1,1,7,0,0,NULL,0),(28,8,8,1,0,1,1,1,'SilverStripe\\Blog\\Model\\Blog','2019-04-18 22:30:07','2019-04-18 22:00:35','Inherit','Inherit','new-blog-2','crazy blog of tyhe year 2081sadfasdf','adsfafss',NULL,NULL,NULL,1,1,7,0,0,NULL,0),(29,8,9,0,0,1,1,0,'SilverStripe\\Blog\\Model\\Blog','2019-04-18 22:35:16','2019-04-18 22:00:35','Inherit','Inherit','new-blog-2','crazy blog of tyhe year 2081sadfasdf','adsfafssjhjhjhjhj',NULL,NULL,NULL,1,1,7,0,0,NULL,0),(30,8,10,1,0,1,1,1,'SilverStripe\\Blog\\Model\\Blog','2019-04-18 22:35:18','2019-04-18 22:00:35','Inherit','Inherit','new-blog-2','crazy blog of tyhe year 2081sadfasdf','adsfafssjhjhjhjhj',NULL,NULL,NULL,1,1,7,0,0,NULL,0),(31,10,1,0,0,1,1,0,'SilverStripe\\Blog\\Model\\Blog','2019-04-18 22:42:43','2019-04-18 22:42:43','Inherit','Inherit','new-blog-3','New Blog',NULL,NULL,NULL,NULL,1,1,8,0,0,NULL,0),(32,10,2,0,0,1,1,0,'SilverStripe\\Blog\\Model\\Blog','2019-04-18 22:43:14','2019-04-18 22:42:43','Inherit','Inherit','new-blog-3','New Blog 2',NULL,NULL,NULL,NULL,1,1,8,0,0,NULL,0),(33,10,3,1,0,1,1,1,'SilverStripe\\Blog\\Model\\Blog','2019-04-18 22:43:16','2019-04-18 22:42:43','Inherit','Inherit','new-blog-3','New Blog 2',NULL,NULL,NULL,NULL,1,1,8,0,0,NULL,0),(34,11,1,0,0,1,1,0,'SilverStripe\\Blog\\Model\\BlogPost','2019-04-18 22:50:36','2019-04-18 22:50:36','Inherit','Inherit','new-blog-post-2','New Blog Post',NULL,NULL,NULL,NULL,0,1,2,0,0,NULL,6),(35,12,1,0,0,1,1,0,'SilverStripe\\Blog\\Model\\BlogPost','2019-04-18 23:02:32','2019-04-18 23:02:32','Inherit','Inherit','new-blog-post-3','New Blog Post',NULL,NULL,NULL,NULL,0,1,3,0,0,NULL,6),(36,12,2,0,0,1,1,0,'SilverStripe\\Blog\\Model\\BlogPost','2019-04-18 23:03:08','2019-04-18 23:02:32','Inherit','Inherit','new-blog-post-3','aaaarrrgghhhhhh','aaargh','<p>blogoggsg</p>',NULL,NULL,0,1,3,0,0,NULL,6),(37,12,3,0,0,1,1,0,'SilverStripe\\Blog\\Model\\BlogPost','2019-04-18 23:03:10','2019-04-18 23:02:32','Inherit','Inherit','new-blog-post-3','aaaarrrgghhhhhh','aaargh','<p>blogoggsg</p>',NULL,NULL,0,1,3,0,0,NULL,6),(38,12,4,1,0,1,1,1,'SilverStripe\\Blog\\Model\\BlogPost','2019-04-18 23:03:10','2019-04-18 23:02:32','Inherit','Inherit','new-blog-post-3','aaaarrrgghhhhhh','aaargh','<p>blogoggsg</p>',NULL,NULL,0,1,3,0,0,NULL,6),(39,12,5,0,0,1,1,0,'SilverStripe\\Blog\\Model\\BlogPost','2019-04-18 23:03:45','2019-04-18 23:02:32','Inherit','Inherit','new-blog-post-3','aaaarrrgghhhhhh','aaargh','<p>blogoggsg</p>',NULL,NULL,0,1,1,0,0,NULL,6),(40,7,5,1,1,0,1,1,'SilverStripe\\Blog\\Model\\BlogPost','2019-04-18 23:04:54','2019-04-18 21:47:12','Inherit','Inherit',NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,0,NULL,0),(41,12,6,1,1,0,1,1,'SilverStripe\\Blog\\Model\\BlogPost','2019-04-18 23:04:55','2019-04-18 23:02:32','Inherit','Inherit',NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,0,NULL,0),(42,7,6,0,1,1,1,0,'SilverStripe\\Blog\\Model\\BlogPost','2019-04-18 23:04:55','2019-04-18 21:47:12','Inherit','Inherit',NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,0,NULL,0),(43,12,7,0,1,1,1,0,'SilverStripe\\Blog\\Model\\BlogPost','2019-04-18 23:04:56','2019-04-18 23:02:32','Inherit','Inherit',NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,0,NULL,0),(44,11,2,0,1,1,1,0,'SilverStripe\\Blog\\Model\\BlogPost','2019-04-18 23:04:56','2019-04-18 22:50:36','Inherit','Inherit',NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,0,NULL,0),(45,6,3,1,1,1,1,1,'SilverStripe\\Blog\\Model\\Blog','2019-04-18 23:04:56','2019-04-18 21:46:41','Inherit','Inherit',NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,0,NULL,0),(46,9,5,1,1,0,1,1,'SilverStripe\\Blog\\Model\\BlogPost','2019-04-18 23:04:57','2019-04-18 22:02:55','Inherit','Inherit',NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,0,NULL,0),(47,9,6,0,1,1,1,0,'SilverStripe\\Blog\\Model\\BlogPost','2019-04-18 23:04:58','2019-04-18 22:02:55','Inherit','Inherit',NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,0,NULL,0),(48,8,11,1,1,1,1,1,'SilverStripe\\Blog\\Model\\Blog','2019-04-18 23:04:58','2019-04-18 22:00:35','Inherit','Inherit',NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,0,NULL,0),(49,10,4,1,1,1,1,1,'SilverStripe\\Blog\\Model\\Blog','2019-04-18 23:04:59','2019-04-18 22:42:43','Inherit','Inherit',NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,0,NULL,0),(50,13,1,0,0,1,1,0,'SilverStripe\\Blog\\Model\\Blog','2019-04-18 23:05:51','2019-04-18 23:05:51','Inherit','Inherit','new-blog','New Blog',NULL,NULL,NULL,NULL,1,1,6,0,0,NULL,0),(51,13,2,1,0,1,1,1,'SilverStripe\\Blog\\Model\\Blog','2019-04-18 23:06:45','2019-04-18 23:05:51','Inherit','Inherit','new-blog','New Blog',NULL,NULL,NULL,NULL,1,1,6,0,0,NULL,0),(52,2,3,1,1,1,1,1,'Page','2019-04-20 12:11:14','2019-04-18 18:43:11','Inherit','Inherit',NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,0,NULL,0),(53,3,3,1,1,1,1,1,'Page','2019-04-20 12:11:14','2019-04-18 18:43:11','Inherit','Inherit',NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,0,NULL,0),(54,14,1,0,0,1,1,0,'SilverStripe\\Blog\\Model\\Blog','2019-04-20 12:12:09','2019-04-20 12:12:09','Inherit','Inherit','new-blog-2','New Blog',NULL,NULL,NULL,NULL,1,1,7,0,0,NULL,0),(55,14,2,0,0,1,1,0,'SilverStripe\\Blog\\Model\\Blog','2019-04-20 12:14:25','2019-04-20 12:12:09','Inherit','Inherit','blog','Blog',NULL,NULL,NULL,NULL,1,1,7,0,0,NULL,0),(56,14,3,1,0,1,1,1,'SilverStripe\\Blog\\Model\\Blog','2019-04-20 12:14:27','2019-04-20 12:12:09','Inherit','Inherit','blog','Blog',NULL,NULL,NULL,NULL,1,1,7,0,0,NULL,0),(57,15,1,0,0,1,1,0,'SilverStripe\\Blog\\Model\\BlogPost','2019-04-20 12:14:44','2019-04-20 12:14:44','Inherit','Inherit','new-blog-post','New Blog Post',NULL,NULL,NULL,NULL,0,1,1,0,0,NULL,14),(58,15,2,0,0,1,1,0,'SilverStripe\\Blog\\Model\\BlogPost','2019-04-20 12:28:36','2019-04-20 12:14:44','Inherit','Inherit','product-roadmap-2019-q1','Product Roadmap: 2019 Q1','Product Roadmap 2019 Q1','<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</p><p>&nbsp;</p><table><tbody><tr><td>[image src=\"/silverstripe-cms/assets/Uploads/e8fa9273b6/link_oot.png\" id=\"2\" width=\"408\" height=\"408\" class=\"leftAlone ss-htmleditorfield-file image\" title=\"link oot\" alt=\"link\"]</td>\n<td>[image src=\"/silverstripe-cms/assets/Uploads/65114dbce0/mario_64-.png\" id=\"3\" width=\"544\" height=\"400\" class=\"leftAlone ss-htmleditorfield-file image\" title=\"mario 64 \" alt=\"mario\"]</td>\n<td>[image src=\"/silverstripe-cms/assets/Uploads/5e477476bb/wave_race.png\" id=\"4\" width=\"574\" height=\"362\" class=\"leftAlone ss-htmleditorfield-file image\" title=\"wave race\" alt=\"wave\"]</td>\n</tr></tbody></table><p>&nbsp;</p><p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</p><p>&nbsp;</p>',NULL,NULL,0,1,1,0,0,NULL,14),(59,15,3,0,0,1,1,0,'SilverStripe\\Blog\\Model\\BlogPost','2019-04-20 12:28:42','2019-04-20 12:14:44','Inherit','Inherit','product-roadmap-2019-q1','Product Roadmap: 2019 Q1','Product Roadmap 2019 Q1','<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</p><p>&nbsp;</p><table><tbody><tr><td>[image src=\"/silverstripe-cms/assets/Uploads/e8fa9273b6/link_oot.png\" id=\"2\" width=\"408\" height=\"408\" class=\"leftAlone ss-htmleditorfield-file image\" title=\"link oot\" alt=\"link\"]</td>\n<td>[image src=\"/silverstripe-cms/assets/Uploads/65114dbce0/mario_64-.png\" id=\"3\" width=\"544\" height=\"400\" class=\"leftAlone ss-htmleditorfield-file image\" title=\"mario 64 \" alt=\"mario\"]</td>\n<td>[image src=\"/silverstripe-cms/assets/Uploads/5e477476bb/wave_race.png\" id=\"4\" width=\"574\" height=\"362\" class=\"leftAlone ss-htmleditorfield-file image\" title=\"wave race\" alt=\"wave\"]</td>\n</tr></tbody></table><p>&nbsp;</p><p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</p><p>&nbsp;</p>',NULL,NULL,0,1,1,0,0,NULL,14),(60,15,4,1,0,1,1,1,'SilverStripe\\Blog\\Model\\BlogPost','2019-04-20 12:28:46','2019-04-20 12:14:44','Inherit','Inherit','product-roadmap-2019-q1','Product Roadmap: 2019 Q1','Product Roadmap 2019 Q1','<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</p><p>&nbsp;</p><table><tbody><tr><td>[image src=\"/silverstripe-cms/assets/Uploads/e8fa9273b6/link_oot.png\" id=\"2\" width=\"408\" height=\"408\" class=\"leftAlone ss-htmleditorfield-file image\" title=\"link oot\" alt=\"link\"]</td>\n<td>[image src=\"/silverstripe-cms/assets/Uploads/65114dbce0/mario_64-.png\" id=\"3\" width=\"544\" height=\"400\" class=\"leftAlone ss-htmleditorfield-file image\" title=\"mario 64 \" alt=\"mario\"]</td>\n<td>[image src=\"/silverstripe-cms/assets/Uploads/5e477476bb/wave_race.png\" id=\"4\" width=\"574\" height=\"362\" class=\"leftAlone ss-htmleditorfield-file image\" title=\"wave race\" alt=\"wave\"]</td>\n</tr></tbody></table><p>&nbsp;</p><p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</p><p>&nbsp;</p>',NULL,NULL,0,1,1,0,0,NULL,14),(61,15,5,0,0,1,1,0,'SilverStripe\\Blog\\Model\\BlogPost','2019-04-21 10:09:47','2019-04-20 12:14:44','Inherit','Inherit','product-roadmap-2019-q1','Product Roadmap: 2019 Q1','Product Roadmap 2019 Q1','<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</p><div class=\"image-container\">[image src=\"/silverstripe-cms/assets/Uploads/e8fa9273b6/link_oot.png\" id=\"2\" width=\"292\" height=\"292\" class=\"container-image\" title=\"link oot\" alt=\"link\"][image src=\"/silverstripe-cms/assets/Uploads/65114dbce0/mario_64-.png\" id=\"3\" width=\"307\" height=\"226\" class=\"container-image\" title=\"mario 64 \" alt=\"mario\"][image src=\"/silverstripe-cms/assets/Uploads/5e477476bb/wave_race.png\" id=\"4\" width=\"574\" height=\"362\" class=\"container-image\" title=\"wave race\" alt=\"wave\"]</div><p>&nbsp;</p><p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</p><p>&nbsp;</p>',NULL,NULL,0,1,1,0,0,NULL,14),(62,15,6,1,0,1,1,1,'SilverStripe\\Blog\\Model\\BlogPost','2019-04-21 10:09:53','2019-04-20 12:14:44','Inherit','Inherit','product-roadmap-2019-q1','Product Roadmap: 2019 Q1','Product Roadmap 2019 Q1','<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</p><div class=\"image-container\">[image src=\"/silverstripe-cms/assets/Uploads/e8fa9273b6/link_oot.png\" id=\"2\" width=\"292\" height=\"292\" class=\"container-image\" title=\"link oot\" alt=\"link\"][image src=\"/silverstripe-cms/assets/Uploads/65114dbce0/mario_64-.png\" id=\"3\" width=\"307\" height=\"226\" class=\"container-image\" title=\"mario 64 \" alt=\"mario\"][image src=\"/silverstripe-cms/assets/Uploads/5e477476bb/wave_race.png\" id=\"4\" width=\"574\" height=\"362\" class=\"container-image\" title=\"wave race\" alt=\"wave\"]</div><p>&nbsp;</p><p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</p><p>&nbsp;</p>',NULL,NULL,0,1,1,0,0,NULL,14),(63,15,7,0,0,1,1,0,'SilverStripe\\Blog\\Model\\BlogPost','2019-04-21 10:56:28','2019-04-20 12:14:44','Inherit','Inherit','product-roadmap-2019-q1','Product Roadmap: 2019 Q1','Product Roadmap 2019 Q1','<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</p><div class=\"image-container\">[image src=\"/silverstripe-cms/assets/Uploads/e8fa9273b6/link_oot.png\" id=\"2\" width=\"292\" height=\"292\" class=\"container-image ss-htmleditorfield-file image\" title=\"link oot\" alt=\"link\"][image src=\"/silverstripe-cms/assets/Uploads/65114dbce0/mario_64-.png\" id=\"3\" width=\"307\" height=\"226\" class=\"container-image ss-htmleditorfield-file image\" title=\"mario 64 \" alt=\"mario\"][image src=\"/silverstripe-cms/assets/Uploads/5e477476bb/wave_race.png\" id=\"4\" width=\"574\" height=\"362\" class=\"container-image ss-htmleditorfield-file image\" title=\"wave race\" alt=\"wave\"]</div><p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</p>',NULL,NULL,0,1,1,0,0,NULL,14),(64,15,8,1,0,1,1,1,'SilverStripe\\Blog\\Model\\BlogPost','2019-04-21 10:56:33','2019-04-20 12:14:44','Inherit','Inherit','product-roadmap-2019-q1','Product Roadmap: 2019 Q1','Product Roadmap 2019 Q1','<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</p><div class=\"image-container\">[image src=\"/silverstripe-cms/assets/Uploads/e8fa9273b6/link_oot.png\" id=\"2\" width=\"292\" height=\"292\" class=\"container-image ss-htmleditorfield-file image\" title=\"link oot\" alt=\"link\"][image src=\"/silverstripe-cms/assets/Uploads/65114dbce0/mario_64-.png\" id=\"3\" width=\"307\" height=\"226\" class=\"container-image ss-htmleditorfield-file image\" title=\"mario 64 \" alt=\"mario\"][image src=\"/silverstripe-cms/assets/Uploads/5e477476bb/wave_race.png\" id=\"4\" width=\"574\" height=\"362\" class=\"container-image ss-htmleditorfield-file image\" title=\"wave race\" alt=\"wave\"]</div><p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</p>',NULL,NULL,0,1,1,0,0,NULL,14),(65,16,1,0,0,1,1,0,'SilverStripe\\Blog\\Model\\BlogPost','2019-04-21 11:45:22','2019-04-21 11:45:22','Inherit','Inherit','new-blog-post','New Blog Post',NULL,NULL,NULL,NULL,0,1,2,0,0,NULL,14),(66,16,2,0,0,1,1,0,'SilverStripe\\Blog\\Model\\BlogPost','2019-04-21 11:49:15','2019-04-21 11:45:22','Inherit','Inherit','q1-priorities-2019','Q1 Priorities 2019',NULL,'<p><span>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</span></p><p><span>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</span></p>',NULL,NULL,0,1,2,0,0,NULL,14),(67,16,3,0,0,1,1,0,'SilverStripe\\Blog\\Model\\BlogPost','2019-04-21 11:49:17','2019-04-21 11:45:22','Inherit','Inherit','q1-priorities-2019','Q1 Priorities 2019',NULL,'<p><span>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</span></p><p><span>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</span></p>',NULL,NULL,0,1,2,0,0,NULL,14),(68,16,4,1,0,1,1,1,'SilverStripe\\Blog\\Model\\BlogPost','2019-04-21 11:49:18','2019-04-21 11:45:22','Inherit','Inherit','q1-priorities-2019','Q1 Priorities 2019',NULL,'<p><span>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</span></p><p><span>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</span></p>',NULL,NULL,0,1,2,0,0,NULL,14),(69,15,9,0,0,1,1,0,'SilverStripe\\Blog\\Model\\BlogPost','2019-04-22 23:31:16','2019-04-20 12:14:44','Inherit','Inherit','product-roadmap-2019-q1','Product Roadmap: 2019 Q1','Product Roadmap 2019 Q1','<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</p><div class=\"image-container\">[image src=\"/silverstripe-cms/assets/Uploads/e8fa9273b6/link_oot.png\" id=\"2\" width=\"292\" height=\"292\" class=\"container-image ss-htmleditorfield-file image\" title=\"link oot\" alt=\"link\"][image src=\"/silverstripe-cms/assets/Uploads/65114dbce0/mario_64-.png\" id=\"3\" width=\"307\" height=\"226\" class=\"container-image ss-htmleditorfield-file image\" title=\"mario 64 \" alt=\"mario\"][image src=\"/silverstripe-cms/assets/Uploads/5e477476bb/wave_race.png\" id=\"4\" width=\"574\" height=\"362\" class=\"container-image ss-htmleditorfield-file image\" title=\"wave race\" alt=\"wave\"]</div><p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</p><div class=\"image-container\">[image src=\"/silverstripe-cms/assets/a8a148aa9e/king-kong.jpg\" id=\"9\" width=\"1446\" height=\"766\" class=\"leftAlone ss-htmleditorfield-file image\" title=\"king kong\" alt=\"king-kong\"]</div>',NULL,NULL,0,1,1,0,0,NULL,14),(70,15,10,1,0,1,1,1,'SilverStripe\\Blog\\Model\\BlogPost','2019-04-22 23:31:22','2019-04-20 12:14:44','Inherit','Inherit','product-roadmap-2019-q1','Product Roadmap: 2019 Q1','Product Roadmap 2019 Q1','<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</p><div class=\"image-container\">[image src=\"/silverstripe-cms/assets/Uploads/e8fa9273b6/link_oot.png\" id=\"2\" width=\"292\" height=\"292\" class=\"container-image ss-htmleditorfield-file image\" title=\"link oot\" alt=\"link\"][image src=\"/silverstripe-cms/assets/Uploads/65114dbce0/mario_64-.png\" id=\"3\" width=\"307\" height=\"226\" class=\"container-image ss-htmleditorfield-file image\" title=\"mario 64 \" alt=\"mario\"][image src=\"/silverstripe-cms/assets/Uploads/5e477476bb/wave_race.png\" id=\"4\" width=\"574\" height=\"362\" class=\"container-image ss-htmleditorfield-file image\" title=\"wave race\" alt=\"wave\"]</div><p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</p><div class=\"image-container\">[image src=\"/silverstripe-cms/assets/a8a148aa9e/king-kong.jpg\" id=\"9\" width=\"1446\" height=\"766\" class=\"leftAlone ss-htmleditorfield-file image\" title=\"king kong\" alt=\"king-kong\"]</div>',NULL,NULL,0,1,1,0,0,NULL,14),(71,15,11,0,0,1,1,0,'SilverStripe\\Blog\\Model\\BlogPost','2019-04-22 23:33:44','2019-04-20 12:14:44','Inherit','Inherit','product-roadmap-2019-q1','Product Roadmap: 2019 Q1','Product Roadmap 2019 Q1','<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</p><div class=\"image-container\">[image src=\"/silverstripe-cms/assets/Uploads/e8fa9273b6/link_oot.png\" id=\"2\" width=\"292\" height=\"292\" class=\"container-image ss-htmleditorfield-file image\" title=\"link oot\" alt=\"link\"][image src=\"/silverstripe-cms/assets/Uploads/65114dbce0/mario_64-.png\" id=\"3\" width=\"307\" height=\"226\" class=\"container-image ss-htmleditorfield-file image\" title=\"mario 64 \" alt=\"mario\"][image src=\"/silverstripe-cms/assets/Uploads/5e477476bb/wave_race.png\" id=\"4\" width=\"574\" height=\"362\" class=\"container-image ss-htmleditorfield-file image\" title=\"wave race\" alt=\"wave\"]</div><p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</p><div class=\"image-container\">[image src=\"/silverstripe-cms/assets/a8a148aa9e/king-kong.jpg\" id=\"9\" width=\"1446\" height=\"766\" class=\"container-image ss-htmleditorfield-file image\" title=\"king kong\" alt=\"king-kong\"]</div>',NULL,NULL,0,1,1,0,0,NULL,14),(72,15,12,1,0,1,1,1,'SilverStripe\\Blog\\Model\\BlogPost','2019-04-22 23:33:49','2019-04-20 12:14:44','Inherit','Inherit','product-roadmap-2019-q1','Product Roadmap: 2019 Q1','Product Roadmap 2019 Q1','<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</p><div class=\"image-container\">[image src=\"/silverstripe-cms/assets/Uploads/e8fa9273b6/link_oot.png\" id=\"2\" width=\"292\" height=\"292\" class=\"container-image ss-htmleditorfield-file image\" title=\"link oot\" alt=\"link\"][image src=\"/silverstripe-cms/assets/Uploads/65114dbce0/mario_64-.png\" id=\"3\" width=\"307\" height=\"226\" class=\"container-image ss-htmleditorfield-file image\" title=\"mario 64 \" alt=\"mario\"][image src=\"/silverstripe-cms/assets/Uploads/5e477476bb/wave_race.png\" id=\"4\" width=\"574\" height=\"362\" class=\"container-image ss-htmleditorfield-file image\" title=\"wave race\" alt=\"wave\"]</div><p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</p><div class=\"image-container\">[image src=\"/silverstripe-cms/assets/a8a148aa9e/king-kong.jpg\" id=\"9\" width=\"1446\" height=\"766\" class=\"container-image ss-htmleditorfield-file image\" title=\"king kong\" alt=\"king-kong\"]</div>',NULL,NULL,0,1,1,0,0,NULL,14),(73,15,13,0,0,1,1,0,'SilverStripe\\Blog\\Model\\BlogPost','2019-04-22 23:41:24','2019-04-20 12:14:44','Inherit','Inherit','product-roadmap-2019-q1','Product Roadmap: 2019 Q1','Product Roadmap 2019 Q1','<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</p><div class=\"image-container\">[image src=\"/silverstripe-cms/assets/Uploads/e8fa9273b6/link_oot.png\" id=\"2\" width=\"292\" height=\"292\" class=\"container-image ss-htmleditorfield-file image\" title=\"link oot\" alt=\"link\"][image src=\"/silverstripe-cms/assets/Uploads/65114dbce0/mario_64-.png\" id=\"3\" width=\"307\" height=\"226\" class=\"container-image ss-htmleditorfield-file image\" title=\"mario 64 \" alt=\"mario\"][image src=\"/silverstripe-cms/assets/Uploads/5e477476bb/wave_race.png\" id=\"4\" width=\"574\" height=\"362\" class=\"container-image ss-htmleditorfield-file image\" title=\"wave race\" alt=\"wave\"]</div><p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</p><div class=\"image-container\">[image src=\"/silverstripe-cms/assets/a8a148aa9e/king-kong.jpg\" id=\"9\" width=\"1446\" height=\"766\" class=\"container-image-single ss-htmleditorfield-file image\" title=\"king kong\" alt=\"king-kong\"]</div>',NULL,NULL,0,1,1,0,0,NULL,14),(74,15,14,1,0,1,1,1,'SilverStripe\\Blog\\Model\\BlogPost','2019-04-22 23:41:30','2019-04-20 12:14:44','Inherit','Inherit','product-roadmap-2019-q1','Product Roadmap: 2019 Q1','Product Roadmap 2019 Q1','<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</p><div class=\"image-container\">[image src=\"/silverstripe-cms/assets/Uploads/e8fa9273b6/link_oot.png\" id=\"2\" width=\"292\" height=\"292\" class=\"container-image ss-htmleditorfield-file image\" title=\"link oot\" alt=\"link\"][image src=\"/silverstripe-cms/assets/Uploads/65114dbce0/mario_64-.png\" id=\"3\" width=\"307\" height=\"226\" class=\"container-image ss-htmleditorfield-file image\" title=\"mario 64 \" alt=\"mario\"][image src=\"/silverstripe-cms/assets/Uploads/5e477476bb/wave_race.png\" id=\"4\" width=\"574\" height=\"362\" class=\"container-image ss-htmleditorfield-file image\" title=\"wave race\" alt=\"wave\"]</div><p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</p><div class=\"image-container\">[image src=\"/silverstripe-cms/assets/a8a148aa9e/king-kong.jpg\" id=\"9\" width=\"1446\" height=\"766\" class=\"container-image-single ss-htmleditorfield-file image\" title=\"king kong\" alt=\"king-kong\"]</div>',NULL,NULL,0,1,1,0,0,NULL,14),(75,17,1,0,0,1,1,0,'SilverStripe\\Blog\\Model\\BlogPost','2019-04-23 23:40:42','2019-04-23 23:40:42','Inherit','Inherit','new-blog-post','New Blog Post',NULL,NULL,NULL,NULL,0,1,3,0,0,NULL,14),(76,17,2,0,0,1,1,0,'SilverStripe\\Blog\\Model\\BlogPost','2019-04-24 00:33:41','2019-04-23 23:40:42','Inherit','Inherit','scheduling-and-rates','Scheduling & Rates',NULL,'<p>Knowing how your staff are tracking against a budget is something our projects module aims to do as a core feature. The latest update...Knowing how your staff are tracking against a budget is something our projects module aims to do as a core feature. The latest update...Knowing how your staff are tracking against a budget is something our projects module aims to do as a core feature. The latest update...Knowing how your staff are tracking against a budget is something our projects module aims to do as a core feature. The latest update...Knowing how your staff are tracking against a budget is something our projects module aims to do as a core feature. The latest update...Knowing how your staff are tracking against a budget is something our projects module aims to do as a core feature. The latest update...Knowing how your staff are tracking against a budget is something our projects module aims to do as a core feature. The latest update...Knowing how your staff are tracking against a budget is something our projects module aims to do as a core feature. The latest update...Knowing how your staff are tracking against a budget is something our projects module aims to do as a core feature. The latest update...Knowing how your staff are tracking against a budget is something our projects module aims to do as a core feature. The latest update...Knowing how your staff are tracking against a budget is something our projects module aims to do as a core feature. The latest update...Knowing how your staff are tracking against a budget is something our projects module aims to do as a core feature. The latest update...Knowing how your staff are tracking against a budget is something our projects module aims to do as a core feature. The latest update...Knowing how your staff are tracking against a budget is something our projects module aims to do as a core feature. The latest update...Knowing how your staff are tracking against a budget is something our projects module aims to do as a core feature. The latest update...Knowing how your staff are tracking against a budget is something our projects module aims to do as a core feature. The latest update...Knowing how your staff are tracking against a budget is something our projects module aims to do as a core feature. The latest update...Knowing how your staff are tracking against a budget is something our projects module aims to do as a core feature. The latest update...Knowing how your staff are tracking against a budget is something our projects module aims to do as a core feature. The latest update...Knowing how your staff are tracking against a budget is something our projects module aims to do as a core feature. The latest update...</p>',NULL,NULL,0,1,3,0,0,NULL,14),(77,17,3,0,0,1,1,0,'SilverStripe\\Blog\\Model\\BlogPost','2019-04-24 00:33:43','2019-04-23 23:40:42','Inherit','Inherit','scheduling-and-rates','Scheduling & Rates',NULL,'<p>Knowing how your staff are tracking against a budget is something our projects module aims to do as a core feature. The latest update...Knowing how your staff are tracking against a budget is something our projects module aims to do as a core feature. The latest update...Knowing how your staff are tracking against a budget is something our projects module aims to do as a core feature. The latest update...Knowing how your staff are tracking against a budget is something our projects module aims to do as a core feature. The latest update...Knowing how your staff are tracking against a budget is something our projects module aims to do as a core feature. The latest update...Knowing how your staff are tracking against a budget is something our projects module aims to do as a core feature. The latest update...Knowing how your staff are tracking against a budget is something our projects module aims to do as a core feature. The latest update...Knowing how your staff are tracking against a budget is something our projects module aims to do as a core feature. The latest update...Knowing how your staff are tracking against a budget is something our projects module aims to do as a core feature. The latest update...Knowing how your staff are tracking against a budget is something our projects module aims to do as a core feature. The latest update...Knowing how your staff are tracking against a budget is something our projects module aims to do as a core feature. The latest update...Knowing how your staff are tracking against a budget is something our projects module aims to do as a core feature. The latest update...Knowing how your staff are tracking against a budget is something our projects module aims to do as a core feature. The latest update...Knowing how your staff are tracking against a budget is something our projects module aims to do as a core feature. The latest update...Knowing how your staff are tracking against a budget is something our projects module aims to do as a core feature. The latest update...Knowing how your staff are tracking against a budget is something our projects module aims to do as a core feature. The latest update...Knowing how your staff are tracking against a budget is something our projects module aims to do as a core feature. The latest update...Knowing how your staff are tracking against a budget is something our projects module aims to do as a core feature. The latest update...Knowing how your staff are tracking against a budget is something our projects module aims to do as a core feature. The latest update...Knowing how your staff are tracking against a budget is something our projects module aims to do as a core feature. The latest update...</p>',NULL,NULL,0,1,3,0,0,NULL,14),(78,17,4,1,0,1,1,1,'SilverStripe\\Blog\\Model\\BlogPost','2019-04-24 00:33:43','2019-04-23 23:40:42','Inherit','Inherit','scheduling-and-rates','Scheduling & Rates',NULL,'<p>Knowing how your staff are tracking against a budget is something our projects module aims to do as a core feature. The latest update...Knowing how your staff are tracking against a budget is something our projects module aims to do as a core feature. The latest update...Knowing how your staff are tracking against a budget is something our projects module aims to do as a core feature. The latest update...Knowing how your staff are tracking against a budget is something our projects module aims to do as a core feature. The latest update...Knowing how your staff are tracking against a budget is something our projects module aims to do as a core feature. The latest update...Knowing how your staff are tracking against a budget is something our projects module aims to do as a core feature. The latest update...Knowing how your staff are tracking against a budget is something our projects module aims to do as a core feature. The latest update...Knowing how your staff are tracking against a budget is something our projects module aims to do as a core feature. The latest update...Knowing how your staff are tracking against a budget is something our projects module aims to do as a core feature. The latest update...Knowing how your staff are tracking against a budget is something our projects module aims to do as a core feature. The latest update...Knowing how your staff are tracking against a budget is something our projects module aims to do as a core feature. The latest update...Knowing how your staff are tracking against a budget is something our projects module aims to do as a core feature. The latest update...Knowing how your staff are tracking against a budget is something our projects module aims to do as a core feature. The latest update...Knowing how your staff are tracking against a budget is something our projects module aims to do as a core feature. The latest update...Knowing how your staff are tracking against a budget is something our projects module aims to do as a core feature. The latest update...Knowing how your staff are tracking against a budget is something our projects module aims to do as a core feature. The latest update...Knowing how your staff are tracking against a budget is something our projects module aims to do as a core feature. The latest update...Knowing how your staff are tracking against a budget is something our projects module aims to do as a core feature. The latest update...Knowing how your staff are tracking against a budget is something our projects module aims to do as a core feature. The latest update...Knowing how your staff are tracking against a budget is something our projects module aims to do as a core feature. The latest update...</p>',NULL,NULL,0,1,3,0,0,NULL,14),(79,18,1,0,0,1,1,0,'SilverStripe\\Blog\\Model\\BlogPost','2019-04-24 00:46:42','2019-04-24 00:46:42','Inherit','Inherit','new-blog-post','New Blog Post',NULL,NULL,NULL,NULL,0,1,4,0,0,NULL,14),(80,18,2,0,0,1,1,0,'SilverStripe\\Blog\\Model\\BlogPost','2019-04-24 00:48:33','2019-04-24 00:46:42','Inherit','Inherit','digital-agencies-our-future-of-media','Digital Agencies: Our Future Of Media',NULL,'<p>2019 is shaping up to be a big year at Accelo, and we have some awesome new features to help you and your business and...2019 is shaping up to be a big year at Accelo, and we have some awesome new features to help you and your business and...2019 is shaping up to be a big year at Accelo, and we have some awesome new features to help you and your business and...2019 is shaping up to be a big year at Accelo, and we have some awesome new features to help you and your business and...2019 is shaping up to be a big year at Accelo, and we have some awesome new features to help you and your business and...2019 is shaping up to be a big year at Accelo, and we have some awesome new features to help you and your business and...2019 is shaping up to be a big year at Accelo, and we have some awesome new features to help you and your business and...2019 is shaping up to be a big year at Accelo, and we have some awesome new features to help you and your business and...2019 is shaping up to be a big year at Accelo, and we have some awesome new features to help you and your business and...2019 is shaping up to be a big year at Accelo, and we have some awesome new features to help you and your business and...2019 is shaping up to be a big year at Accelo, and we have some awesome new features to help you and your business and...2019 is shaping up to be a big year at Accelo, and we have some awesome new features to help you and your business and...2019 is shaping up to be a big year at Accelo, and we have some awesome new features to help you and your business and...2019 is shaping up to be a big year at Accelo, and we have some awesome new features to help you and your business and...2019 is shaping up to be a big year at Accelo, and we have some awesome new features to help you and your business and...2019 is shaping up to be a big year at Accelo, and we have some awesome new features to help you and your business and...2019 is shaping up to be a big year at Accelo, and we have some awesome new features to help you and your business and...2019 is shaping up to be a big year at Accelo, and we have some awesome new features to help you and your business and...</p>',NULL,NULL,0,1,4,0,0,NULL,14),(81,18,3,0,0,1,1,0,'SilverStripe\\Blog\\Model\\BlogPost','2019-04-24 00:48:35','2019-04-24 00:46:42','Inherit','Inherit','digital-agencies-our-future-of-media','Digital Agencies: Our Future Of Media',NULL,'<p>2019 is shaping up to be a big year at Accelo, and we have some awesome new features to help you and your business and...2019 is shaping up to be a big year at Accelo, and we have some awesome new features to help you and your business and...2019 is shaping up to be a big year at Accelo, and we have some awesome new features to help you and your business and...2019 is shaping up to be a big year at Accelo, and we have some awesome new features to help you and your business and...2019 is shaping up to be a big year at Accelo, and we have some awesome new features to help you and your business and...2019 is shaping up to be a big year at Accelo, and we have some awesome new features to help you and your business and...2019 is shaping up to be a big year at Accelo, and we have some awesome new features to help you and your business and...2019 is shaping up to be a big year at Accelo, and we have some awesome new features to help you and your business and...2019 is shaping up to be a big year at Accelo, and we have some awesome new features to help you and your business and...2019 is shaping up to be a big year at Accelo, and we have some awesome new features to help you and your business and...2019 is shaping up to be a big year at Accelo, and we have some awesome new features to help you and your business and...2019 is shaping up to be a big year at Accelo, and we have some awesome new features to help you and your business and...2019 is shaping up to be a big year at Accelo, and we have some awesome new features to help you and your business and...2019 is shaping up to be a big year at Accelo, and we have some awesome new features to help you and your business and...2019 is shaping up to be a big year at Accelo, and we have some awesome new features to help you and your business and...2019 is shaping up to be a big year at Accelo, and we have some awesome new features to help you and your business and...2019 is shaping up to be a big year at Accelo, and we have some awesome new features to help you and your business and...2019 is shaping up to be a big year at Accelo, and we have some awesome new features to help you and your business and...</p>',NULL,NULL,0,1,4,0,0,NULL,14),(82,18,4,1,0,1,1,1,'SilverStripe\\Blog\\Model\\BlogPost','2019-04-24 00:48:36','2019-04-24 00:46:42','Inherit','Inherit','digital-agencies-our-future-of-media','Digital Agencies: Our Future Of Media',NULL,'<p>2019 is shaping up to be a big year at Accelo, and we have some awesome new features to help you and your business and...2019 is shaping up to be a big year at Accelo, and we have some awesome new features to help you and your business and...2019 is shaping up to be a big year at Accelo, and we have some awesome new features to help you and your business and...2019 is shaping up to be a big year at Accelo, and we have some awesome new features to help you and your business and...2019 is shaping up to be a big year at Accelo, and we have some awesome new features to help you and your business and...2019 is shaping up to be a big year at Accelo, and we have some awesome new features to help you and your business and...2019 is shaping up to be a big year at Accelo, and we have some awesome new features to help you and your business and...2019 is shaping up to be a big year at Accelo, and we have some awesome new features to help you and your business and...2019 is shaping up to be a big year at Accelo, and we have some awesome new features to help you and your business and...2019 is shaping up to be a big year at Accelo, and we have some awesome new features to help you and your business and...2019 is shaping up to be a big year at Accelo, and we have some awesome new features to help you and your business and...2019 is shaping up to be a big year at Accelo, and we have some awesome new features to help you and your business and...2019 is shaping up to be a big year at Accelo, and we have some awesome new features to help you and your business and...2019 is shaping up to be a big year at Accelo, and we have some awesome new features to help you and your business and...2019 is shaping up to be a big year at Accelo, and we have some awesome new features to help you and your business and...2019 is shaping up to be a big year at Accelo, and we have some awesome new features to help you and your business and...2019 is shaping up to be a big year at Accelo, and we have some awesome new features to help you and your business and...2019 is shaping up to be a big year at Accelo, and we have some awesome new features to help you and your business and...</p>',NULL,NULL,0,1,4,0,0,NULL,14),(83,19,1,0,0,1,1,0,'SilverStripe\\Blog\\Model\\BlogPost','2019-04-24 13:28:13','2019-04-24 13:28:13','Inherit','Inherit','new-blog-post','New Blog Post',NULL,NULL,NULL,NULL,0,1,5,0,0,NULL,14),(84,19,2,0,0,1,1,0,'SilverStripe\\Blog\\Model\\BlogPost','2019-04-24 13:31:01','2019-04-24 13:28:13','Inherit','Inherit','timers-and-timesheets','Timers And Timesheets',NULL,'<p>Time is important. It’s a resource, and we know a lot of our end-users base their business workflows on how much of it they have...Time is important. It’s a resource, and we know a lot of our end-users base their business workflows on how much of it they have...Time is important. It’s a resource, and we know a lot of our end-users base their business workflows on how much of it they have...Time is important. It’s a resource, and we know a lot of our end-users base their business workflows on how much of it they have...Time is important. It’s a resource, and we know a lot of our end-users base their business workflows on how much of it they have...Time is important. It’s a resource, and we know a lot of our end-users base their business workflows on how much of it they have...Time is important. It’s a resource, and we know a lot of our end-users base their business workflows on how much of it they have...Time is important. It’s a resource, and we know a lot of our end-users base their business workflows on how much of it they have...Time is important. It’s a resource, and we know a lot of our end-users base their business workflows on how much of it they have...Time is important. It’s a resource, and we know a lot of our end-users base their business workflows on how much of it they have...Time is important. It’s a resource, and we know a lot of our end-users base their business workflows on how much of it they have...Time is important. It’s a resource, and we know a lot of our end-users base their business workflows on how much of it they have...Time is important. It’s a resource, and we know a lot of our end-users base their business workflows on how much of it they have...Time is important. It’s a resource, and we know a lot of our end-users base their business workflows on how much of it they have...Time is important. It’s a resource, and we know a lot of our end-users base their business workflows on how much of it they have...Time is important. It’s a resource, and we know a lot of our end-users base their business workflows on how much of it they have...Time is important. It’s a resource, and we know a lot of our end-users base their business workflows on how much of it they have...</p>',NULL,NULL,0,1,5,0,0,NULL,14),(85,19,3,0,0,1,1,0,'SilverStripe\\Blog\\Model\\BlogPost','2019-04-24 13:31:03','2019-04-24 13:28:13','Inherit','Inherit','timers-and-timesheets','Timers And Timesheets',NULL,'<p>Time is important. It’s a resource, and we know a lot of our end-users base their business workflows on how much of it they have...Time is important. It’s a resource, and we know a lot of our end-users base their business workflows on how much of it they have...Time is important. It’s a resource, and we know a lot of our end-users base their business workflows on how much of it they have...Time is important. It’s a resource, and we know a lot of our end-users base their business workflows on how much of it they have...Time is important. It’s a resource, and we know a lot of our end-users base their business workflows on how much of it they have...Time is important. It’s a resource, and we know a lot of our end-users base their business workflows on how much of it they have...Time is important. It’s a resource, and we know a lot of our end-users base their business workflows on how much of it they have...Time is important. It’s a resource, and we know a lot of our end-users base their business workflows on how much of it they have...Time is important. It’s a resource, and we know a lot of our end-users base their business workflows on how much of it they have...Time is important. It’s a resource, and we know a lot of our end-users base their business workflows on how much of it they have...Time is important. It’s a resource, and we know a lot of our end-users base their business workflows on how much of it they have...Time is important. It’s a resource, and we know a lot of our end-users base their business workflows on how much of it they have...Time is important. It’s a resource, and we know a lot of our end-users base their business workflows on how much of it they have...Time is important. It’s a resource, and we know a lot of our end-users base their business workflows on how much of it they have...Time is important. It’s a resource, and we know a lot of our end-users base their business workflows on how much of it they have...Time is important. It’s a resource, and we know a lot of our end-users base their business workflows on how much of it they have...Time is important. It’s a resource, and we know a lot of our end-users base their business workflows on how much of it they have...</p>',NULL,NULL,0,1,5,0,0,NULL,14),(86,19,4,1,0,1,1,1,'SilverStripe\\Blog\\Model\\BlogPost','2019-04-24 13:31:03','2019-04-24 13:28:13','Inherit','Inherit','timers-and-timesheets','Timers And Timesheets',NULL,'<p>Time is important. It’s a resource, and we know a lot of our end-users base their business workflows on how much of it they have...Time is important. It’s a resource, and we know a lot of our end-users base their business workflows on how much of it they have...Time is important. It’s a resource, and we know a lot of our end-users base their business workflows on how much of it they have...Time is important. It’s a resource, and we know a lot of our end-users base their business workflows on how much of it they have...Time is important. It’s a resource, and we know a lot of our end-users base their business workflows on how much of it they have...Time is important. It’s a resource, and we know a lot of our end-users base their business workflows on how much of it they have...Time is important. It’s a resource, and we know a lot of our end-users base their business workflows on how much of it they have...Time is important. It’s a resource, and we know a lot of our end-users base their business workflows on how much of it they have...Time is important. It’s a resource, and we know a lot of our end-users base their business workflows on how much of it they have...Time is important. It’s a resource, and we know a lot of our end-users base their business workflows on how much of it they have...Time is important. It’s a resource, and we know a lot of our end-users base their business workflows on how much of it they have...Time is important. It’s a resource, and we know a lot of our end-users base their business workflows on how much of it they have...Time is important. It’s a resource, and we know a lot of our end-users base their business workflows on how much of it they have...Time is important. It’s a resource, and we know a lot of our end-users base their business workflows on how much of it they have...Time is important. It’s a resource, and we know a lot of our end-users base their business workflows on how much of it they have...Time is important. It’s a resource, and we know a lot of our end-users base their business workflows on how much of it they have...Time is important. It’s a resource, and we know a lot of our end-users base their business workflows on how much of it they have...</p>',NULL,NULL,0,1,5,0,0,NULL,14),(87,20,1,0,0,1,1,0,'SilverStripe\\Blog\\Model\\BlogPost','2019-04-24 22:04:26','2019-04-24 22:04:26','Inherit','Inherit','new-blog-post','New Blog Post',NULL,NULL,NULL,NULL,0,1,6,0,0,NULL,14),(88,20,2,0,0,1,1,0,'SilverStripe\\Blog\\Model\\BlogPost','2019-04-24 23:05:43','2019-04-24 22:04:26','Inherit','Inherit','working-on-the-road-our-new-timers-update-is-live','Working on the road? Our new timers update is live!',NULL,'<p>For a long time, timers have been at the heart of our time keeping system. A lot of our clients love how timers can start from anywhere in our web app and mobile app. With this new update, we’re aiming to solve the pain point around adding descriptions to timers while they’re active…<br><br><br>For a long time, timers have been at the heart of our time keeping system. A lot of our clients love how timers can start from anywhere in our web app and mobile app. With this new update, we’re aiming to solve the pain point around adding descriptions to timers while they’re active…<br><br>For a long time, timers have been at the heart of our time keeping system. A lot of our clients love how timers can start from anywhere in our web app and mobile app. With this new update, we’re aiming to solve the pain point around adding descriptions to timers while they’re active…For a long time, timers have been at the heart of our time keeping system. A lot of our clients love how timers can start from anywhere in our web app and mobile app. With this new update, we’re aiming to solve the pain point around adding descriptions to timers while they’re active…</p>',NULL,NULL,0,1,6,0,0,NULL,14),(89,20,3,0,0,1,1,0,'SilverStripe\\Blog\\Model\\BlogPost','2019-04-24 23:05:45','2019-04-24 22:04:26','Inherit','Inherit','working-on-the-road-our-new-timers-update-is-live','Working on the road? Our new timers update is live!',NULL,'<p>For a long time, timers have been at the heart of our time keeping system. A lot of our clients love how timers can start from anywhere in our web app and mobile app. With this new update, we’re aiming to solve the pain point around adding descriptions to timers while they’re active…<br><br><br>For a long time, timers have been at the heart of our time keeping system. A lot of our clients love how timers can start from anywhere in our web app and mobile app. With this new update, we’re aiming to solve the pain point around adding descriptions to timers while they’re active…<br><br>For a long time, timers have been at the heart of our time keeping system. A lot of our clients love how timers can start from anywhere in our web app and mobile app. With this new update, we’re aiming to solve the pain point around adding descriptions to timers while they’re active…For a long time, timers have been at the heart of our time keeping system. A lot of our clients love how timers can start from anywhere in our web app and mobile app. With this new update, we’re aiming to solve the pain point around adding descriptions to timers while they’re active…</p>',NULL,NULL,0,1,6,0,0,NULL,14),(90,20,4,1,0,1,1,1,'SilverStripe\\Blog\\Model\\BlogPost','2019-04-24 23:05:46','2019-04-24 22:04:26','Inherit','Inherit','working-on-the-road-our-new-timers-update-is-live','Working on the road? Our new timers update is live!',NULL,'<p>For a long time, timers have been at the heart of our time keeping system. A lot of our clients love how timers can start from anywhere in our web app and mobile app. With this new update, we’re aiming to solve the pain point around adding descriptions to timers while they’re active…<br><br><br>For a long time, timers have been at the heart of our time keeping system. A lot of our clients love how timers can start from anywhere in our web app and mobile app. With this new update, we’re aiming to solve the pain point around adding descriptions to timers while they’re active…<br><br>For a long time, timers have been at the heart of our time keeping system. A lot of our clients love how timers can start from anywhere in our web app and mobile app. With this new update, we’re aiming to solve the pain point around adding descriptions to timers while they’re active…For a long time, timers have been at the heart of our time keeping system. A lot of our clients love how timers can start from anywhere in our web app and mobile app. With this new update, we’re aiming to solve the pain point around adding descriptions to timers while they’re active…</p>',NULL,NULL,0,1,6,0,0,NULL,14),(91,21,1,0,0,1,1,0,'SilverStripe\\Blog\\Model\\BlogPost','2019-04-25 23:33:07','2019-04-25 23:33:07','Inherit','Inherit','new-blog-post','New Blog Post',NULL,NULL,NULL,NULL,0,1,7,0,0,NULL,14),(92,21,2,0,0,1,1,0,'SilverStripe\\Blog\\Model\\BlogPost','2019-04-25 23:35:14','2019-04-25 23:33:07','Inherit','Inherit','invoice-template-customization','Invoice Template Customization',NULL,'<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt...</p><p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt...</p><p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt...</p><p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt...</p><p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt...</p><p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt...</p><p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt...</p><p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt...</p><p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt...</p><p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt...</p><p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt...</p><p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt...</p>',NULL,NULL,0,1,7,0,0,NULL,14),(93,21,3,0,0,1,1,0,'SilverStripe\\Blog\\Model\\BlogPost','2019-04-25 23:35:16','2019-04-25 23:33:07','Inherit','Inherit','invoice-template-customization','Invoice Template Customization',NULL,'<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt...</p><p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt...</p><p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt...</p><p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt...</p><p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt...</p><p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt...</p><p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt...</p><p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt...</p><p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt...</p><p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt...</p><p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt...</p><p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt...</p>',NULL,NULL,0,1,7,0,0,NULL,14),(94,21,4,1,0,1,1,1,'SilverStripe\\Blog\\Model\\BlogPost','2019-04-25 23:35:17','2019-04-25 23:33:07','Inherit','Inherit','invoice-template-customization','Invoice Template Customization',NULL,'<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt...</p><p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt...</p><p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt...</p><p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt...</p><p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt...</p><p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt...</p><p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt...</p><p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt...</p><p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt...</p><p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt...</p><p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt...</p><p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt...</p>',NULL,NULL,0,1,7,0,0,NULL,14),(95,13,3,1,1,1,1,1,'SilverStripe\\Blog\\Model\\Blog','2019-04-26 16:38:57','2019-04-18 23:05:51','Inherit','Inherit',NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,0,NULL,0),(96,1,3,1,1,0,1,1,'Page','2019-04-26 16:39:36','2019-04-18 18:43:10','Inherit','Inherit',NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,0,NULL,0);
/*!40000 ALTER TABLE `SiteTree_Versions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SiteTree_ViewerGroups`
--

DROP TABLE IF EXISTS `SiteTree_ViewerGroups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SiteTree_ViewerGroups` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SiteTreeID` int(11) NOT NULL DEFAULT '0',
  `GroupID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `SiteTreeID` (`SiteTreeID`),
  KEY `GroupID` (`GroupID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SiteTree_ViewerGroups`
--

LOCK TABLES `SiteTree_ViewerGroups` WRITE;
/*!40000 ALTER TABLE `SiteTree_ViewerGroups` DISABLE KEYS */;
/*!40000 ALTER TABLE `SiteTree_ViewerGroups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `VirtualPage`
--

DROP TABLE IF EXISTS `VirtualPage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `VirtualPage` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `VersionID` int(11) NOT NULL DEFAULT '0',
  `CopyContentFromID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `CopyContentFromID` (`CopyContentFromID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `VirtualPage`
--

LOCK TABLES `VirtualPage` WRITE;
/*!40000 ALTER TABLE `VirtualPage` DISABLE KEYS */;
/*!40000 ALTER TABLE `VirtualPage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `VirtualPage_Live`
--

DROP TABLE IF EXISTS `VirtualPage_Live`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `VirtualPage_Live` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `VersionID` int(11) NOT NULL DEFAULT '0',
  `CopyContentFromID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `CopyContentFromID` (`CopyContentFromID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `VirtualPage_Live`
--

LOCK TABLES `VirtualPage_Live` WRITE;
/*!40000 ALTER TABLE `VirtualPage_Live` DISABLE KEYS */;
/*!40000 ALTER TABLE `VirtualPage_Live` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `VirtualPage_Versions`
--

DROP TABLE IF EXISTS `VirtualPage_Versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `VirtualPage_Versions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `VersionID` int(11) NOT NULL DEFAULT '0',
  `CopyContentFromID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  KEY `RecordID` (`RecordID`),
  KEY `Version` (`Version`),
  KEY `CopyContentFromID` (`CopyContentFromID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `VirtualPage_Versions`
--

LOCK TABLES `VirtualPage_Versions` WRITE;
/*!40000 ALTER TABLE `VirtualPage_Versions` DISABLE KEYS */;
/*!40000 ALTER TABLE `VirtualPage_Versions` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-04-26 10:03:43
