FROM brettt89/silverstripe-web

WORKDIR /var/www/html
COPY . .
RUN chown -R www-data:www-data /var/www/html
RUN chmod 755 /var/www/html